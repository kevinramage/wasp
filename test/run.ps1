echo "Run WS tests"
Remove-Item -LiteralPath testsGenenared  -Force -Recurse
New-Item -Path . -Name testsGenenared -ItemType 'directory' | Out-Null
Get-ChildItem -Path tests\*.yml | ForEach-Object {
	Set-Variable -Name fileName $_.Name
	Set-Variable -Name content (Get-Content ('tests\' + $_.Name))
	echo $content | Select-String -Pattern '##INCLUDE.*' | ForEach-Object {
		Set-Variable -Name includeFileName ($_.Line.Substring(10))
		Set-Variable -Name includeContent (Get-Content -Raw ('tests\' + $includeFileName))
		Set-Variable -Name content ($content.Replace($_.Line, $includeContent))
	}
	New-Item ('testsGenenared\' + $fileName ) | Out-Null
	Set-Content -Path ('testsGenenared\' + $fileName ) -Value $content
}