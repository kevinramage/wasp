import * as winston from "winston";
import * as http from "http";
import * as zlib from "zlib";
import { IRequest } from "../common/interface/request";
import { IResponse } from "../common/interface/response";
import { IHeader } from "../common/interface/header";
import { HEADER_KEY, CONFIGURATION } from "../common/constantes";
import { ConfigurationManager } from "./ConfigurationManager";

export class RequestManager {

    private static _instance : RequestManager = null;
    private static MAX_REQUESTS = 1;
    private _agent : http.Agent;

    /**
     * Constructor
     */
    constructor() {

        // Get max socket
        var maxSocket = Number.parseInt(ConfigurationManager.instance.getValue(CONFIGURATION.INJECTION.AGENT_NUMBER));
        if ( isNaN(maxSocket) ) {
            maxSocket = RequestManager.MAX_REQUESTS;
        }
        // Get timeout
        var timeout = Number.parseInt(ConfigurationManager.instance.getValue(CONFIGURATION.INJECTION.TIMEOUT));
        if ( isNaN(timeout) ) {
            timeout = 0;
        }

        // Declare agent
        this._agent = new http.Agent({
            keepAlive: true,
            maxSockets: maxSocket,
            timeout: timeout
        })
    }

    /**
     * Send a request to process
     * @param request request to process
     */
    public sendRequest(request : IRequest ) : Promise<IResponse> {
        winston.debug("RequestManager.sendRequest");
        const instance = this;
        return new Promise<IResponse>((resolve, reject) => {

            // Update content length header
            RequestManager.updateHeader(request, HEADER_KEY.CONTENT_LENGTH, request.body.length.toString());

            // Update user agent header
            const newUserAgent = ConfigurationManager.instance.getValue(CONFIGURATION.INJECTION.USERAGENT);
            if ( newUserAgent && newUserAgent !== "" ) {
                RequestManager.updateHeader(request, HEADER_KEY.USERAGENT, newUserAgent);
            }

            // Add a custom header
            const customHeaderEnabled = ConfigurationManager.instance.getValue(CONFIGURATION.INJECTION.CUSTOM_HEADER.ENABLED) === "true" ? true : false;
            if ( customHeaderEnabled ) {
                const customHeaderKey = ConfigurationManager.instance.getValue(CONFIGURATION.INJECTION.CUSTOM_HEADER.KEY);
                const customHeaderValue = ConfigurationManager.instance.getValue(CONFIGURATION.INJECTION.CUSTOM_HEADER.VALUE);
                RequestManager.updateHeader(request, customHeaderKey, customHeaderValue);
            }

            // Add a TDS Version header
            const tdsVersionEnabled = ConfigurationManager.instance.getValue(CONFIGURATION.INJECTION.TDS_HEADER.ENABLED) === "true" ? true : false;
            if ( tdsVersionEnabled ) {
                const value = ConfigurationManager.instance.getValue(CONFIGURATION.INJECTION.TDS_HEADER.VALUE);
                RequestManager.updateHeader(request, HEADER_KEY.TDSVERSION, value);
            }

            // Proxy host
            var proxyHost = "", proxyPort = "";
            const proxyEnabled = ConfigurationManager.instance.getValue(CONFIGURATION.INJECTION.PROXY.HOST) === "true" ? true : false;
            if ( proxyEnabled ) {
                proxyHost = ConfigurationManager.instance.getValue(CONFIGURATION.INJECTION.PROXY.HOST);
                proxyPort = ConfigurationManager.instance.getValue(CONFIGURATION.INJECTION.PROXY.PORT);
            }

            // Build request
            const options = this.buildRequest(request, proxyHost, proxyPort);
            const httpRequest = http.request(options, (res: http.IncomingMessage) => {
                instance.manageResponse(res).then((response) => {
                    request.response = response;
                    response.request = request;
                    resolve(response);
                }).catch((err) => {
                    reject(err);
                });
            });
            httpRequest.on("error", (err) => { 
                winston.error("RequestManager.sendRequest - request - Internal error: ", err);
                reject(err); 
            });

            // Execute request
            if ( request.body ) {
                httpRequest.write(request.body);
            }
            httpRequest.end();
        });
    }

    /**
     * Build the request to send
     * @param request request to send
     * @param proxy proxy host if defined, empty else
     * @param proxyPort proxy port
     */
    private buildRequest ( request : IRequest, proxy: string, proxyPort: string ) : http.RequestOptions {
        
        // Headers
        const headers = {};
        request.headers.forEach(h => { headers[h.key] = h.value; });

        // Determine complete url
        const url = request.protocol + "://" + request.host + request.url;

        // Build request
        const options : http.RequestOptions = {
            agent: this._agent,
            method: request.method,
            headers: headers
        };

        // Define proxy
        if ( proxy ) {
            options.hostname = proxy;
            options.port = proxyPort;
            options.path = url;

        } else {
            options.hostname = request.host;
            options.path = request.url;
        }

        return options;
    }

    /**
     * Update header value
     * @param request request to update
     * @param key key to update
     * @param value value to set
     */
    private static updateHeader(request : IRequest, key: string, value: string) {
        const header = request.headers.find(h => { return h.key === key });
        if ( header ) {
            header.value = value;
        } else {
            request.headers.push({ key: key, value: value });
        }
    }

    private manageResponse ( res : http.IncomingMessage) {
        const instance = this;
        return new Promise<IResponse>((resolve, reject) => {
            const buffers : Array<Buffer> = [];
            res.on("error", (err: Error) => { reject(err) });
            res.on("data", (chunk: any) => { buffers.push(chunk as Buffer); });
            res.on("end", async () => {
                const buffer : Buffer = Buffer.concat(buffers);
                const body = await instance.decodeBuffer(res, buffer);
                const response = await instance.buildResponse(res, body);
                resolve(response);
            });
        });
    }

    /**
     * Decode buffer
     * @param message http response
     * @param buffer buffer to decode
     */
    private decodeBuffer(message : http.IncomingMessage, buffer : Buffer) {
        const encoding = message.headers['content-encoding'];
        return new Promise<string>((resolve, reject) => {
            if (encoding == 'gzip') {
                zlib.gunzip(buffer, function(err, decoded) {
                    if ( !err ) {
                        resolve(decoded.toString());
                    } else {
                        winston.error("Proxy.getBody - Issue during the gzip encoding");
                        reject(null);
                    }
                });
            } else if (encoding == 'deflate') {
                zlib.inflate(buffer, function(err, decoded) {
                    if ( !err ) {
                        resolve(decoded.toString());
                    } else {
                        winston.error("Proxy.getBody - Issue during the deflate encoding");
                        reject(null);
                    }
                });
            } else {
                resolve(buffer.toString());
            }    
        });    
    }

    /**
     * Build reponse
     * @param res response to process
     * @param body response body
     */
    private buildResponse(res: http.IncomingMessage, body: string) : IResponse {

        // Build headers
        const headers : Array<IHeader> = new Array<IHeader>();
        for ( var i = 0; i < res.rawHeaders.length; i+=2) {
            headers.push({
                key: res.rawHeaders[i],
                value: res.rawHeaders[i+1]
            })
        };

        // Build response
        return {
            body: body,
            headers: headers,
            statusCode: res.statusCode,
            receivedDate: new Date()
        }
    }

    /**
     * Get the unique instance
     */
    public static get instance() {
        if ( RequestManager._instance == null ) {
            RequestManager._instance = new RequestManager();
        }
        return RequestManager._instance;
    }
}