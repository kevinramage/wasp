import { EventEmitter } from "events";
import winston = require("winston");

/**
 * Manage the emit and subscribe events for all the application
 */
export class EventManager {
    private static _instance : EventManager;
    private _eventEmitter : EventEmitter;

    /**
     * Constructor
     */
    constructor() {
        this._eventEmitter = new EventEmitter();
    }

    /**
     * Emit an event
     * @param event event to emit
     * @param args args to transfert
     */
    emit(event: string, ...args: any[]) {
        winston.debug("EventManager.emit: " +  event);
        this._eventEmitter.emit(event, args);
    }

    /**
     * Subscribe to an event
     * @param event event to listen
     * @param listener listener to execute
     */
    subscribe(event: string, listener : ((...args: any[]) => void)) {
        winston.debug("EventManager.subscribe: " + event);
        this._eventEmitter.addListener(event, listener);
    }


    /**
     * Return the unique instance of the event manager
     * @returns return the unique instance
     */
    public static get instance() : EventManager {
        if ( EventManager._instance == null ) {
            EventManager._instance = new EventManager();
        }
        return this._instance;
    }
}