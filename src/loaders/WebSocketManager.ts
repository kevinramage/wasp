import * as socket from "socket.io";
import * as winston from "winston";
import { COMMON_PORT } from "../common/constantes";
import { ProxyManager } from "./ProxyManager";
import { IRequest } from "../common/interface/request";

export class WebSocketManager {

    private static _instance : WebSocketManager = null;
    private _clientRecording : socket.Socket[];

    /**
     * Constructor
     */
    private constructor() {
        this._clientRecording = [];
    }

    /**
     * Initialize
     */
    public init() {
        winston.debug("WebSocketManager.init");
        const instance = this;
        const io = socket.listen(COMMON_PORT.SOCKET_PORT);
        io.on("connection", (socketConnected: socket.Socket) => {
            socketConnected.on("StartRecord", () => {
                instance.startRecord.call(instance, socketConnected);
            });
            socketConnected.on("StopRecord", () => {
                instance.stopRecord.call(instance, socketConnected);
            });
            socketConnected.on("PlayRequest", (request: IRequest) => {
                instance.playRequest.call(this, request);
            });
            socketConnected.error((err) => {
                winston.error("WebSocket internal error: ", err);
            });
        });
    }

    /**
     * Emit an event to all listeners
     * @param eventName event name to emit
     * @param message message object to emit
     */
    public emitEvent(eventName: string, message: object) {
        winston.debug("WebSocketManager.emitEvent");
        this._clientRecording.forEach(c => {
            c.emit(eventName, message);
        });
    }

    /**
     * Start the proxy recording
     * @param socket client socket
     */
    private startRecord(socket: socket.Socket) {
        winston.debug("WebSocketManager.startRecord");
        const instance = this;
        ProxyManager.instance.start(null).then(() => {
            ProxyManager.instance.isRecording = true;
            instance._clientRecording.push(socket);
        });
    }

    /**
     * Stop the proxy recording
     * @param socket client socket
     */
    private stopRecord(socket: socket.Socket) {
        winston.debug("WebSocketManager.stopRecord");
        ProxyManager.instance.stop();
        ProxyManager.instance.isRecording = false;
        const index = this._clientRecording.indexOf(socket);
        if ( index > -1 ) {
            this._clientRecording.splice(index, 1);
        }
    }

    /**
     * Play the request actually pause by the system
     * @param socket client socket
     * @param request request to play
     */
    public playRequest(request : IRequest) {
        winston.debug("WebSocketManager.playRequest");
        ProxyManager.instance.playRequest(request);
    }


    /**
     * Get unique instance
     */
    public static get instance() {
        if ( WebSocketManager._instance == null ) {
            WebSocketManager._instance = new WebSocketManager();
        }
        return WebSocketManager._instance;
    }
}