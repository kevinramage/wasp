import * as winston from "winston";
import { v4 } from "uuid";
import { COMMON_PORT, CONFIGURATION } from "../common/constantes";
import { IRequest } from "../common/interface/request";
import { HTTPUtils } from "../common/utils/HTTPUtils";
import { IResponse } from "../common/interface/response";
import { EventManager } from "./EventManager";
import { Session } from "../models/session";
import { WebSocketManager } from "./WebSocketManager";
import { ConfigurationManager } from "./ConfigurationManager";
const MITM = require('http-mitm-proxy');

/**
 * Proxy manager
 * Enable to analyze incoming request and outcoming response
 */
export class ProxyManager {
    private static _instance : ProxyManager;
    private _proxy;
    private _session : Session;
    private _started : boolean;
    private _isRecording : boolean;
    private _interceptionEnabled : boolean;
    private _callback : Function;
    private _ctx : any;

    private constructor() {
        this._interceptionEnabled = false;
    }

    /**
     * Initialize the proxy
     */
    public initialize() {
        winston.debug("ProxyManager.initialize");
        this._started = false;
        this._proxy = new MITM();
        this._proxy.onRequest(this.onRequest.bind(this));
        this._proxy.onError(function(ctx, err) {
            if ( !err || err.code != "ERR_SSL_SSLV3_ALERT_BAD_CERTIFICATE") {
                winston.error("Proxy error: ", err);
            }
        });
    }

    /**
     * Start the proxy
     * Listen on the port
     */
    public start(session: Session) : Promise<void> {
        winston.debug("ProxyManager.start - Start listenning on " + COMMON_PORT.PROXY_PORT);
        const instance = this;
        return new Promise<void>((resolve, reject) => {
            if ( !instance._started ) {
                instance._proxy.listen({port: COMMON_PORT.PROXY_PORT}, (err) => {
                    if (!err) {
                        instance._session = session;
                        instance._started = true;
                        resolve();
                    } else {
                        reject(err);
                    }
                });
            } else {
                resolve();
            }
        });
    }

    /**
     * Stop the proxy
     * Close the socket
     */
    public stop() {
        winston.debug("ProxyManager.stop - Stop listenning on " + COMMON_PORT.PROXY_PORT);
        if ( this._started ) { 
            this._proxy.close();
            this._started = false;
        }
    }

    /**
     * On request received
     * @param ctx ctx 
     * @param callback callback 
     */
    private onRequest(ctx, callback) {
        winston.debug("ProxyManager.onRequest");

        // Use gunzip
        ctx.use(MITM.gunzip);

        // Collect request informations
        this.collectRequestInformations(ctx);

        // Subscribe on response
        ctx.onResponse(this.onResponse.bind(this));

        return callback();
    }

    /**
     * Collect request informations
     * @param ctx ctx
     */
    private collectRequestInformations(ctx) {
        winston.debug("ProxyManager.collectRequestInformations - Url : " + ctx.clientToProxyRequest.url);
        const instance = this;

        // Collect request metadata
        const request : IRequest = {};
        request.uuid = v4();
        request.protocol = ctx.isSSL ? "https" : "http";
        request.host = ctx.clientToProxyRequest.headers.host;
        request.method = ctx.clientToProxyRequest.method;
        request.url = ctx.clientToProxyRequest.url;
        request.sentDate = new Date();
        request.headers = HTTPUtils.transformHeaders(ctx.clientToProxyRequest.headers);

        // Collect request body
        const chunks = [];
        ctx.onRequestData((ctx, chunk, callback) => {
            chunks.push(chunk);
            return callback(null, chunk);
        });
        ctx.onRequestEnd((ctx, callback) => {
            request.body = (Buffer.concat(chunks)).toString();
            ctx.clientToProxyRequest.request = request;

            // Send request to client if recording activated
            if ( instance._isRecording && this.isViewableRequest(request) ) {
                WebSocketManager.instance.emitEvent("SendRequest", request);
            }

            if ( instance._interceptionEnabled ) {
                WebSocketManager.instance.emitEvent("InterceptRequest", request);
                instance._callback = callback;
            } else {
                return callback();
            }
            
            //return callback();
        });
    }

    /**
     * On response received
     * @param ctx ctx 
     * @param callback callback 
     */
    private onResponse(ctx, callback) {
        winston.debug("ProxyManager.onResponse");
        const instance = this;
        
        // Collect response metadata
        const response : IResponse = {};
        response.statusCode = ctx.serverToProxyResponse.statusCode;
        response.receivedDate = new Date();
        response.headers = HTTPUtils.transformHeaders(ctx.serverToProxyResponse.headers);
        response.request = ctx.clientToProxyRequest.request;

        // Collect request body
        const chunks = [];
        ctx.onResponseData((ctx, chunk, callback) => {
            chunks.push(chunk);
            return callback(null, chunk);
        });
        ctx.onResponseEnd((ctx, callback) => {
            response.body = (Buffer.concat(chunks)).toString();

            // Send response to client if recording activated
            if ( this._isRecording && this.isViewableRequest(response.request) ) {

                // Collect the response type
                response.type = HTTPUtils.identifyType(response.request, response);

                // Throw send response event
                WebSocketManager.instance.emitEvent("SendResponse", response);
            }

            if ( instance.session ) {
                EventManager.instance.emit("NewResponse", response.request, response);
            }
            return callback();
        });

        return callback();
    }

    /**
     * Detect if this request msut be display in GUI or not
     * @param request request
     */
    private isViewableRequest(request : IRequest) {
        return this.displayFirefoxDetectPortal() || request.host !== "detectportal.firefox.com";
    }

    /**
     * Display firefox detect portal
     */
    private displayFirefoxDetectPortal() {
        const value = ConfigurationManager.instance.getValue(CONFIGURATION.PROXY.DISPLAY.FIREFOX_DETECTPORTAL);
        return value === "1" ? true : false;
    }

    /**
     * Update the request actually prepare and send it
     * @param request request to play
     */
    public playRequest(request : IRequest) {
        winston.info("playRequest");
        if ( this._callback ) {
            /*
            this._ctx.proxyToServerRequest = {};
            this._ctx.proxyToServerRequest.method = request.method;
            this._ctx.proxyToServerRequest.url = request.url;
            this._ctx.proxyToServerRequest.headers = {};
            request.headers.forEach(h => {
                this._ctx.proxyToServerRequest.headers[h.key] = h.value;
            });
            */

            // Resume the request
            this._callback();
            this._callback = null;
        }
    }

    /**
     * Return the active session
     */
    public get session() : Session {
        return this._session;
    }

    /**
     * Set the interseption mode
     * @param value true to enable, false to disable
     */
    public set interceptionEnabled(value : boolean) {
        this._interceptionEnabled = value;
    }

    /**
     * Define if the system is in recording mode or not
     * @param value recording activated or not
     */
    public set isRecording(value: boolean) {
        this._isRecording = value;
    }

    /**
     * Return the unique instance of the database manager
     * @returns return the unique instance
     */
    public static get instance() : ProxyManager {
        if ( ProxyManager._instance == null ) {
            ProxyManager._instance = new ProxyManager();
        }
        return this._instance;
    }
}