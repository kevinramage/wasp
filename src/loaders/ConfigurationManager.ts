import { ConfigurationService } from "../services/configurationService";

export class ConfigurationManager {

    private static _instance : ConfigurationManager = null;
    private _configurations : { [ id: string ] : string };

    /**
     * Constructor
     */
    private constructor() {
        this._configurations = {};
    }

    /**
     * Initialize configurations
     */
    public init() {
        const instance = this;
        ConfigurationService.getAllConfigurations().then((configurations) => {
            configurations.forEach(c => {
                instance._configurations[c.key] = c.value;
            });
        });
    }

    /**
     * Get configuration value
     * @param key configuration key
     */
    public getValue(key: string) {
        return this._configurations[key];
    }

    /**
     * Set the configuration value
     * @param key configuration key
     * @param value configuration value
     */
    public setValue(key: string, value: string) {
        this._configurations[key] = value;
    }

    /**
     * Get unique instance
     */
    public static get instance() {
        if ( ConfigurationManager._instance == null ) {
            ConfigurationManager._instance = new ConfigurationManager();
        }
        return ConfigurationManager._instance;
    }
}