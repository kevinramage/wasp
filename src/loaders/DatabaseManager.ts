import * as winston from "winston";
import * as fs from "fs";
import { Sequelize, QueryTypes } from "sequelize";
import { createModels } from "../models";
const sequelizeConfig = require("../config/sequelizeConfig");

/**
 * Initialize the dabase 
 * Link between the application and the database
 */
export class DatabaseManager {
    
    public static SYNC = false;
    private static _instance : DatabaseManager;
    private _database : Sequelize;

    /**
     * Initialize the database
     * - Create models for the first start
     * - Create instances for the first start
     */
    public initialize () : Promise<void> {
        winston.debug("DatabaseManager.initialize");
        const instance = this;
        return new Promise((resolve, reject) => {

            // Create models
            instance._database = createModels(sequelizeConfig);
            instance._database.sync({ force: DatabaseManager.SYNC })
            .then(() => {

                // Execute init SQL script
                if ( DatabaseManager.SYNC ) {
                    var initSQLCode : string = fs.readFileSync('config/initDatabase.sql').toString();
                    initSQLCode = initSQLCode.replace(/\r/g, "").replace(/\n/g, " ");
                    instance._database.query(initSQLCode, { type: QueryTypes.INSERT }).then(() => {
                        winston.info("DatabaseManager.initialize - Database initialized");
                        resolve();

                    }).catch((err) => {
                        winston.error("DatabaseManager.initialize - Internal error: ", err);
                        reject(err);
                    });
                } else {
                    winston.info("DatabaseManager.initialize - Database initialized");
                    resolve();
                }

            }).catch((err) => {
                reject(err);
            });
        });
    }


    /**
     * Return the database instance
     * @returns return the database instance
     */
    public get database() : Sequelize {
        return this._database;
    }

    /**
     * Return the unique instance of the database manager
     * @returns return the unique instance
     */
    public static get instance() : DatabaseManager {
        if ( DatabaseManager._instance == null ) {
            DatabaseManager._instance = new DatabaseManager();
        }
        return this._instance;
    }
}