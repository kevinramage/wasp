import * as winston from "winston";
import * as express from "express";
import * as morgan from "morgan";
import * as bodyParser from "body-parser"; // pull information from HTML POST (express4)
import * as methodOverride from "method-override"; // simulate DELETE and PUT (express4)
import * as helmet from "helmet"; // Security
import * as Router from "../api";
import { COMMON_PORT } from "../common/constantes";

/**
 * API Manager
 * Manage express initialization
 */
export class APIManager {

    /**
     * Initialize the service
     */
    public initialize() : void {
        winston.debug("APIManager.initialize");

        const app = express();

        // Env
        process.env.NODE_ENV = process.env.NODE_ENV || "DEV";

        // Configuration
        app.use(helmet());
        app.use(bodyParser.urlencoded({'extended':true, limit: '50mb', parameterLimit: 50000})); // parse application/x-www-form-urlencoded
        app.use(bodyParser.json()); // parse application/json
        app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
        app.use(methodOverride());        
        if ( process.env.NODE_ENV == "DEV" ) {
            app.use(morgan('dev'));
        } else {
            //app.use(compression());
        }
        
        // Router
        app.use('/api/v1/', Router);

        // Listen
        winston.info("APIManager.initialize - Listen on " + COMMON_PORT.API_PORT)
        app.listen(COMMON_PORT.API_PORT);
    }
}