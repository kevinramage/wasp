import * as express from "express";
import * as winston from "winston";
import { NextFunction } from "express-serve-static-core";
import { COMMON_ERROR } from "../common/constantes";
import { sendBadRequestResponse } from "../api/base";

/**
 * Validate an application
 */
export class ApplicationValidator {

    /**
     * Validate an application
     * @param req request
     * @param res response
     * @param next next
     */
    public static validateApplication(req : express.Request, res: express.Response, next: NextFunction ) {
        winston.debug("ApplicationValidator.validateApplication");
        if ( !!req.body.name ) {
            next();
        } else {
            const error = COMMON_ERROR.BAD_REQUEST_REQUIRED;
            error.message = error.message.replace("%s", "name");
            sendBadRequestResponse(res, error);
        }
    }
}