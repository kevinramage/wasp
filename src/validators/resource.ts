import * as express from "express";
import * as winston from "winston";
import { NextFunction } from "connect";
import { IEntryPoint } from "../common/interface/entryPoint";
import { COMMON_ERROR } from "../common/constantes";
import { sendBadRequestResponse } from "../api/base";

/**
 * Validate a resource
 */
export class ResourceValidator {

    /**
     * Validate entry points
     * @param req request to validate
     * @param res response
     * @param next next function
     */
    public static validateEntryPoints(req : express.Request, res: express.Response, next: NextFunction) {
        winston.debug("ResourceValidator.validateEntryPoints");
        const entryPoints = req.body.entryPoints as Array<IEntryPoint>;
        if ( entryPoints ) {
            var entryPointsChecked = true;
            entryPoints.forEach(e => {
                if ( !e.id && ( !e.name || !e.type )) {
                    next();
                } 
            });
            if ( entryPointsChecked ) {
                next();
            } else {
                const error = COMMON_ERROR.BAD_REQUEST_FORMAT;
                error.message = error.message.replace("%s", "entryPoints");
                sendBadRequestResponse(res, error);
            }
        } else {
            const error = COMMON_ERROR.BAD_REQUEST_REQUIRED;
            error.message = error.message.replace("%s", "entryPoints");
            sendBadRequestResponse(res, error);
        }
    }
}