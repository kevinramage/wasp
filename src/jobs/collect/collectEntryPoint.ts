import * as winston from "winston";
import { IResource } from "../../common/interface/resource";
import { IRequest } from "../../common/interface/request";
import { IEntryPoint } from "../../common/interface/entryPoint";
import { ENTRYPOINT_TYPE } from "../../common/constantes";
import { EntryPointService } from "../../services/entryPointService";
import { EventManager } from "../../loaders/EventManager";
import { Control } from "../../models/control";

/**
 * Collect entry points of a resource
 * @param args args
 */
export function collecEntryPoints(args: any[]) {
    winston.debug("collecEntryPoints");
    const request : IRequest = args[0];
    const resource : IResource = args[1];
    const controls : Control[] = args[2];

    // Identify entry points
    var entryPoints : IEntryPoint[] = [];
    entryPoints = identifyRequestQuery(resource);
    entryPoints = entryPoints.concat(identifyRequestBody(request, resource));
    entryPoints = entryPoints.concat(identifyRequestHeader(request, resource));

    // Save entry points in database
    EntryPointService.createEntryPoints(entryPoints).then(() => {
        EventManager.instance.emit("determineComponents", request, resource, controls);
    }).catch((err) => {
        winston.error("collecEntryPoints - Internal error: ", err);
    });
}

/**
 * Identify request query entry points
 * @param resource resource to analyze
 */
function identifyRequestQuery(resource : IResource)  {
    const entryPoints : IEntryPoint[] = [];
    const searchParams = new URL(resource.url).searchParams;
    searchParams.forEach((value: string, key: string) => {
        entryPoints.push({
            name: key,
            type: ENTRYPOINT_TYPE.QUERY
        });
    });
    return entryPoints;
}

/**
 * Identify request body entry points
 * @param request request to analyze
 * @param resource resource to analyze
 */
function identifyRequestBody(request: IRequest, resource: IResource) {
    const entryPoints : IEntryPoint[] = [];
    const jsonRegex = /\{.*\:\{.*\:.*\}\}/g;
    const formEncoded = /^[A-Za-z0-9%\.]+=[A-Za-z0-9%\.]*(&[A-Za-z0-9%\.]+=[A-Za-z0-9%\.]*)*$/g;
    const formEncodedKey = /(?<KEY>[A-Za-z0-9%\.]+)=/g;

    // FormEncoded
    if ( request.body.toString().match(formEncoded)) {
        var match : RegExpExecArray;
        while ( match = formEncodedKey.exec(request.body.toString())) {
            entryPoints.push({
                name: match.groups["KEY"],
                type: ENTRYPOINT_TYPE.BODY,
                resourceId: resource.id
            });
        }
    }

    return entryPoints;
}

/**
 * Identify request header entry points
 * @param request request
 * @param resource resource
 */
function identifyRequestHeader(request : IRequest, resource: IResource) {
    const headersName = [ "referer", "origin", "user-agent"];
    const headersFound = request.headers.filter(h => headersName.includes(h.key) );
    return headersFound.map(h => {
        return {
            name: h.key,
            type: ENTRYPOINT_TYPE.HEADER,
            resourceId: resource.id
        };
    })
}