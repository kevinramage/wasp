import * as winston from "winston";
import { EventManager } from "../../loaders/EventManager";

export function runCollects(args : any[]) {
    winston.debug("runCollects");
    EventManager.instance.emit("CollectLinks", args[0], args[1]);
}