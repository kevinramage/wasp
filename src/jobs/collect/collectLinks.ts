import * as winston from "winston";
import { Request } from "../../models/request";
import { HTMLUtils } from "../../common/utils/HTMLUtils";
import { IControl } from "../../common/interface/control";
import { ControlService } from "../../services/controlService";
import { Resource } from "../../models/resource";
import { EventManager } from "../../loaders/EventManager";

export function collectLinks(args: any[]) {
    winston.debug("collectLinks");
    const request : Request = args[0];
    const resource : Resource = args[1];

    if ( request.response ) {

        // Extract informations
        const links : IControl[] = HTMLUtils.extractLinks(request.response.body);

        // Create controls
        ControlService.createControls(links, resource.id).then((links) => {
            EventManager.instance.emit("CollectForms", request, resource, links);
        }).catch((err) => {
            winston.error("collectLinks - Internal error, stop of the process", err);
        });
        
    } else {
        winston.error("collectLinks - Response missing");
    }
    
}