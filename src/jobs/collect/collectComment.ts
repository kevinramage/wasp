import * as winston from "winston";
import { HTMLUtils } from "../../common/utils/HTMLUtils";
import { Resource } from "../../models/resource";
import { Request } from "../../models/request";
import { CommentService } from "../../services/commentService";
import { EventManager } from "../../loaders/EventManager";
import { Control } from "../../models/control";

export function collectComments(args : any[]) {
    winston.debug("collectComments");
    const request : Request = args[0];
    const resource : Resource = args[1];
    const controls : Control[] = args[2];

    if ( request.response ) {

        // Extract comments
        const comments : string[] = HTMLUtils.extractComments(request.response.body);

        // Create comment
        CommentService.createComments(comments, resource.id).then(() => {
            EventManager.instance.emit("CollectWords", request, resource, controls);
        }).catch((err) => {
            winston.error("collectLinks - Internal error, stop of the process: ", err);
        });

    } else {
        winston.error("collectComments - Response missing");
    }
}