import * as winston from "winston";
import { Request } from "../../models/request";
import { Resource } from "../../models/resource";
import { HTMLUtils } from "../../common/utils/HTMLUtils";
import { IControl } from "../../common/interface/control";
import { ControlService } from "../../services/controlService";
import { EventManager } from "../../loaders/EventManager";
import { Control } from "../../models/control";

export function collectForms(args: any[]) {
    winston.debug("collectForms");
    const request : Request = args[0];
    const resource : Resource = args[1];
    const links : Control[] = args[2];
    
    if ( request.response ) {

        // Extract informations
        var controls : IControl[] = HTMLUtils.extractForm(request.response.body);
        controls = controls.concat(HTMLUtils.extractH1(request.response.body));
        controls = controls.concat(HTMLUtils.extractH2(request.response.body));
        controls = controls.concat(HTMLUtils.extractH3(request.response.body));
        controls = controls.concat(HTMLUtils.extractH4(request.response.body));
        controls = controls.concat(HTMLUtils.extractH5(request.response.body));
        controls = controls.concat(HTMLUtils.extractH6(request.response.body));

        // Create controls
        ControlService.createControls(controls, resource.id).then((newControls) => {
            EventManager.instance.emit("CollectComments", request, resource, newControls.concat(links));
        }).catch((err) => {
            winston.error("collectLinks - Internal error, stop of the process: ", err);
        });

    } else {
        winston.error("collectForms - Response missing");
    }
}