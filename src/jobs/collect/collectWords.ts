import * as winston from "winston";
import { Request } from "../../models/request";
import { Resource } from "../../models/resource";
import { HTMLUtils } from "../../common/utils/HTMLUtils";
import { WordService } from "../../services/wordService";
import { EventManager } from "../../loaders/EventManager";
import { Control } from "../../models/control";

export function collectWords(args: any[]) {
    winston.debug("collectWords");
    const request : Request = args[0];
    const resource : Resource = args[1];
    const controls : Control[] = args[2];

    if ( request.response ) {

        // Extract informations
        const words = HTMLUtils.extractWords(request.response.body);
        const counter = countWords(words);

        // Create instance
        WordService.createWords(counter, resource.id).then(() => {
            EventManager.instance.emit("CollecEntryPoints", request, resource, controls);
        }).catch((err) => {
            winston.error("collectWords - Internal error: ", err);
        });
    }
}

function countWords(words : string[]) {
    const dictionnary : { [id: string]: number} = {};
    words.forEach(w => {
        if (dictionnary[w]) {
            dictionnary[w]++;
        } else {
            dictionnary[w] = 1;
        }
    });
    return dictionnary;
}