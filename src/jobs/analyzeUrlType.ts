import * as winston from "winston";
import { Response } from "../models/response";
import { ProxyManager } from "../loaders/ProxyManager";
import { SessionService } from "../services/sessionService";
import { URL_TYPE, URLTYPE_PAGESCORE, ANALYZEURL_MINREQUEST } from "../common/constantes";
import { ArrayUtils } from "../common/utils/ArrayUtils";
import { ApplicationService } from "../services/applicationService";
import { HTTPUtils } from "../common/utils/HTTPUtils";
import { URLUtils } from "../common/utils/URLUtils";
import { EventManager } from "../loaders/EventManager";
import { Request } from "../models/request";

/**
 * Job to analyze the url type of the application
 * @param args 
 */
export async function analyzeUrlType (args : any[]) {
    winston.debug("analyzeUrlType");

    const request : Request = args[0];

    // Check if analyzeUrlType process if required or not
    const application = await ApplicationService.getApplication(ProxyManager.instance.session.applicationId);
    if ( application.urlType ) {
        if ( HTTPUtils.isXHRRequest(request) ) {
            EventManager.instance.emit("GenerateResource", request, application.urlType);
        }

    } else {

        // Check if there are enought informations to determine the url type
        const xhrRequests = await SessionService.getXHRRequests(ProxyManager.instance.session.id);
        winston.debug("analyzeUrlType XHR: " + xhrRequests.length);
        if ( xhrRequests.length >= ANALYZEURL_MINREQUEST ) {

            // Collect and check there are representative urls
            const urls = collectUrls(xhrRequests);
            winston.debug("analyzeUrlType Urls: " + urls.length);
            if ( urls.length >= ANALYZEURL_MINREQUEST ) {

                // Update application
                const urlType = computeUrlType(urls);
                ApplicationService.changeUrlType(ProxyManager.instance.session.applicationId, urlType);

                // Send XHR requests to the next stage
                xhrRequests.forEach(r => {
                    EventManager.instance.emit("GenerateResource", r, urlType);
                });
            }
        }
    }
}

/**
 * Extract url of each requests
 * @param xhrRequests XHR requests to analyze
 */
export function collectUrls(xhrRequests) {
    var urls = xhrRequests.map(HTTPUtils.getCompleteUrl);
    urls = urls.filter(ArrayUtils.isUnique);
    return urls;
}

/**
 * Extract pathname of each url
 * @param urls url to analyze
 */
export function collectPathnames(urls) {
    return urls.map(URLUtils.getLastPathName);
}

/**
 * Compute url type
 * @param urls urls to analyze
 */
export function computeUrlType(urls) {
    const pathnames = collectPathnames(urls);
    const counter = ArrayUtils.countOccurences(pathnames);
    const path = ArrayUtils.getMaxElement(counter);
    const score = counter[path] / pathnames.length;
    return score > URLTYPE_PAGESCORE ? URL_TYPE.PAGE : URL_TYPE.VIEW;
}