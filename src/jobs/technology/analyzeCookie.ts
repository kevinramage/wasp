import * as winston from "winston";
import { Request } from "../../models/request";
import { Mutex } from "async-mutex";
import { ITechnology } from "../../common/interface/technology";
import { TECHNOLOGY_TYPE, HEADER_KEY } from "../../common/constantes";
import { ProxyManager } from "../../loaders/ProxyManager";
import { TechnologyService } from "../../services/technologyService";

const mutex = new Mutex();

/**
 * Job to analyze cookie
 * @param args args
 */
export async function analyzeCookie(args : any[]) {
    winston.debug("analyzeCookie");
    const request : Request = args[0];
    
    // Search the header "Server"
    if ( request && request.headers ) {
        const serverHeader = request.headers.find(h => { return h.key && h.key.toLowerCase() == HEADER_KEY.COOKIE });
        if ( serverHeader ) {

            // Collect technology informations
            const technology : ITechnology = {};
            technology.type = TECHNOLOGY_TYPE.COOKIE;

            // Put a lock
            const release = await mutex.acquire();

            try {
                // Get technology
                const oldTechnology = await TechnologyService.getTechnologyFromType(ProxyManager.instance.session.applicationId, TECHNOLOGY_TYPE.COOKIE);

                // Extract all cookies names
                const cookiesName : string[] = serverHeader.value.split(";").map(elt => {
                    return elt.split("=")[0];
                }).sort();

                // Merge the previous cookie if necessary
                if ( oldTechnology ) {
                    const oldCookiesName = oldTechnology.value.split(";").sort();
                    oldCookiesName.forEach((c) => {
                        if ( cookiesName.indexOf(c) == -1 ) {
                            cookiesName.push(c);
                        }
                    });
                }
                technology.value = cookiesName.join(";");

                // Instanciate a new technology instance
                await TechnologyService.createTechnology(ProxyManager.instance.session.applicationId, technology);
            
            } finally {
                
                // Release lock
                release();
            }
        }
    }    
}