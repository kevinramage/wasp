import * as winston from "winston";
import { Request } from "../../models/request";

import { Mutex } from "async-mutex";
import { ITechnology } from "../../common/interface/technology";
import { TECHNOLOGY_TYPE } from "../../common/constantes";
import { TechnologyService } from "../../services/technologyService";
import { ProxyManager } from "../../loaders/ProxyManager";

const mutex = new Mutex();

export async function analyzeClientLibrary(args : any[]) {
    winston.debug("analyzeClientLibrary");
    const request : Request = args[0];

    if ( request.response ) {

        var technology : ITechnology = {
            type: TECHNOLOGY_TYPE.CLIENTLIB
        };

        // Jquery
        if ( request.response.body.toLowerCase().indexOf("jquery") != -1) {
            technology.value = "JQuery";
        }

        // Bootstrap
        else if ( request.response.body.toLowerCase().indexOf("bootstrap") != -1) {
            technology.value = technology.value ? technology.value + ";Bootstrap" : "Bootstrap";
        }

        // Instanciate a new technology instance
        if ( technology.value ) {
            const release = await mutex.acquire();
            try {
                await TechnologyService.createTechnology(ProxyManager.instance.session.applicationId, technology);
            } finally {
                release();
            }
        }
    }
}