import * as winston from "winston";
import { Mutex } from "async-mutex";
import { Request } from "../../models/request";
import { HEADER_KEY, TECHNOLOGY_TYPE } from "../../common/constantes";
import { ITechnology } from "../../common/interface/technology";
import { TechnologyService } from "../../services/technologyService";
import { ProxyManager } from "../../loaders/ProxyManager";
import { IWarning } from "../../common/interface/warning";
import { Resource } from "../../models/resource";
import { WarningService } from "../../services/warningService";
import { WARNING_SERVER_VERSION } from "../../common/warnings";

const mutex = new Mutex();

/**
 * Job to analyze the server header
 * @param args args
 */
export async function analyzeServerHeader(args : any[]) {
    winston.debug("analyzeServerHeader");
    const request : Request = args[0];
    const resource : Resource = args[1];
    var release;
    
    // Search the header "Server"
    if ( request && request.response && request.response.headers ) {
        const serverHeader = request.response.headers.find(h => { return h.key && h.key.toLowerCase() == HEADER_KEY.SERVER });
        if ( serverHeader ) {

            // Collect technology informations
            const technology : ITechnology = {};
            technology.type = TECHNOLOGY_TYPE.SERVER;
            technology.value = serverHeader.value;

            // Try to identify the version
            const regex = /(?<name>[a-zA-Z]*).(?<version>[0-9]*\.[0-9]*(\.[0-9]*)?)/g;
            const regexExec = regex.exec(serverHeader.value);
            if ( regexExec ) {

                // Update technology values with the version
                technology.name = regexExec.groups.name;
                technology.version = regexExec.groups.version;
                technology.value = technology.name;

                // Create a warning
                const warning : IWarning = {
                    title: WARNING_SERVER_VERSION.title,
                    risk: WARNING_SERVER_VERSION.risk,
                    type: WARNING_SERVER_VERSION.type,
                    value: serverHeader.value,
                    message: WARNING_SERVER_VERSION.message.replace("{HEADER_NAME}", serverHeader.key)
                        .replace("${HEADER_VALUE}", serverHeader.value),
                    resourceId: resource.id,
                    applicationId: ProxyManager.instance.session.applicationId
                }
                release = await mutex.acquire();
                try {
                    await WarningService.createWarning(warning);
                } finally {
                    release();
                }
            }

            // Instanciate a new technology instance
            release = await mutex.acquire();
            try {
                await TechnologyService.createTechnology(ProxyManager.instance.session.applicationId, technology);
            } finally {
                release();
            }
        }
    }
}