import * as winston from "winston";
import { Request } from "../../models/request";
import { ITechnology } from "../../common/interface/technology";
import { TECHNOLOGY_TYPE } from "../../common/constantes";
import { TechnologyService } from "../../services/technologyService";
import { ProxyManager } from "../../loaders/ProxyManager";
import { Mutex } from "async-mutex";

const mutex = new Mutex();

/**
 * Job to analyze Angular technology
 * @param args args
 */
export async function analyzeAngularTechnology(args : any[]) {
    winston.debug("analyzeAngularTechnology");
    const request : Request = args[0];

    if ( request.response ) {
        const body = request.response.body.toLowerCase();
        var angularDetected = false;
        var angularVersion = null;

        // ng-version
        const regex = /ng-version=["|']([0-9]*(.[0-9]*(.[0-9]*)?)?)["|']/gi;
        const match = regex.exec(request.response.body);
        if ( match && match.length > 1 ) {
            angularDetected = true;
            angularVersion = match[1];
        }
        // ng-app
        if ( !angularDetected && body.indexOf("ng-app") != -1) {
            angularDetected = true;
        // app-root
        } else if ( !angularDetected && body.indexOf("app-root") != -1 ) {
            angularDetected = true;
        }

        if ( angularDetected ) {

            // Prepare instance
            const technology : ITechnology = {};
            technology.type = TECHNOLOGY_TYPE.CLIENT;
            technology.value = "Angular";
            if ( angularVersion ) {
                technology.name = "Angular";
                technology.version = angularVersion;
            }

            // Instanciate a new technology instance
            const release = await mutex.acquire();
            try {
                await TechnologyService.createTechnology(ProxyManager.instance.session.applicationId, technology);
            } finally {
                release();
            }
        }
    }
}