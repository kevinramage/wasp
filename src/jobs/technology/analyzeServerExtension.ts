import * as winston from "winston";
import { ITechnology } from "../../common/interface/technology";
import { TECHNOLOGY_TYPE } from "../../common/constantes";
import { TechnologyService } from "../../services/technologyService";
import { ProxyManager } from "../../loaders/ProxyManager";
import { Mutex } from "async-mutex";

const mutex = new Mutex();

export async function analyzeServerExtension (args : any[])  {
    winston.debug("analyzeServerHeader");
    const request : Request = args[0];

    // Collect informations
    const technology : ITechnology = {};
    technology.type = TECHNOLOGY_TYPE.EXTENSION;

    const extensions = [ ".html", ".php", ".asp", ".aspx", ".jsp" ];
    extensions.forEach(ext => {
        if ( request.url.indexOf(ext) != -1) {
            technology.value = ext;
        }
    });

    if ( technology.value ) {

        // Instanciate a new technology instance
        const release = await mutex.acquire();
        try {
            await TechnologyService.createTechnology(ProxyManager.instance.session.applicationId, technology);
        } finally {
            release();
        }
    }
}