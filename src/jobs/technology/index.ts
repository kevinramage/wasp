import * as winston from "winston";
import { EventManager } from "../../loaders/EventManager";

/**
 * Job to analyze technologies
 * @param args args
 */
export function analyzeTechnologies(args : any[]) : void {
    winston.debug("analyzeTechnologies");
    const request = args[0];
    const resource = args[1];
    EventManager.instance.emit("AnalyzeServerHeader", request, resource);
    EventManager.instance.emit("AnalyzePoweredByHeader", request, resource);
    EventManager.instance.emit("AnalyzeCookie", request, resource);
    EventManager.instance.emit("AnalyzeAngularTechnology", request, resource);
    EventManager.instance.emit("AnalyzeServerExtension", request, resource);
    EventManager.instance.emit("AnalyseMysqlTechnology", request, resource);
    EventManager.instance.emit("AnalyzeClientLibrary", request, resource);
}