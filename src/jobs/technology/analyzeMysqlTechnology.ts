import * as winston from "winston";
import { Mutex } from "async-mutex";
import { Request } from "../../models/request";
import { Resource } from "../../models/resource";
import { IWarning } from "../../common/interface/warning";
import { TECHNOLOGY_TYPE } from "../../common/constantes";
import { ProxyManager } from "../../loaders/ProxyManager";
import { WarningService } from "../../services/warningService";
import { ITechnology } from "../../common/interface/technology";
import { TechnologyService } from "../../services/technologyService";
import { WARNING_SERVER_VERSION } from "../../common/warnings";

const mutex = new Mutex();

export async function analyseMysqlTechnology(args : any[]) {
    winston.debug("analyseMysqlTechnology");
    const request : Request = args[0];
    const resource : Resource = args[1];
    var release;

    if ( request.response ) {
        const mysqlErrorMessage = "Warning: mysql_fetch_array() expects parameter";
        if ( request.response.body.indexOf(mysqlErrorMessage) != -1 ) {

            // Prepare warning and technology
            const warning : IWarning = {
                title: WARNING_SERVER_VERSION.title,
                risk: WARNING_SERVER_VERSION.risk,
                type: WARNING_SERVER_VERSION.type,
                message: WARNING_SERVER_VERSION.message,
                value: mysqlErrorMessage,
                applicationId: ProxyManager.instance.session.applicationId,
                resourceId: resource.id
            };
            const technology : ITechnology = {
                type: TECHNOLOGY_TYPE.DATABASE,
                value: "Mysql"
            }

            // Create instance of warning and technology
            release = await mutex.acquire();
            try {
                await WarningService.createWarning(warning);
                await TechnologyService.createTechnology(ProxyManager.instance.session.applicationId, technology);
            } catch {
                release();
            }
        }
    }
}