import * as winston from "winston";
import { IResource } from "../../common/interface/resource";
import { IControl } from "../../common/interface/control";
import { IComponent } from "../../common/interface/component";
import { ComponentService } from "../../services/componentService";
import { COMPONENT_TYPE, COMPONENT_COMMON, ATTRIBUTE_NAME, CONTROL_TYPE } from "../../common/constantes";
import { StringUtils } from "../../common/utils/StringUtils";

export const determineLogoutComponent = (args : any[]) => {
    winston.debug("determineLogoutComponent");
    const resource = args[1];
    const controls = args[2];

    // Compute the logout component
    const logoutComponent = computeResourceScore(resource, controls);

    // Create component
    if ( logoutComponent ) {
        ComponentService.createComponent(logoutComponent);
    }
};

function computeResourceScore(resource : IResource, controls : IControl[]) : IComponent {
    
    // Filter controls to collect only forms
    const links : IControl[] = controls.filter(f => { return f.type === CONTROL_TYPE.LINK });

    // Compute the first element
    var currentComponent : IComponent = null;
    if ( links.length > 0 ) {
        currentComponent = computeLinkScore(resource, links[0]);
    }

    // Compute the remaining elements
    for ( var i = 1; i < links.length; i++) {
        const newComponent = computeLinkScore(resource, links[i]);
        if ( newComponent.score > currentComponent.score ) {
            currentComponent = newComponent;
        }
    }

    return currentComponent;
}


function computeLinkScore(resource : IResource, link : IControl) : IComponent {
    const logoutComponent : IComponent = {
        type: COMPONENT_TYPE.LOGOUT,
        html: link.html,
        detail: "",
        score: 0,
        resourceId: resource.id
    }

    // Identifier (3 points)
    const isLogoutIdentifier = link.identifier && StringUtils.containsOneOf(link.identifier.toLowerCase(), COMPONENT_COMMON.LOGOUT_KEYWORDS);
    if ( isLogoutIdentifier ) {
        logoutComponent.score += 3;
    }
    const logoutIdentifierDetail = "Identifier: " + ((isLogoutIdentifier) ? "OK" : "KO");
    logoutComponent.detail += logoutIdentifierDetail;


    // Text (5 points)
    const isLogoutText = link.text && StringUtils.containsOneOf(link.text.toLowerCase(), COMPONENT_COMMON.LOGOUT_KEYWORDS);
    if ( isLogoutText ) {
        logoutComponent.score += 5;
    }
    const logoutTextDetail = "\nText: " + ((isLogoutText) ? "OK" : "KO");
    logoutComponent.detail += logoutTextDetail;

    // Href (3 points)
    const hrefAtt = link.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.HREF});
    const isLogoutHref = hrefAtt && hrefAtt.value && StringUtils.containsOneOf(hrefAtt.value.toLowerCase(), COMPONENT_COMMON.LOGOUT_KEYWORDS);
    if ( isLogoutHref ) {
        logoutComponent.score += 3;
    }
    const logoutHrefDetail = "\nHref: " + ((isLogoutHref) ? "OK" : "KO");
    logoutComponent.detail += logoutHrefDetail;

    logoutComponent.score /= 11.0;
    winston.debug("determineLogoutComponent.computeFormScore - Score: " + logoutComponent.score);

    return logoutComponent;
}