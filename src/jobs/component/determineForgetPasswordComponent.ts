import * as winston from "winston";
import { IRequest } from "../../common/interface/request";
import { IResource } from "../../common/interface/resource";
import { IControl } from "../../common/interface/control";
import { IComponent } from "../../common/interface/component";
import { CONTROL_TYPE, COMPONENT_TYPE } from "../../common/constantes";
import { ControlService } from "../../services/controlService";
import { ComponentService } from "../../services/componentService";

export const determineForgetPasswordComponent = (args : any[]) => {
    winston.debug("determineForgetPasswordComponent");
    const request = args[0];
    const resource = args[1];
    const controls = args[2];

    // Compute the forget password component
    const forgetPasswordComponent = computeFormsScore(request, resource, controls);

    // Create 
    if ( forgetPasswordComponent ) {
        ComponentService.createComponent(forgetPasswordComponent);
    }
}


function computeFormsScore(request: IRequest, resource: IResource, controls : IControl[]) : IComponent {
    
    // Filter controls to collect only forms
    const forms : IControl[] = controls.filter(f => { return f.type === CONTROL_TYPE.FORM });

    // Compute the first element
    var currentComponent : IComponent = null;
    if ( forms.length > 0 ) {
        currentComponent = computeFormScore(request, resource, forms[0], controls);
    }

    // Compute the remaining elements
    for ( var i = 1; i < forms.length; i++) {
        const newComponent = computeFormScore(request, resource, forms[i], controls);
        if ( newComponent.score > currentComponent.score ) {
            currentComponent = newComponent;
        }
    }

    return currentComponent;
}

function computeFormScore(request: IRequest, resource: IResource, form: IControl, controls: IControl[]) : IComponent{
    const forgetPasswordComponent : IComponent = {
        type: COMPONENT_TYPE.FORGET_PASSWORD,
        html: form.html,
        score: 0,
        detail: "",
        resourceId: resource.id
    };


    return forgetPasswordComponent;
}