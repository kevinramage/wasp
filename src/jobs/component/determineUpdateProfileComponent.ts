import * as winston from "winston";
import { ComponentService } from "../../services/componentService";
import { IRequest } from "../../common/interface/request";
import { IResource } from "../../common/interface/resource";
import { IControl } from "../../common/interface/control";
import { CONTROL_TYPE, ATTRIBUTE_NAME, HTTP_METHOD, COMPONENT_TYPE, COMPONENT_COMMON, INPUT_TYPE } from "../../common/constantes";
import { IComponent } from "../../common/interface/component";
import { StringUtils } from "../../common/utils/StringUtils";

export const determineUpdateProfileComponent = (args : any[]) => {
    winston.debug("determineUpdateProfileComponent");
    const request = args[0];
    const resource = args[1];
    const controls = args[2];

    // Compute the login component
    const updateProfileComponent = computeFormsScore(request, resource, controls);

    // Create component
    if ( updateProfileComponent ) {
        ComponentService.createComponent(updateProfileComponent);
    }
};

function computeFormsScore(request: IRequest, resource: IResource, controls : IControl[]) : IComponent {
    
    // Filter controls to collect only forms
    const forms : IControl[] = controls.filter(f => { return f.type === CONTROL_TYPE.FORM });

    // Compute the first element
    var currentComponent : IComponent = null;
    if ( forms.length > 0 ) {
        currentComponent = computeFormScore(request, resource, forms[0], controls);
    }

    // Compute the remaining elements
    for ( var i = 1; i < forms.length; i++) {
        const newComponent = computeFormScore(request, resource, forms[i], controls);
        if ( newComponent.score > currentComponent.score ) {
            currentComponent = newComponent;
        }
    }

    return currentComponent;
}

function computeFormScore(request: IRequest, resource: IResource, form: IControl, controls: IControl[]) : IComponent{
    const updateProfileComponent : IComponent = {
        type: COMPONENT_TYPE.UPDATE_PROFILE,
        html: form.html,
        score: 0,
        detail: "",
        resourceId: resource.id
    };

    // Title (2 points)
    const isUpdateProfileTitle = StringUtils.containsOneOf(resource.title, COMPONENT_COMMON.PROFIL_KEYWORDS);
    if ( isUpdateProfileTitle ) {
        updateProfileComponent.score += 2;
    }
    const titleDetail = "Title: " + ((isUpdateProfileTitle) ? "OK" : "KO");
    winston.debug("determineUpdateProfileComponent.computeFormScore - " + titleDetail);
    updateProfileComponent.detail += titleDetail;

    // HTML Header (1 point)
    const loginHeaderTag = controls.find(c => {
        return StringUtils.equalsOneOf(c.type, [ CONTROL_TYPE.H1, CONTROL_TYPE.H2, CONTROL_TYPE.H3, CONTROL_TYPE.H4, CONTROL_TYPE.H5, CONTROL_TYPE.H6 ])
            && StringUtils.containsOneOf(c.text.toLowerCase(), COMPONENT_COMMON.PROFIL_KEYWORDS )
    });
    if ( loginHeaderTag ) {
        updateProfileComponent.score ++;
    }
    const htmlHeaderDetail = "\nHtml header: " + ((loginHeaderTag) ? "OK" : "KO");
    winston.debug("determineUpdateProfileComponent.computeFormScore - " + htmlHeaderDetail);
    updateProfileComponent.detail += htmlHeaderDetail;

    // Url (2 points)
    var isUpdateProfileUrl = false;
    const actionAtt = form.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.ACTION });
    if ( actionAtt && actionAtt.value.trim() !== "" && actionAtt.value !== "#" &&
        StringUtils.containsOneOf(actionAtt.value, COMPONENT_COMMON.PROFIL_KEYWORDS)) {
        updateProfileComponent.score += 2;
        isUpdateProfileUrl = true;
    } else if ( StringUtils.containsOneOf(request.url, COMPONENT_COMMON.PROFIL_KEYWORDS)) {
        updateProfileComponent.score += 2;
        isUpdateProfileUrl = true;
    }
    const urlDetail = "\nUrl: " + ((isUpdateProfileUrl) ? "OK" : "KO");
    winston.debug("determineUpdateProfileComponent.computeFormScore - " + urlDetail);
    updateProfileComponent.detail += urlDetail;

    // POST method (3 points)
    const methodAtt = form.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.METHOD && a.value.toUpperCase() === HTTP_METHOD.POST });
    if ( methodAtt ) {
        updateProfileComponent.score += 3;
    }
    const methodDetail = "\nPOST Method: " + ((methodAtt) ? "OK" : "KO");
    winston.debug("determineUpdateProfileComponent.computeFormScore - " + methodDetail);
    updateProfileComponent.detail += methodDetail;

    // Username field (5 points)
    const userNameField = form.children.find(c => {
        const attType = c.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.TYPE });
        return c.type === CONTROL_TYPE.INPUT 
            && ((attType && attType.value && (attType.value.trim() === "" || attType.value.toLowerCase() === INPUT_TYPE.TEXT)) || !attType)
            && StringUtils.containsOneOf(c.identifier.toLowerCase(), COMPONENT_COMMON.USERNAME_KEYWORDS);
    });
    if ( userNameField ) {
        updateProfileComponent.score += 5;
    }
    const userNameDetail = "\nUsername field: " + ((userNameField) ? "OK" : "KO"); 
    winston.debug("determineUpdateProfileComponent.computeFormScore - " + userNameDetail);
    updateProfileComponent.detail += userNameDetail;

    // Mail field (5 points)
    const mailField = form.children.find(c => {
        const attType = c.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.TYPE });
        return c.type === CONTROL_TYPE.INPUT 
            && ((attType && attType.value && (attType.value.trim() === "" || attType.value.toLowerCase() === INPUT_TYPE.TEXT || attType.value.toLowerCase() === INPUT_TYPE.MAIL)) || !attType)
            && StringUtils.containsOneOf(c.identifier.toLowerCase(), COMPONENT_COMMON.MAIL_KEYWORDS);
    });
    if ( mailField ) {
        updateProfileComponent.score += 5;
    }
    const mailDetail = "\nMail field: " + ((mailField) ? "OK" : "KO"); 
    winston.debug("determineUpdateProfileComponent.computeFormScore - " + mailDetail);
    updateProfileComponent.detail += mailDetail;

    // Phone field (3 points)
    const phoneField = form.children.find(c => {
        const attType = c.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.TYPE });
        return c.type === CONTROL_TYPE.INPUT 
            && ((attType && attType.value && (attType.value.trim() === "" || attType.value.toLowerCase() === INPUT_TYPE.TEXT || attType.value.toLowerCase() === INPUT_TYPE.TEL)) || !attType)
            && StringUtils.containsOneOf(c.identifier.toLowerCase(), COMPONENT_COMMON.PHONE_KEYWORDS);
    });
    if ( phoneField ) {
        updateProfileComponent.score += 3;
    }
    const phoneDetail = "\nPhone field: " + ((phoneField) ? "OK" : "KO"); 
    winston.debug("determineUpdateProfileComponent.computeFormScore - " + phoneDetail);
    updateProfileComponent.detail += phoneDetail;

    // Address field (2 points)
    const addressField = form.children.find(c => {
        const attType = c.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.TYPE });
        return c.type === CONTROL_TYPE.INPUT 
            && ((attType && attType.value && (attType.value.trim() === "" || attType.value.toLowerCase() === INPUT_TYPE.TEXT)) || !attType)
            && StringUtils.containsOneOf(c.identifier.toLowerCase(), COMPONENT_COMMON.ADDRESS_KEYWORDS);
    });
    if ( addressField ) {
        updateProfileComponent.score += 2;
    }
    const addressDetail = "\nAddress field: " + ((addressField) ? "OK" : "KO"); 
    winston.debug("determineUpdateProfileComponent.computeFormScore - " + addressDetail);
    updateProfileComponent.detail += addressDetail;

    // City field (2 points)
    const cityField = form.children.find(c => {
        const attType = c.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.TYPE });
        return c.type === CONTROL_TYPE.INPUT 
            && ((attType && attType.value && (attType.value.trim() === "" || attType.value.toLowerCase() === INPUT_TYPE.TEXT)) || !attType)
            && StringUtils.containsOneOf(c.identifier.toLowerCase(), COMPONENT_COMMON.CITY_KEYWORDS);
    });
    if ( cityField ) {
        updateProfileComponent.score += 2;
    }
    const cityDetail = "\nCity field: " + ((cityField) ? "OK" : "KO"); 
    winston.debug("determineUpdateProfileComponent.computeFormScore - " + cityDetail);
    updateProfileComponent.detail += cityDetail;

    // Submit button (4 points)
    const submitButton = form.children.find(c => {
        const attType = c.attributes.find(a => { return a.key && a.key.toLowerCase() === ATTRIBUTE_NAME.TYPE });
        const attValue = c.attributes.find(a => { return a.key && a.key.toLowerCase() === ATTRIBUTE_NAME.VALUE });
        return c.type === CONTROL_TYPE.INPUT 
            && attType && attType.value && attType.value.toLowerCase() === INPUT_TYPE.SUBMIT
            && ( attValue && attValue.value && StringUtils.containsOneOf(attValue.value.toLowerCase(), COMPONENT_COMMON.UPDATE_KEYWORDS) 
                || c.text && StringUtils.containsOneOf(c.text.toLowerCase(), COMPONENT_COMMON.UPDATE_KEYWORDS))
    });
    if ( submitButton ) {
        updateProfileComponent.score += 4;
    }
    const submitDetail = "\nSubmit button: " + ((submitButton) ? "OK" : "KO");
    winston.debug("determineUpdateProfileComponent.computeFormScore - " + submitDetail);
    updateProfileComponent.detail += submitDetail;

    updateProfileComponent.score /= 29.0;
    winston.debug("determineUpdateProfileComponent.computeFormScore - Score: " + updateProfileComponent.score);

    return updateProfileComponent;
}