import * as winston from "winston";
import { IControl } from "../../common/interface/control";
import { CONTROL_TYPE, ATTRIBUTE_NAME, COMPONENT_COMMON, COMPONENT_TYPE, HTTP_METHOD, INPUT_TYPE } from "../../common/constantes";
import { StringUtils } from "../../common/utils/StringUtils";
import { IComponent } from "../../common/interface/component";
import { IRequest } from "../../common/interface/request";
import { IResource } from "../../common/interface/resource";
import { ComponentService } from "../../services/componentService";

export const determineLoginComponent = (args : any[]) => {
    winston.debug("determineLoginComponent");
    const request = args[0];
    const resource = args[1];
    const controls = args[2];

    // Compute the login component
    const loginComponent = computeFormsScore(request, resource, controls);

    // Create component
    if ( loginComponent ) {
        ComponentService.createComponent(loginComponent);
    }
};


/**
 * Compute the component score from page forms
 * @param request request
 * @param resource resource
 * @param forms forms to analyze
 */
function computeFormsScore(request: IRequest, resource: IResource, controls : IControl[]) : IComponent {
    
    // Filter controls to collect only forms
    const forms : IControl[] = controls.filter(f => { return f.type === CONTROL_TYPE.FORM });

    // Compute the first element
    var currentComponent : IComponent = null;
    if ( forms.length > 0 ) {
        currentComponent = computeFormScore(request, resource, forms[0], controls);
    }

    // Compute the remaining elements
    for ( var i = 1; i < forms.length; i++) {
        const newComponent = computeFormScore(request, resource, forms[i], controls);
        if ( newComponent.score > currentComponent.score ) {
            currentComponent = newComponent;
        }
    }

    return currentComponent;
}

/**
 * Compute the form score
 * @param request request
 * @param resource resource
 * @param form form to analyze
 * @param controls controls to analyze
 */
function computeFormScore(request: IRequest, resource: IResource, form : IControl, controls : IControl[]) : IComponent {

    const component : IComponent = {
        type: COMPONENT_TYPE.LOGIN,
        html: form.html,
        score: 0,
        detail: "",
        resourceId: resource.id
    }

    // Html title ( 2 points )
    var isLoginTitle = StringUtils.containsOneOf(resource.title, COMPONENT_COMMON.LOGIN_URL);
    if ( isLoginTitle ) {
        component.score += 2;
    }
    const htmlTitleDetail = "HtmlTitle: " + ((isLoginTitle) ? "OK" : "KO");
    winston.debug("determineLoginComponent.computeFormScore - " + htmlTitleDetail);
    component.detail += htmlTitleDetail;

    // Html header (1 point)
    const loginHeaderTag = controls.find(c => {
        return StringUtils.equalsOneOf(c.type, [ CONTROL_TYPE.H1, CONTROL_TYPE.H2, CONTROL_TYPE.H3, CONTROL_TYPE.H4, CONTROL_TYPE.H5, CONTROL_TYPE.H6 ])
            && StringUtils.containsOneOf(c.text.toLowerCase(), COMPONENT_COMMON.LOGIN_URL )
    });
    if ( loginHeaderTag ) {
        component.score ++;
    }
    const htmlHeaderDetail = "\nHtml header: " + ((loginHeaderTag) ? "OK" : "KO");
    component.detail += htmlHeaderDetail;

    // Url (2 points)
    var isLoginUrl = false;
    const actionAtt = form.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.ACTION });
    if ( actionAtt && actionAtt.value.trim() !== "" && actionAtt.value !== "#" &&
        StringUtils.containsOneOf(actionAtt.value, COMPONENT_COMMON.LOGIN_URL)) {
        component.score += 2;
        isLoginUrl = true;
    } else if ( StringUtils.containsOneOf(request.url, COMPONENT_COMMON.LOGIN_URL)) {
        component.score += 2;
        isLoginUrl = true;
    }
    const urlDetail = "\nUrl: " + ((isLoginUrl) ? "OK" : "KO");
    winston.debug("determineLoginComponent.computeFormScore - " + urlDetail);
    component.detail += urlDetail;

    // Form method ( 3 points )
    const methodAtt = form.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.METHOD && a.value.toUpperCase() === HTTP_METHOD.POST });
    if ( methodAtt ) {
        component.score += 3;
    }
    const methodDetail = "\nPOST Method: " + ((methodAtt) ? "OK" : "KO");
    winston.debug("determineLoginComponent.computeFormScore - " + methodDetail);
    component.detail += methodDetail;

    // Text input ( 5 points )
    const loginTextInput = form.children.find(c => {
        const attType = c.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.TYPE });
        return c.type === CONTROL_TYPE.INPUT 
            && ((attType && attType.value && (attType.value.trim() === "" || attType.value.toLowerCase() === INPUT_TYPE.TEXT)) || !attType)
            && StringUtils.containsOneOf(c.identifier.toLowerCase(), COMPONENT_COMMON.LOGIN_KEYWORDS)
    });
    if ( loginTextInput ) {
        component.score += 5;
    }
    const loginInputDetail = "\nLogin input field: " + ((loginTextInput) ? "OK" : "KO"); 
    winston.debug("determineLoginComponent.computeFormScore - " + loginInputDetail);
    component.detail += loginInputDetail;

    // Password input ( 5 points )
    const passwordInput = form.children.find(c => {
        const attType = c.attributes.find(a => { return a.key && a.key.toLowerCase() === ATTRIBUTE_NAME.TYPE });
        return c.type === CONTROL_TYPE.INPUT 
            && attType && attType.value && attType.value.toLowerCase() === INPUT_TYPE.PASSWORD
            && StringUtils.containsOneOf(c.identifier.toLowerCase(), COMPONENT_COMMON.PASSWORD_KEYWORDS)
    });
    if ( passwordInput ) {
        component.score += 5;
    }
    const passwordInputDetail = "\nPassword input field: " + ((passwordInput) ? "OK" : "KO");
    winston.debug("determineLoginComponent.computeFormScore - " + passwordInputDetail);
    component.detail += passwordInputDetail;

    // Submit input ( 4 points )
    const submitInput = form.children.find(c => {
        const attType = c.attributes.find(a => { return a.key && a.key.toLowerCase() === ATTRIBUTE_NAME.TYPE });
        const attValue = c.attributes.find(a => { return a.key && a.key.toLowerCase() === ATTRIBUTE_NAME.VALUE });
        return c.type === CONTROL_TYPE.INPUT 
            && attType && attType.value && attType.value.toLowerCase() === INPUT_TYPE.SUBMIT
            && ( attValue && attValue.value && StringUtils.containsOneOf(attValue.value.toLowerCase(), COMPONENT_COMMON.LOGIN_SUBMIT_KEYWORD) 
                || c.text && StringUtils.containsOneOf(c.text.toLowerCase(), COMPONENT_COMMON.LOGIN_SUBMIT_KEYWORD))
    });
    if ( submitInput ) {
        component.score += 4;
    }
    const submitDetail = "\nSubmit input field: " + ((submitInput) ? "OK" : "KO");
    winston.debug("determineLoginComponent.computeFormScore - " + submitDetail);
    component.detail += submitDetail;

    // Checkbox ( 2 points )
    const chkInput = form.children.find(c => {
        const attType = c.attributes.find(a => { return a.key && a.key.toLowerCase() === ATTRIBUTE_NAME.TYPE });
        return c.type === CONTROL_TYPE.INPUT 
            && attType && attType.value && attType.value.toLowerCase() === INPUT_TYPE.CHECKBOX
            && c.text && StringUtils.containsOneOf(c.text.toLowerCase(), COMPONENT_COMMON.LOGIN_REMEMBER_KEYWORD)
    });
    if ( chkInput ) {
        component.score += 2;
    }
    const checkBoxDetail = "\nCheckbox input field: " + ((chkInput) ? "OK" : "KO");
    winston.debug("determineLoginComponent.computeFormScore - " + checkBoxDetail);
    component.detail += checkBoxDetail;

    component.score /= 24.0; 
    winston.debug("determineLoginComponent.computeFormScore - Score: " + component.score);

    return component;
}