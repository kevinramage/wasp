import * as winston from "winston";
import { ComponentService } from "../../services/componentService";

export const determineOrderComponent = (args : any[]) => {
    winston.debug("determineOrderComponent");
    const request = args[0];
    const resource = args[1];
    const controls = args[2];

    // Compute the order component
    const orderComponent = computeFormsScore();

    // Create component
    if ( orderComponent ) {
        ComponentService.createComponent(orderComponent);
    }
};

function computeFormsScore() {
    return null;
}