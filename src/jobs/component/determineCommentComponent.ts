import * as winston from "winston";
import { ControlService } from "../../services/controlService";
import { ComponentService } from "../../services/componentService";
import { IRequest } from "../../common/interface/request";
import { IResource } from "../../common/interface/resource";
import { IControl } from "../../common/interface/control";
import { IComponent } from "../../common/interface/component";
import { CONTROL_TYPE, COMPONENT_TYPE, COMPONENT_COMMON, ATTRIBUTE_NAME, HTTP_METHOD, INPUT_TYPE } from "../../common/constantes";
import { StringUtils } from "../../common/utils/StringUtils";

export const determineCommentComponent = (args : any[]) => {
    winston.debug("determineCommentComponent");
    const request = args[0];
    const resource = args[1];
    const controls = args[2];

    // Compute the forget password component
    const commentComponent = computeFormsScore(request, resource, controls);

    // Create component
    if ( commentComponent ) {
        ComponentService.createComponent(commentComponent);
    }
}

/**
 * Compute the component score from page forms
 * @param request request
 * @param resource resource
 * @param forms forms to analyze
 */
function computeFormsScore(request: IRequest, resource: IResource, controls : IControl[]) : IComponent {
    
    // Filter controls to collect only forms
    const forms : IControl[] = controls.filter(f => { return f.type === CONTROL_TYPE.FORM });

    // Compute the first element
    var currentComponent : IComponent = null;
    if ( forms.length > 0 ) {
        currentComponent = computeFormScore(request, resource, forms[0], controls);
    }

    // Compute the remaining elements
    for ( var i = 1; i < forms.length; i++) {
        const newComponent = computeFormScore(request, resource, forms[i], controls);
        if ( newComponent.score > currentComponent.score ) {
            currentComponent = newComponent;
        }
    }

    return currentComponent;
}

/**
 * Compute the form score
 * @param request request
 * @param resource resource
 * @param form form to analyze
 * @param controls controls to analyze
 */
function computeFormScore(request: IRequest, resource: IResource, form : IControl, controls : IControl[]) : IComponent {

    const component : IComponent = {
        type: COMPONENT_TYPE.COMMENT,
        html: form.html,
        score: 0,
        detail: "",
        resourceId: resource.id
    }

    // Title (2 points)
    const isCommentTitle = StringUtils.containsOneOf(resource.title, COMPONENT_COMMON.COMMENT_KEYWORDS);
    if ( isCommentTitle ) {
        component.score += 2;
    }
    const titleDetail = "Title: " + ((isCommentTitle) ? "OK" : "KO");
    winston.debug("determineLoginComponent.computeFormScore - " + titleDetail);
    component.detail += titleDetail;

    // HTML headers (1 point)
    const isCommentHeader = controls.find(c => {
        return StringUtils.equalsOneOf(c.type, [ CONTROL_TYPE.H1, CONTROL_TYPE.H2, CONTROL_TYPE.H3, CONTROL_TYPE.H4, CONTROL_TYPE.H5, CONTROL_TYPE.H6 ])
                && StringUtils.containsOneOf(c.text.toLowerCase(), COMPONENT_COMMON.COMMENT_KEYWORDS )
    });
    if ( isCommentHeader ) {
        component.score++;
    }
    const headerDetail = "\nHeader: " + ((isCommentHeader) ? "OK" : "KO");
    winston.debug("determineLoginComponent.computeFormScore - " + headerDetail);
    component.detail += headerDetail;

    // Url (2 points)
    var isCommentUrl = false;
    const actionAtt = form.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.ACTION });
    if ( actionAtt && actionAtt.value.trim() !== "" && actionAtt.value !== "#" &&
        StringUtils.containsOneOf(actionAtt.value, COMPONENT_COMMON.COMMENT_KEYWORDS)) {
        component.score += 2;
        isCommentUrl = true;
    } else if ( StringUtils.containsOneOf(request.url, COMPONENT_COMMON.COMMENT_KEYWORDS)) {
        component.score += 2;
        isCommentUrl = true;
    }
    const urlDetail = "\nUrl: " + ((isCommentUrl) ? "OK" : "KO");
    winston.debug("determineLoginComponent.computeFormScore - " + urlDetail);
    component.detail += urlDetail;

    // Post method (3 points)
    const commentForm = form.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.METHOD && a.value.toUpperCase() === HTTP_METHOD.POST });
    if ( commentForm ) {
        component.score += 3;
    }
    const formDetail = "\nForm: " + ((commentForm) ? "OK" : "KO");
    winston.debug("determineLoginComponent.computeFormScore - " + formDetail);
    component.detail += formDetail;

    // Textarea (3 points)
    const commentTextArea = form.children.find(c => {
        return c.type === CONTROL_TYPE.TEXTAREA 
            && StringUtils.containsOneOf(c.identifier.toLowerCase(), COMPONENT_COMMON.COMMENT_KEYWORDS)
    });
    if ( commentTextArea ) {
        component.score += 3;
    }
    const textAreaDetail = "\nTextarea: " + ((commentTextArea) ? "OK" : "KO");
    winston.debug("determineLoginComponent.computeFormScore - " + textAreaDetail);
    component.detail += textAreaDetail;

    // Submit button (3 points)
    const commentSubmit = form.children.find(c => {
        const attType = c.attributes.find(a => { return a.key && a.key.toLowerCase() === ATTRIBUTE_NAME.TYPE });
        return c.type === CONTROL_TYPE.INPUT 
            && attType && attType.value && attType.value.toLowerCase() === INPUT_TYPE.SUBMIT
            && StringUtils.containsOneOf(c.identifier.toLowerCase(), COMPONENT_COMMON.ADDCOMMENT_KEYWORDS)
    });
    if ( commentSubmit ) {
        component.score += 3;
    }
    const submitDetail = "\nSubmit: " + ((commentSubmit) ? "OK" : "KO");
    winston.debug("determineLoginComponent.computeFormScore - " + submitDetail);
    component.detail += submitDetail;

    component.score /= 14.0; 
    winston.debug("determineLoginComponent.computeFormScore - Score: " + component.score);

    return component;
}