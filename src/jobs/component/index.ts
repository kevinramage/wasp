import * as winston from "winston";
import { EventManager } from "../../loaders/EventManager";

export const determineComponents = (args : any[]) => {
    winston.debug("determineComponents");
    const request = args[0];
    const resource = args[1];
    const controls = args[2];

    EventManager.instance.emit("determineLoginComponent", request, resource, controls);
    EventManager.instance.emit("determineLogoutComponent", request, resource, controls);
    EventManager.instance.emit("determineUpdateProfileComponent", request, resource, controls);
    EventManager.instance.emit("determineOrderComponent", request, resource, controls);
    EventManager.instance.emit("determineForgetPasswordComponent", request, resource, controls);
    EventManager.instance.emit("determineCommentComponent", request, resource, controls);
    EventManager.instance.emit("determineSearchComponent", request, resource, controls);
};