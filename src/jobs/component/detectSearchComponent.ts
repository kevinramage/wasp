import * as winston from "winston";
import { ControlService } from "../../services/controlService";
import { ComponentService } from "../../services/componentService";
import { IRequest } from "../../common/interface/request";
import { IResource } from "../../common/interface/resource";
import { IControl } from "../../common/interface/control";
import { IComponent } from "../../common/interface/component";
import { CONTROL_TYPE, COMPONENT_TYPE, ATTRIBUTE_NAME, COMPONENT_COMMON, HTTP_METHOD, INPUT_TYPE } from "../../common/constantes";
import { StringUtils } from "../../common/utils/StringUtils";

export const determineSearchComponent = (args : any[]) => {
    winston.debug("determineSearchComponent");
    const request = args[0];
    const resource = args[1];
    const controls = args[2];

    // Compute the search component
    const searchComponent = computeFormsScore(request, resource, controls);

    // Create component
    if ( searchComponent ) {
        ComponentService.createComponent(searchComponent);
    }
};

function computeFormsScore(request: IRequest, resource: IResource, controls : IControl[]) : IComponent {
    
    // Filter controls to collect only forms
    const forms : IControl[] = controls.filter(f => { return f.type === CONTROL_TYPE.FORM });

    // Compute the first element
    var currentComponent : IComponent = null;
    if ( forms.length > 0 ) {
        currentComponent = computeFormScore(request, resource, forms[0], controls);
    }

    // Compute the remaining elements
    for ( var i = 1; i < forms.length; i++) {
        const newComponent = computeFormScore(request, resource, forms[i], controls);
        if ( newComponent.score > currentComponent.score ) {
            currentComponent = newComponent;
        }
    }

    return currentComponent;
}

function computeFormScore(request: IRequest, resource: IResource, form: IControl, controls: IControl[]) : IComponent{
    const searchComponent : IComponent = {
        type: COMPONENT_TYPE.SEARCH,
        html: form.html,
        score: 0,
        detail: "",
        resourceId: resource.id
    };

    // Form action (2 points)
    const formActionAttribute = form.attributes.find(a => { return a.key && a.key.toLowerCase() === ATTRIBUTE_NAME.ACTION });
    const isSearchAction = formActionAttribute && StringUtils.containsOneOf(formActionAttribute.value, COMPONENT_COMMON.SEARCH_KEYWORDS);
    if ( isSearchAction ) {
        searchComponent.score += 2;
    }
    const formActionDetail = "Form action: " + ((isSearchAction) ? "OK" : "KO");
    winston.debug("determineSearchComponent.computeFormScore - " + formActionDetail);
    searchComponent.detail += formActionDetail;

    // Text input (5 points)
    const textInputAction = form.children.find(c => {
        const attType = c.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.TYPE });
        return c.type === CONTROL_TYPE.INPUT 
            && ((attType && attType.value && (attType.value.trim() === "" || attType.value.toLowerCase() === INPUT_TYPE.TEXT)) || !attType)
            && StringUtils.containsOneOf(c.identifier.toLowerCase(), COMPONENT_COMMON.SEARCH_KEYWORDS);
    });;
    if ( textInputAction ) {
        searchComponent.score += 5;
    }
    const textInputDetail = "\nText input: " + ((textInputAction) ? "OK" : "KO");
    winston.debug("determineSearchComponent.computeFormScore - " + textInputDetail);
    searchComponent.detail += textInputDetail;

    // Submit input (4 points)
    const submitAction = form.children.find(c => {
        const attType = c.attributes.find(a => { return a.key && a.key.toLowerCase() === ATTRIBUTE_NAME.TYPE });
        const attValue = c.attributes.find(a => { return a.key && a.key.toLowerCase() === ATTRIBUTE_NAME.VALUE });
        return c.type === CONTROL_TYPE.INPUT 
            && attType && attType.value && attType.value.toLowerCase() === INPUT_TYPE.SUBMIT
            && ( attValue && attValue.value && StringUtils.containsOneOf(attValue.value.toLowerCase(), COMPONENT_COMMON.SEARCH_SUBMIT_KEYWORDS) 
                || c.text && StringUtils.containsOneOf(c.text.toLowerCase(), COMPONENT_COMMON.SEARCH_SUBMIT_KEYWORDS))
    });
    if ( submitAction ) {
        searchComponent.score += 4;
    }
    const submitDetail = "\nSubmit input: " + ((submitAction) ? "OK" : "KO");
    winston.debug("determineSearchComponent.computeFormScore - " + submitDetail);
    searchComponent.detail += submitDetail;

    searchComponent.score /= 11.0;
    winston.debug("determineSearchComponent.computeFormScore - Score: " + searchComponent.score);

    return searchComponent;
}