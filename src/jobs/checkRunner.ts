import * as winston from "winston";
import { ProxyManager } from "../loaders/ProxyManager";
import { ApplicationService } from "../services/applicationService";
import { CheckRun } from "../models/checkRun";
import { CheckExecutionService } from "../services/checkExecutionService";
import { Check } from "../models/check";
import { EventManager } from "../loaders/EventManager";
import { CheckExecution } from "../models/checkExecution";
import { CHECK_TYPE } from "../common/constantes";

export async function runChecks(args: any[]) {
    winston.debug("runChecks");
    const run : CheckRun = args[0];
    const checks : Check[] = args[1];

    // Create a new session
    ApplicationService.stopProfiling();
    ApplicationService.startProfiling(ProxyManager.instance.session.applicationId);

    // Create a check run instance
    const instances = await CheckExecutionService.createExecutions(run, checks);

    // Execute 
    instances.forEach(i => {
        EventManager.instance.emit("RunCheck", i);
    });
}

export function runCheck(args: any[]) {
    winston.debug("runCheck");
    const instance : CheckExecution = args[0];
    switch ( instance.check.type ) {
        case CHECK_TYPE.PAGE_ENABLED:
            break;
    }
}
