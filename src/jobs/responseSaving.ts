import * as winston from "winston";
import { EventManager } from "../loaders/EventManager";
import { IRequest } from "../common/interface/request";
import { IResponse } from "../common/interface/response";
import { RequestService } from "../services/requestService";

/**
 * Job to save response received
 * @param args args
 */
export function responseSaving (args : any[]) : void {
    winston.debug("responseSaving");
    const requestDTO : IRequest = args[0];
    const responseDTO : IResponse = args[1];

    // Create request and response
    RequestService.createRequest(requestDTO, responseDTO).then((request) => {
        EventManager.instance.emit("AnalyzeUrlType", request);
    });
}