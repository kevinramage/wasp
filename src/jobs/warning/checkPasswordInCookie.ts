import * as winston from "winston";
import { Request } from "../../models/request";
import { Resource } from "../../models/resource";
import { HTTP_METHOD, ATTRIBUTE_NAME, INPUT_TYPE, HEADER_KEY, CONTROL_TYPE } from "../../common/constantes";
import { Control } from "../../models/control";
import { IWarning } from "../../common/interface/warning";
import { ProxyManager } from "../../loaders/ProxyManager";
import { Mutex } from "async-mutex";
import { WarningService } from "../../services/warningService";
import { ControlService } from "../../services/controlService";
import { WARNING_PWDSTOREDINCOOKIE } from "../../common/warnings";

const mutex = new Mutex();

export async function checkPasswordInCookie(args : any[]) {
    winston.debug("checkPasswordInCookie");
    const request : Request = args[0];
    const resource : Resource = args[1];
    var release;
    
    // POST Request
    winston.debug("checkPasswordInCookie - Check post request");
    if ( request.method == HTTP_METHOD.POST ) {

        // Collect informations
        const bodyDecoded = analyzeBody(request.body);
        const passwordsControls = await getPasswordControls(resource.id);
        const passwordsName : string [] = passwordsControls.map(c => {
            return c.attributes.find(a => {
                return a.key.toLowerCase() == ATTRIBUTE_NAME.NAME;
            });
        }).map(a => {
            return a.value
        });
        
        // Search a common point entre control and response body
        var commonPassword = null;
        Object.keys(bodyDecoded).forEach(k => {
            if ( passwordsName.indexOf(k) != -1 ) {
                commonPassword = passwordsName;
            }
        });

        winston.debug("checkPasswordInCookie - Check common password", passwordsName);
        winston.debug("checkPasswordInCookie - Check common password", bodyDecoded);
        if ( commonPassword ) {

            // Search the response
            const setCookie = request.response.headers.find(h => { return h.key.toLowerCase() == HEADER_KEY.SETCOOKIE; });
            winston.debug("checkPasswordInCookie - Check cookie: ", setCookie ? true : false);
            if ( setCookie ) {

                // Analyze cookie
                var passwordInCookie = false;
                setCookie.value.split(";").forEach((cookie) => {
                    const values = cookie.split("=");
                    if ( values.length == 2 ) {
                        if ( values[1].indexOf(commonPassword)) {
                            passwordInCookie = true;
                        }
                    }
                });

                // Create a warning
                winston.debug("checkPasswordInCookie - Check password in cookie: ", setCookie.value);
                if ( passwordInCookie ) {
                    const warning : IWarning = {
                        title: WARNING_PWDSTOREDINCOOKIE.title,
                        risk: WARNING_PWDSTOREDINCOOKIE.risk,
                        type: WARNING_PWDSTOREDINCOOKIE.type,
                        value: setCookie.value,
                        message: WARNING_PWDSTOREDINCOOKIE.message,
                        resourceId: resource.id,
                        applicationId: ProxyManager.instance.session.applicationId
                    }
                    release = await mutex.acquire();
                    try {
                        await WarningService.createWarning(warning);
                    } finally {
                        release();
                    }
                }
            }
        }
    }
}

function analyzeBody(body : string) {
    winston.debug("checkPasswordInCookie.analyzeBody");
    const bodyDecoded : { [ key: string ] : string } = {};

    // Try to parse the content to JSON
    try {
        const jsonObj = JSON.parse(body);
        Object.keys(jsonObj).forEach(k => {
            bodyDecoded[k] = jsonObj.k + "";
        });

    // Try to parse a form based on key / value
    } catch {
        body.split("&").forEach(elt => {
            const value = elt.split("=");
            if ( value.length == 2) {
                bodyDecoded[value[0]] = value[1];
            }
        });
    }

    return bodyDecoded;
}

/**
 * Get password controls
 * @param resourceId resource id
 */
function getPasswordControls(resourceId : number) {
    winston.info("checkPasswordInCookie.getPasswordControl:" + resourceId);
    return new Promise<Control[]>(async (resolve) => {
        var controls = await ControlService.getAllControls(resourceId);
        winston.info("checkPasswordInCookie - Controls length: " + controls.length);
        controls = controls.map(c => { return c.children }).reduce((prev, current) => { return prev.concat(current); });
        winston.info("checkPasswordInCookie - children length: " + controls.length);
        const passwordControls : Control[] = controls.filter(ctrl => {
            winston.info("checkPasswordInCookie - Attribute Number: " + ctrl.attributes.length);
            const type = ctrl.attributes.find(att => {
                winston.info("checkPasswordInCookie - Attribute: ", att);
                return att.key.toLowerCase() == ATTRIBUTE_NAME.TYPE && att.value.toLowerCase() == INPUT_TYPE.PASSWORD;
            });
            winston.info("checkPasswordInCookie - Type: ", type ? type.value : null);
            return ctrl.type == CONTROL_TYPE.INPUT && type;
        });
        winston.info("checkPasswordInCookie - PWD Control: " + passwordControls.length);
        resolve(passwordControls);
    });
}