import * as winston from "winston";
import { Mutex } from "async-mutex";
import { IRequest } from "../../common/interface/request";
import { IResource } from "../../common/interface/resource";
import { HEADER_KEY } from "../../common/constantes";
import { IWarning } from "../../common/interface/warning";
import { ProxyManager } from "../../loaders/ProxyManager";
import { WarningService } from "../../services/warningService";
import { WARNING_BLOCKALLMIXEDCONTENT_NOTPRESENT } from "../../common/warnings";

const mutex = new Mutex();

export async function checkBlockAllMixedContent(args: any[]) {
    winston.debug("checkBlockAllMixedContent");
    const BLOCK_ALL_MIXED_CONTENT = "block-all-mixed-content";
    const requests : IRequest = args[0];
    const resource : IResource = args[1];
    var release, warningValue = "";

    // Search in Content-Security-Policy
    var cspHeader = requests.response.headers.find(h => { return h.key === HEADER_KEY.CONTENTSECURITYPOLICY });
    if ( cspHeader ) {
        if ( cspHeader.value.toString().toLowerCase().indexOf(BLOCK_ALL_MIXED_CONTENT) === -1) {
            warningValue = "Content-Security-Policy"
        }
    }

    // Search in X-Content-Security-Policy
    if ( warningValue ) {
        cspHeader = requests.response.headers.find(h => { return h.key === HEADER_KEY.XCONTENTSECURITYPOLICY });
        if ( cspHeader ) {
            if ( cspHeader.value.toString().toLowerCase().indexOf(BLOCK_ALL_MIXED_CONTENT) === -1) {
                warningValue = "X-Content-Security-Policy"
            }
        }
    }

    // Warning
    if ( warningValue !== "") {
        const warning : IWarning = {
            title: WARNING_BLOCKALLMIXEDCONTENT_NOTPRESENT.title,
            risk: WARNING_BLOCKALLMIXEDCONTENT_NOTPRESENT.risk,
            type: WARNING_BLOCKALLMIXEDCONTENT_NOTPRESENT.type,
            message: WARNING_BLOCKALLMIXEDCONTENT_NOTPRESENT.message,
            value: warningValue,
            resourceId: resource.id,
            applicationId: ProxyManager.instance.session.applicationId
        };
        release = await mutex.acquire();
        try {
            await WarningService.createWarning(warning);
        } finally {
            release();
        }
    }
}