import * as winston from "winston";
import { IRequest } from "../../common/interface/request";
import { AbstractCheckPageEnabled } from "./abstractCheckPageEnabled";
import { WARNING_SERVERSTATUS_DIVULGATION } from "../../common/warnings";
import { IResource } from "../../common/interface/resource";

export function checkServerStatusPage(args: any[]) {
    winston.debug("checkServerStatusPage");
    const request : IRequest = args[0];
    const resource : IResource = args[1];
    
    // Check page
    const checkPage = new AbstractCheckPageEnabled("server-status", WARNING_SERVERSTATUS_DIVULGATION);
    checkPage.check(request, resource);
}