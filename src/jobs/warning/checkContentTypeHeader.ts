import * as winston from "winston";
import { IRequest } from "../../common/interface/request";
import { IResource } from "../../common/interface/resource";
import { AbstractHeaderNonPresent } from "./abstractHeaderNonPresent";
import { HEADER_KEY } from "../../common/constantes";
import { WARNING_CONTENTTYPE_MISSING } from "../../common/warnings";

export function checkContentTypeHeader(args: any[]) {
    winston.debug("checkContentTypeHeader");
    const request : IRequest = args[0];
    const resource : IResource = args[1];

    // Check header
    const checkHeader = new AbstractHeaderNonPresent(HEADER_KEY.CONTENT_TYPE, WARNING_CONTENTTYPE_MISSING);
    checkHeader.check(request, resource);
}