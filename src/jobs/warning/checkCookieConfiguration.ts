import * as winston from "winston";
import { IRequest } from "../../common/interface/request";
import { IResource } from "../../common/interface/resource";
import { Mutex } from "async-mutex";
import { HEADER_KEY } from "../../common/constantes";
import { WarningService } from "../../services/warningService";
import { IWarning } from "../../common/interface/warning";
import { ProxyManager } from "../../loaders/ProxyManager";
import { WARNING_COOKIESECURE_NOTPRESENT, WARNING_COOKIEHTTPONLY_NOTPRESENT } from "../../common/warnings";

const mutex = new Mutex();

export async function checkCookieConfiguration(args: any[]) {
    winston.debug("checkCookieConfiguration");
    const COOKIE_SECURE = "secure";
    const COOKIE_HTTPONLY = "httponly";
    const requests : IRequest = args[0];
    const resource : IResource = args[1];
    var release, cookieSecure = "", cookieHttpOnly = "";

    // Identify configuration issues in set-cookie header
    const setCookieHeader = requests.response.headers.find(h => { return h.key === HEADER_KEY.SETCOOKIE });
    if ( setCookieHeader ) {

        // Extract keys
        const keys = setCookieHeader.value.toString().split(";").map(v => {
            return v.split("=")[0].toLowerCase();
        });

        // Cookie Secure
        if ( cookieSecure === "" && keys.indexOf(COOKIE_SECURE) === -1) {
            cookieSecure = setCookieHeader.value.toString();
        }

        // Cookie HttpOnly
        if ( cookieHttpOnly === "" && keys.indexOf(COOKIE_HTTPONLY) === -1) {
            cookieHttpOnly = setCookieHeader.value.toString();
        }

        // Warning
        if (cookieSecure !== "" || cookieHttpOnly !== "" ) {
            release = await mutex.acquire();
            try {
                var warning : IWarning;

                // Cookie secure
                if ( cookieSecure ) {
                    warning = {
                        title: WARNING_COOKIESECURE_NOTPRESENT.title,
                        risk: WARNING_COOKIESECURE_NOTPRESENT.risk,
                        type: WARNING_COOKIESECURE_NOTPRESENT.type,
                        value: cookieSecure,
                        message: WARNING_COOKIESECURE_NOTPRESENT.message,
                        resourceId: resource.id,
                        applicationId: ProxyManager.instance.session.applicationId
                    }
                    await WarningService.createWarning(warning);
                }
                // Cookie HttpOnly
                if ( cookieHttpOnly ) {
                    warning = {
                        title:WARNING_COOKIEHTTPONLY_NOTPRESENT.title,
                        risk: WARNING_COOKIEHTTPONLY_NOTPRESENT.risk,
                        type: WARNING_COOKIEHTTPONLY_NOTPRESENT.type,
                        value: cookieHttpOnly,
                        message: WARNING_COOKIEHTTPONLY_NOTPRESENT.message,
                        resourceId: resource.id,
                        applicationId: ProxyManager.instance.session.applicationId
                    }
                    await WarningService.createWarning(warning);
                }
                
            } finally {
                release();
            }
        }
    }
}