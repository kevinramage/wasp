import * as winston from "winston";
import { IRequest } from "../../common/interface/request";
import { HEADER_KEY } from "../../common/constantes";
import { IResource } from "../../common/interface/resource";
import { IWarning } from "../../common/interface/warning";
import { ProxyManager } from "../../loaders/ProxyManager";
import { WarningService } from "../../services/warningService";
import { Mutex } from "async-mutex";
import { WARNING_CSP_NOTPRESENT, WARNING_XCSP_NOTPRESENT } from "../../common/warnings";

const mutex = new Mutex();

export async function checkContentSecurityPolicy(args: any[]) {
    winston.debug("checkContentSecurityPolicy");
    const requests : IRequest = args[0];
    const resource : IResource = args[1];
    var release;

    // Content Security Policy
    var cspHeader = requests.response.headers.find(h => { return h.key === HEADER_KEY.CONTENTSECURITYPOLICY });
    if ( !cspHeader ) {
        const warning : IWarning = {
            title: WARNING_CSP_NOTPRESENT.title,
            risk: WARNING_CSP_NOTPRESENT.risk,
            type: WARNING_CSP_NOTPRESENT.type,
            message: WARNING_CSP_NOTPRESENT.message,
            value: "",
            resourceId: resource.id,
            applicationId: ProxyManager.instance.session.applicationId
        };
        release = await mutex.acquire();
        try {
            await WarningService.createWarning(warning);
        } finally {
            release();
        }
    }

    // X Content Security Policy
    cspHeader = requests.response.headers.find(h => { return h.key === HEADER_KEY.XCONTENTSECURITYPOLICY });
    if ( !cspHeader ) {
        const warning : IWarning = {
            title: WARNING_XCSP_NOTPRESENT.title,
            risk: WARNING_XCSP_NOTPRESENT.risk,
            type: WARNING_XCSP_NOTPRESENT.type,
            message: WARNING_XCSP_NOTPRESENT.message,
            value: "",
            resourceId: resource.id,
            applicationId: ProxyManager.instance.session.applicationId
        };
        release = await mutex.acquire();
        try {
            await WarningService.createWarning(warning);
        } finally {
            release();
        }
    }
}