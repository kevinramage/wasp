import * as winston from "winston";
import { IRequest } from "../../common/interface/request";
import { IResource } from "../../common/interface/resource";
import { IWarning } from "../../common/interface/warning";
import { ProxyManager } from "../../loaders/ProxyManager";
import { Mutex } from "async-mutex";
import { WarningService } from "../../services/warningService";

const mutex = new Mutex();

export class AbstractCheckPageEnabled {

    private _path : string;
    private _warning : any;

    /**
     * 
     * @param path path
     * @param warning warning
     */
    constructor(path, warning) {
        this._path = path;
        this._warning = warning;
    }

    async check(request: IRequest, resource: IResource) {
        winston.debug("AbstractCheckPageEnabled.check");
        if ( request.url.toLowerCase().endsWith(this._path) && request.response.statusCode === 200) {
            const warning : IWarning = {
                title: this._warning.title,
                risk: this._warning.risk,
                type: this._warning.type,
                message: this._warning.message,
                value: "",
                resourceId: resource.id,
                applicationId: ProxyManager.instance.session.applicationId
            };
            var release = await mutex.acquire();
            try {
                await WarningService.createWarning(warning);
            } finally {
                release();
            }
        }
    }
}