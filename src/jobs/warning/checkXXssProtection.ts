import * as winston from "winston";
import { IRequest } from "../../common/interface/request";
import { IResource } from "../../common/interface/resource";
import { Mutex } from "async-mutex";
import { HEADER_KEY } from "../../common/constantes";
import { IWarning } from "../../common/interface/warning";
import { ProxyManager } from "../../loaders/ProxyManager";
import { WarningService } from "../../services/warningService";
import { WARNING_XXSSPROTECTION_NOTPRESENT } from "../../common/warnings";

const mutex = new Mutex();

export async function checkXXssProtection(args: any[]) {
    winston.debug("checkXXssProtection");
    const requests : IRequest = args[0];
    const resource : IResource = args[1];
    var release;

    // Content Security Policy
    var cspHeader = requests.response.headers.find(h => { return h.key === HEADER_KEY.XXSSPROTECTION });
    if ( !cspHeader || cspHeader.value === "0" ) {
        const warning : IWarning = {
            title: WARNING_XXSSPROTECTION_NOTPRESENT.title,
            risk: WARNING_XXSSPROTECTION_NOTPRESENT.risk,
            type: WARNING_XXSSPROTECTION_NOTPRESENT.type,
            message: WARNING_XXSSPROTECTION_NOTPRESENT.message,
            value: cspHeader ? cspHeader.value.toString() : "",
            resourceId: resource.id,
            applicationId: ProxyManager.instance.session.applicationId
        };
        release = await mutex.acquire();
        try {
            await WarningService.createWarning(warning);
        } finally {
            release();
        }
    }
}