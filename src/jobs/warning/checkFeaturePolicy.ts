import * as winston from "winston";
import { Mutex } from "async-mutex";
import { IRequest } from "../../common/interface/request";
import { IResource } from "../../common/interface/resource";
import { HEADER_KEY } from "../../common/constantes";
import { IWarning } from "../../common/interface/warning";
import { ProxyManager } from "../../loaders/ProxyManager";
import { WarningService } from "../../services/warningService";
import { WARNING_FEATUREPOLICY_NOTPRESENT } from "../../common/warnings";
import { AbstractHeaderNonPresent } from "./abstractHeaderNonPresent";

const mutex = new Mutex();

export async function checkFeaturePolicy(args: any[]) {
    winston.debug("checkFeaturePolicy");
    const requests : IRequest = args[0];
    const resource : IResource = args[1];

    // Check header
    const checkHeader = new AbstractHeaderNonPresent(HEADER_KEY.FEATUREPOLICY, WARNING_FEATUREPOLICY_NOTPRESENT);
    checkHeader.check(requests, resource);
}