import * as winston from "winston";
import { IRequest } from "../../common/interface/request";
import { AbstractCheckPageEnabled } from "./abstractCheckPageEnabled";
import { WARNING_PHPINFO_DIVULGATION } from "../../common/warnings";
import { IResource } from "../../common/interface/resource";

export function checkPhpInfoPage(args: any[]) {
    winston.debug("checkPhpInfoPage");
    const request : IRequest = args[0];
    const resource : IResource = args[1];

    // Check page
    const checkPage = new AbstractCheckPageEnabled("phpinfo.php", WARNING_PHPINFO_DIVULGATION);
    checkPage.check(request, resource);
}