import * as winston from "winston";
import { IRequest } from "../../common/interface/request";
import { IResource } from "../../common/interface/resource";
import { AbstractCheckPageEnabled } from "./abstractCheckPageEnabled";
import { WARNING_SERVERINFO_DIVULGATION } from "../../common/warnings";



export function checkServerInfo(args: any[]) {
    winston.debug("checkServerInfo");
    const request : IRequest = args[0];
    const resource : IResource = args[1];

    const checkPage = new AbstractCheckPageEnabled("server-info", WARNING_SERVERINFO_DIVULGATION);
    checkPage.check(request, resource);
}