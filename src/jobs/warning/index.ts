import * as winston from "winston";
import { EventManager } from "../../loaders/EventManager";

/**
 * Job to check some behaviour and create warnings
 * @param args args
 */
export function checkWarnings(args : any[]) : void {
    winston.debug("checkWarnings");
    const request = args[0];
    const resource = args[1];
    //EventManager.instance.emit("CheckPasswordInCookie", request, resource);
    EventManager.instance.emit("CheckContentSecurityPolicy", request, resource);
    EventManager.instance.emit("CheckUpgradeInsecureRequests", request, resource);
    EventManager.instance.emit("CheckBlockAllMixedContent", request, resource);
    EventManager.instance.emit("CheckFeaturePolicy", request, resource);
    EventManager.instance.emit("CheckCookieConfiguration", request, resource);
    EventManager.instance.emit("CheckReferrerPolicy", request, resource);
    EventManager.instance.emit("CheckXFrameOptions", request, resource);
    EventManager.instance.emit("CheckXXssProtection", request, resource);
    EventManager.instance.emit("CheckStackTraceDivulgation", request, resource);
    EventManager.instance.emit("CheckServerInfo", request, resource);
    EventManager.instance.emit("CheckServerStatusPage", request, resource);
    EventManager.instance.emit("CheckPhpInfoPage", request, resource);
    EventManager.instance.emit("CheckContentTypeHeader", request, resource);
}