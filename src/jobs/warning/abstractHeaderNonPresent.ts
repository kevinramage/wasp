import * as winston from "winston";
import { IRequest } from "../../common/interface/request";
import { Mutex } from "async-mutex";
import { IWarning } from "../../common/interface/warning";
import { IResource } from "../../common/interface/resource";
import { ProxyManager } from "../../loaders/ProxyManager";
import { WarningService } from "../../services/warningService";

const mutex = new Mutex();

export class AbstractHeaderNonPresent {

    private _headerName : string;
    private _warning : any;

    /**
     * Constructor
     * @param headerName header name to search
     * @param warning warning to write
     */
    constructor(headerName: string, warning: any) {
        this._headerName = headerName;
        this._warning = warning;
    }

    /**
     * Check the request
     * @param request request to analyze
     * @param resource resource linked
     */
    async check(request : IRequest, resource: IResource) {
        winston.debug("abstractHeaderNonPresent.check: " + this._headerName);
        const instance = this;
        var cspHeader = request.response.headers.find(h => { return h.key === instance._headerName });
        if ( !cspHeader ) {
            const warning : IWarning = {
                title: this._warning.title,
                risk: this._warning.risk,
                type: this._warning.type,
                message: this._warning.message,
                value: "",
                resourceId: resource.id,
                applicationId: ProxyManager.instance.session.applicationId
            };
            var release = await mutex.acquire();
            try {
                await WarningService.createWarning(warning);
            } finally {
                release();
            }
        }
    }
}