import * as winston from "winston";
import { IResource } from "../../common/interface/resource";
import { IRequest } from "../../common/interface/request";
import { Mutex } from "async-mutex";
import { IWarning } from "../../common/interface/warning";
import { WARNING_STACKTRACE_DIVULGATION } from "../../common/warnings";
import { ProxyManager } from "../../loaders/ProxyManager";
import { WarningService } from "../../services/warningService";

const mutex = new Mutex();

/**
 * Check the presence of stack trace on response body
 * @param args request and resource
 */
export async function checkStackTraceDivulgation(args: any[]) {
    winston.debug("checkStackTraceDivulgation");
    const request : IRequest = args[0];
    const resource : IResource = args[1];

    const regex = /.+Exception[^\n]+\n(\t+\Qat \E.+\s+)+/gi;
    if ( request.response.body.match(regex) ) {
        const warning : IWarning = {
            title: WARNING_STACKTRACE_DIVULGATION.title,
            risk: WARNING_STACKTRACE_DIVULGATION.risk,
            type: WARNING_STACKTRACE_DIVULGATION.type,
            message: WARNING_STACKTRACE_DIVULGATION.message,
            value: "",
            resourceId: resource.id,
            applicationId: ProxyManager.instance.session.applicationId
        };
        var release = await mutex.acquire();
        try {
            await WarningService.createWarning(warning);
        } finally {
            release();
        }
    }
}