import * as winston from "winston";
import { Request } from "../models/request";
import { IResource } from "../common/interface/resource";
import { HTTPUtils } from "../common/utils/HTTPUtils";
import { URLUtils } from "../common/utils/URLUtils";
import { URL_TYPE } from "../common/constantes";
import { ResourceService } from "../services/resourceService";
import { EventManager } from "../loaders/EventManager";
import { ProxyManager } from "../loaders/ProxyManager";
import { HTMLUtils } from "../common/utils/HTMLUtils";

/**
 * Job to generate resource from a request
 * @param args args
 */
export async function generateResource (args : any[]) {
    winston.debug("generateResource");
    const request : Request = args[0];
    const urlType : string = args[1];

    // Collect resource informations
    const resource : IResource = {};
    resource.url = HTTPUtils.getCompleteUrl(request);
    resource.completePathname = URLUtils.getPathName(resource.url);
    resource.pathname = URLUtils.getLastPathName(resource.url);
    resource.path = request.url;
    resource.requestId = request.id;

    // Compute the identifier
    if ( urlType === URL_TYPE.PAGE ) {
        const params = URLUtils.getQueryParams(resource.url);
        if ( params["content"]) {
            resource.identifier = params["content"];
        }
    } else {
        resource.identifier = resource.completePathname;
    }

    // Extarct HTML title
    resource.title = HTMLUtils.extractTitle(request.response.body);

    // Create a resource
    const resourceGenerated = await ResourceService.createResource(resource, ProxyManager.instance.session.id);

    // Go on the process
    EventManager.instance.emit("AnalyzeTechnologies", request, resourceGenerated);
    EventManager.instance.emit("RunCollects", request, resourceGenerated);
    EventManager.instance.emit("CheckWarnings", request, resourceGenerated);
    EventManager.instance.emit("RunChecks", request, resourceGenerated);
}