import * as winston from "winston";
import { Request } from "../models/request";
import { EntryPoint } from "../models/entryPoint";
import { InjectionType } from "../models/injectionType";
import { Payload } from "../models/payload";
import { IRequest, IRequestSimplified } from "../common/interface/request";
import { ENTRYPOINT_TYPE, INJECTIONTYPE_TYPE, HEADER_KEY, INJECTION_STATUS, INJECTION_ERRORMESSAGE, INJECTIONPAYLOAD_STATUS } from "../common/constantes";
import { RequestManager } from "../loaders/RequestManager";
import { IInjectionPayloadStatus } from "../common/interface/injectionPayloadStatus";
import { IInjectionStatus } from "../common/interface/injectionStatus";
import { IDictionnary } from "../common/interface/dictionnary";
import { IEntryPoint } from "../common/interface/entryPoint";

export class InjectionRunner {
    private _request : IRequestSimplified;
    private _entryPoints : IEntryPoint[];
    private _injectionType : InjectionType;
    private _payloads : Payload[];
    private _defaultValue : string;
    private _difference : string;
    private _reference : string;
    private _result : IInjectionStatus;

    /**
     * Constructor
     * @param request request
     * @param entryPoints entry points
     * @param injectionType injection type
     * @param payloads payloads
     */
    constructor(request: IRequestSimplified, entryPoints: IEntryPoint[], injectionType: InjectionType, payloads: Payload[],
        defaultValue: string, difference: string) {
        this._request = request;
        this._entryPoints = entryPoints;
        this._injectionType = injectionType;
        this._payloads = payloads;
        this._defaultValue = defaultValue || "";
        this._difference = difference || "100";

        // Difference default value
        try {
            Number.parseInt(this._difference);
        } catch ( err)  {
            this._difference = "100";
        }
    }

    /**
     * Inject
     */
    public inject() {
        winston.debug("InjectionRunner.inject");
        const instance = this;
        this._result = { 
            defaultValue: this._defaultValue,
            difference: this._difference,
            payloads: [] 
        };
        return new Promise<IInjectionStatus>(async (resolve) => {
            switch ( this._injectionType.type ) {
                case INJECTIONTYPE_TYPE.SQL_BOOLEAN_AND:
                case INJECTIONTYPE_TYPE.SQL_BOOLEAN_OR:
                    await instance.injectSQLBoolean();
                    resolve(instance._result);
                    break;

                default:
                    instance._result.status = INJECTION_STATUS.FAILED;
                    instance._result.errorMessage = INJECTION_ERRORMESSAGE.UNKNOWN_INJECTIONTYPE;
                    resolve(instance._result);
            }
        });
    }

    /**
     * Inject SQL Boolean injection
     */
    private injectSQLBoolean() {
        winston.debug("InjectionRunner.injectSQLBoolean");
        const instance = this;
        return new Promise<void>(async (resolve) => {

            // Reference
            try {
                const request = instance.buildRequest();
                instance.injectEntryPoints(request, instance._defaultValue);
                await instance.injectReference(request);

            } catch ( err ) {
                instance._result.errorMessage = INJECTION_ERRORMESSAGE.REFERENCE_INJECTION_FAILED;
                instance._result.status = INJECTION_STATUS.FAILED;
                resolve();
                return;
            }

            // Payloads
            instance._result.reference = instance._reference;
            for ( var index = 0; index < instance._payloads.length; index++) {
                const payloadStatus = await instance.injectSQLBooleanPayload(instance._payloads[index].payload);
                instance._result.payloads.push(payloadStatus);
            }

            // Determine the injection status
            const isSuccess = instance._result.payloads.every(p => { return p.status === INJECTIONPAYLOAD_STATUS.NOT_INJECTED; })
            instance._result.status = isSuccess ? INJECTION_STATUS.SUCCESS : INJECTION_STATUS.WARNING;

            // Injection status
            resolve();
        });
    }


    /**
     * Inject payload for SQL Boolean injection
     * @param payload payload to inject
     */
    private injectSQLBooleanPayload(payload: string) {
        winston.debug("InjectionRunner.injectSQLBooleanPayload - Start");
        const instance = this;
        return new Promise<IInjectionPayloadStatus>(async (resolve) => {

            // Build request and inject entry point
            const requestToInject : IRequest = instance.buildRequest();
            this.injectEntryPoints(requestToInject, payload);

            // Send request
            RequestManager.instance.sendRequest(requestToInject).then((response) => {
                const reference = Number.parseInt(instance._reference);
                const diff = Number.parseInt(instance._difference);
                const injected = response.body.length < reference - diff || response.body.length > reference + diff;
                requestToInject.response = null;
                const payloadStatus : IInjectionPayloadStatus = {
                    payload: payload,
                    status: injected ? INJECTIONPAYLOAD_STATUS.INJECTED : INJECTIONPAYLOAD_STATUS.NOT_INJECTED,
                    result: response.body.length + "",
                    request: requestToInject
                }
                resolve(payloadStatus);
            }).catch((err) => {
                winston.error("InjectionRunner.injectSQLBoolean - Internal error: ", err);
                requestToInject.response = null;
                const payloadStatus : IInjectionPayloadStatus = {
                    payload: payload,
                    status: INJECTIONPAYLOAD_STATUS.FAILED,
                    result: "",
                    errorMessage: err,
                    request: requestToInject
                }
                resolve(payloadStatus);
            });
        });
    }

    /**
     * Inject reference
     * @param request request
     */
    private injectReference(request: IRequest) {
        winston.debug("InjectionRunner.injectReference - Start");
        const instance = this;
        return new Promise<void>((resolve, reject) => {
            RequestManager.instance.sendRequest(request).then((response) => {
                switch ( instance._injectionType.type ) {
                    case INJECTIONTYPE_TYPE.SQL_BOOLEAN_AND:
                    case INJECTIONTYPE_TYPE.SQL_BOOLEAN_OR:
                        instance._reference = response.body.length + "";
                        break;

                    case INJECTIONTYPE_TYPE.SQL_TIME:
                        break;
                }
                resolve();
            }).catch((err) => {
                winston.error("InjectionRunner.injectReference - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Build request
     */
    private buildRequest() {
        winston.debug("InjectionRunner.buildRequest");
        const request = {
            protocol: this._request.protocol,
            host: this._request.host,
            method: this._request.method,
            url: this._request.url,
            body: this._request.body,
            headers: this._request.headers.map(h => {
                return {
                    key: h.key,
                    value: h.value
                }
            })
        }
        const hostHeader = this._request.headers.find(h => { return h.key === HEADER_KEY.HOST; });
        if ( hostHeader ) {
            //request.url = request.protocol + "://" + hostHeader.value + request.url;
        }
        return request;
    }

    /**
     * Inject entry points
     * @param request request
     * @param value value
     */
    private injectEntryPoints(request: IRequest, value: string) {
        winston.debug("InjectionRunner.injectEntryPoints");
        const instance = this;
        this._entryPoints.forEach(e => {
            instance.injectEntryPoint(request, e, value);
        });
    }

    /**
     * Inject entry point
     * @param request request
     * @param entryPoint entry point
     * @param value value
     */
    private injectEntryPoint(request : IRequest, entryPoint: IEntryPoint, value: string) {
        winston.debug("InjectionRunner.injectEntryPoint");
        switch ( entryPoint.type ) {

            // Body
            case ENTRYPOINT_TYPE.BODY:
                const fields = this.decodeEncodedBody(request.body.toString());
                if ( fields ) {
                    const field = fields.find(f => { return f.key == entryPoint.name });
                    if ( field ) {
                        field.value = value;
                    }
                    request.body = this.encodeFields(fields);
                }
            break;

            // Cookie
            case ENTRYPOINT_TYPE.COOKIE:
            break;

            // Header
            case ENTRYPOINT_TYPE.HEADER:
                if ( request.headers[entryPoint.name] ) {
                    request.headers[entryPoint.name] = value;
                } else {
                    request.headers.push({
                        key: entryPoint.name,
                        value: value
                    });
                }
            break;

            // Query
            case ENTRYPOINT_TYPE.QUERY:
                
            break;
        } 
    }

    /**
     * Decode encoded body
     * @param body body to decode
     */
    private decodeEncodedBody(body: string) : IDictionnary[] {
        try {
            const fields = body.toString().split("&");
            return fields.map(f => {
                const result = f.split("=");
                return {
                    key: result[0],
                    value: result[1]
                }
            })
        } catch ( err ) {
            return null;
        }
    }

    /**
     * Encode fields
     * @param fields fields to encode
     */
    private encodeFields(fields: IDictionnary[]) {
        return fields.map(f => { return f.key + "=" + f.value }).join("&");
    }
}