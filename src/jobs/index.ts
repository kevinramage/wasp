import { EventManager } from "../loaders/EventManager";
import { responseSaving } from "./responseSaving";
import { analyzeUrlType } from "./analyzeUrlType";
import { generateResource } from "./generateResource";
import { analyzeServerHeader } from "./technology/analyzeServerHeader";
import { analyzePoweredByHeader } from "./technology/analyzePoweredByHeader";
import { analyzeCookie } from "./technology/analyzeCookie";
import { analyzeAngularTechnology } from "./technology/analyzeAngularTechnology";
import { analyzeTechnologies } from "./technology";
import { runCollects } from "./collect";
import { collectLinks } from "./collect/collectLinks";
import { collectForms } from "./collect/collectForms";
import { collectComments } from "./collect/collectComment";
import { analyzeServerExtension } from "./technology/analyzeServerExtension";
import { checkWarnings } from "./warning";
import { checkPasswordInCookie } from "./warning/checkPasswordInCookie";
import { analyseMysqlTechnology } from "./technology/analyzeMysqlTechnology";
import { analyzeClientLibrary } from "./technology/analyseClientLibrary";
import { runChecks } from "./check";
import { checkResourceId } from "./check/checkResourceId";
import { determineComponents } from "./component";
import { determineLoginComponent } from "./component/determineLoginComponent";
import { determineLogoutComponent } from "./component/determineLogoutComponent";
import { determineUpdateProfileComponent } from "./component/determineUpdateProfileComponent";
import { determineOrderComponent } from "./component/determineOrderComponent";
import { determineForgetPasswordComponent } from "./component/determineForgetPasswordComponent";
import { determineCommentComponent } from "./component/determineCommentComponent";
import { determineSearchComponent } from "./component/detectSearchComponent";
import { checkContentSecurityPolicy } from "./warning/checkContentSecurityPolicy";
import { checkUpgradeInsecureRequests } from "./warning/checkUpgradeInsecureRequests";
import { checkBlockAllMixedContent } from "./warning/checkBlockAllMixedContent";
import { checkFeaturePolicy } from "./warning/checkFeaturePolicy";
import { checkCookieConfiguration } from "./warning/checkCookieConfiguration";
import { checkReferrerPolicy } from "./warning/checkReferrerPolicy";
import { checkXFrameOptions } from "./warning/checkXFrameOptions";
import { checkXXssProtection } from "./warning/checkXXssProtection";
import { checkStackTraceDivulgation } from "./warning/checkStackTraceDivulgation";
import { checkPhpInfoPage } from "./warning/checkPhpInfoPage";
import { checkServerStatusPage } from "./warning/checkServerStatusPage";
import { checkServerInfo } from "./warning/checkServerInfoPage";
import { checkContentTypeHeader } from "./warning/checkContentTypeHeader";
import { collectWords } from "./collect/collectWords";
import { runCheck } from "./checkRunner";
import { phpInfoPageDivulgation } from "./checkRun/phpInfoPageDivulgation";
import { serverInfoPageDivulgation } from "./checkRun/serverInfoPageDivulgation";
import { serverStatusPageDivulgation } from "./checkRun/serverStatusPageDivulgation";
import { robotsPageDivulgation } from "./checkRun/robotsPageDivulgation";
import { checkPhpInfoDivulgation } from "./check/checkPhpInfoDivulgation";
import { collecEntryPoints } from "./collect/collectEntryPoint";

export function jobFactory() {

    // Proxy
    EventManager.instance.subscribe("NewResponse", responseSaving);

    // Url type
    EventManager.instance.subscribe("AnalyzeUrlType", analyzeUrlType);

    // Generate resource
    EventManager.instance.subscribe("GenerateResource", generateResource);

    // Technology
    EventManager.instance.subscribe("AnalyzeTechnologies", analyzeTechnologies);
    EventManager.instance.subscribe("AnalyzeServerHeader", analyzeServerHeader);
    EventManager.instance.subscribe("AnalyzePoweredByHeader", analyzePoweredByHeader);
    EventManager.instance.subscribe("AnalyzeCookie", analyzeCookie);
    EventManager.instance.subscribe("AnalyzeAngularTechnology", analyzeAngularTechnology);
    EventManager.instance.subscribe("AnalyzeServerExtension", analyzeServerExtension);
    EventManager.instance.subscribe("AnalyseMysqlTechnology", analyseMysqlTechnology);
    EventManager.instance.subscribe("AnalyzeClientLibrary", analyzeClientLibrary);

    // Collect
    EventManager.instance.subscribe("RunCollects", runCollects);
    EventManager.instance.subscribe("CollectLinks", collectLinks);
    EventManager.instance.subscribe("CollectForms", collectForms);
    EventManager.instance.subscribe("CollectComments", collectComments);
    EventManager.instance.subscribe("CollectWords", collectWords);
    EventManager.instance.subscribe("CollecEntryPoints", collecEntryPoints);

    // Warning
    EventManager.instance.subscribe("CheckWarnings", checkWarnings);
    EventManager.instance.subscribe("CheckPasswordInCookie", checkPasswordInCookie);
    EventManager.instance.subscribe("CheckContentSecurityPolicy", checkContentSecurityPolicy);
    EventManager.instance.subscribe("CheckUpgradeInsecureRequests", checkUpgradeInsecureRequests);
    EventManager.instance.subscribe("CheckBlockAllMixedContent", checkBlockAllMixedContent);
    EventManager.instance.subscribe("CheckFeaturePolicy", checkFeaturePolicy);
    EventManager.instance.subscribe("CheckCookieConfiguration", checkCookieConfiguration);
    EventManager.instance.subscribe("CheckReferrerPolicy", checkReferrerPolicy);
    EventManager.instance.subscribe("CheckXFrameOptions", checkXFrameOptions);
    EventManager.instance.subscribe("CheckXXssProtection", checkXXssProtection);
    EventManager.instance.subscribe("CheckStackTraceDivulgation", checkStackTraceDivulgation);
    EventManager.instance.subscribe("CheckServerInfo", checkServerInfo);
    EventManager.instance.subscribe("CheckServerStatusPage", checkServerStatusPage);
    EventManager.instance.subscribe("CheckPhpInfoPage", checkPhpInfoPage);
    EventManager.instance.subscribe("CheckContentTypeHeader", checkContentTypeHeader);

    // Check
    EventManager.instance.subscribe("RunChecks", runChecks);
    EventManager.instance.subscribe("CheckResourceId", checkResourceId);
    EventManager.instance.subscribe("CheckPhpInfoDivulgation", checkPhpInfoDivulgation);

    // Component
    EventManager.instance.subscribe("determineComponents", determineComponents);
    EventManager.instance.subscribe("determineLoginComponent", determineLoginComponent);
    EventManager.instance.subscribe("determineLogoutComponent", determineLogoutComponent);
    EventManager.instance.subscribe("determineUpdateProfileComponent", determineUpdateProfileComponent);
    EventManager.instance.subscribe("determineOrderComponent", determineOrderComponent);
    EventManager.instance.subscribe("determineForgetPasswordComponent", determineForgetPasswordComponent);
    EventManager.instance.subscribe("determineCommentComponent", determineCommentComponent);
    EventManager.instance.subscribe("determineSearchComponent", determineSearchComponent);

    // CheckRunner
    EventManager.instance.subscribe("RunChecks", runChecks);
    EventManager.instance.subscribe("RunCheck", runCheck);

    // Check Run
    EventManager.instance.subscribe("PhpInfoPageDivulgation", phpInfoPageDivulgation);
    EventManager.instance.subscribe("RobotsPageDivulgation", robotsPageDivulgation);
    EventManager.instance.subscribe("ServerInfoPageDivulgation", serverInfoPageDivulgation);
    EventManager.instance.subscribe("ServerStatusPageDivulgation", serverStatusPageDivulgation);
}