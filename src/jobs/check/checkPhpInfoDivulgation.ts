import * as winston from "winston";
import { IRequest } from "../../common/interface/request";
import { IResource } from "../../common/interface/resource";
import { ICheck } from "../../common/interface/check";
import { CHECK_TYPE, CHECK_NAME } from "../../common/constantes";
import { ProxyManager } from "../../loaders/ProxyManager";
import { CheckService } from "../../services/checkService";
import { Mutex } from "async-mutex";

const mutex = new Mutex();

export async function checkPhpInfoDivulgation(args: any[]) {
    winston.debug("checkPhpInfoDivulgation");
    const request : IRequest = args[0];
    const resource : IResource = args[1];

    // Prepare data
    const check : ICheck = {
        name: CHECK_NAME.PHPINFO_ENABLED,
        type: CHECK_TYPE.PAGE_ENABLED,
        requests: {
            template: {
                protocol: request.protocol || "http",
                method: "GET",
                url: "/?phpinfo=1",
                body: "",
                headers: []
            }
        },
        applicationId: ProxyManager.instance.session.applicationId
    }

    // Create check
    const release = await mutex.acquire();
    try {
        await CheckService.createCheck(check);
    } finally {
        release();
    }
}