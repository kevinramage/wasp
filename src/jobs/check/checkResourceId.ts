import * as winston from "winston";
import { Request } from "../../models/request";
import { IRequest } from "../../common/interface/request";
import { IHeader } from "../../common/interface/header";
import { CheckService } from "../../services/checkService";
import { Resource } from "../../models/resource";
import { CHECK_TYPE, CHECK_NAME } from "../../common/constantes";

export function checkResourceId (args : any[]) {
    winston.debug("checkResourceId");

    const request : Request = args[0];
    const resource : Resource = args[1];

    var regex = /(?<begin>.*\/)([0-9]+)\/?$/g;
    execWithRegex(regex, request, resource);

    var regex = /(?<begin>.*=)([0-9]+)(?<end>.*)/g;
    execWithRegex(regex, request, resource);
}

export function execWithRegex(regex : RegExp, request : Request, resource : Resource) {
    const match = regex.exec(request.url);
    if ( match ) {
        /*
        // Generate requests
        const requestTemplate = getRequest(request);
        var template = match.groups.begin;
        template += "%s";
        if ( match.groups.end ) {
            template += match.groups.end;
        }
        const ids = [ "0", "-1", "test", "'1" ];
        const requests = generateRequests(requestTemplate, template, ids);

        // Generate checks
        const checks = requests.map(r => {
            return {
                type: CHECK_TYPE.ERROR_HANDLING,
                name: CHECK_NAME.RESOURCE_ACCESS,
                request: r
            }
        });

        // Create check in bdd
        CheckService.createChecks(checks, resource.id);
        */
    }
}
/*
function getRequest ( request : Request ) : IRequest {
    return {
        url: "",
        body: request.body,
        method: request.method,
        protocol: request.protocol,
        sentDate: new Date(request.sentDate),
        headers: request.headers.map(h => { 
            const header : IHeader = {
                key: h.key,
                value: h.value
            }
            return header;
        })
    }
}

function generateRequests ( requestTemplate : IRequest, template : string, resourceId : string[] ) {
    return resourceId.map(id => {
        const request = Object.assign({}, requestTemplate);
        request.url = template.replace("%s", id);
        return request;
    });
}
*/