import * as winston from "winston";
import { EventManager } from "../../loaders/EventManager";

export function runChecks (args : string[]) {
    winston.debug("runChecks");
    //EventManager.instance.emit("CheckResourceId", args[0], args[1]);
    EventManager.instance.emit("CheckPhpInfoDivulgation", args[0], args[1]);
}