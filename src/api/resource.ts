import * as express from "express";
import { COMMON_ERROR } from "../common/constantes";
import { sendResourceNotFoundResponse, sendContent, sendInternalErrorResponse } from "./base";
import { ResourceService } from "../services/resourceService";
import * as ControlRouter from "./control";
import * as ComponentRouter from "./component";
import * as WordRouter from "./word";
import * as CommentRouter from "./comment";
import * as EntryPointRouter from "./entryPoint";
import winston = require("winston");
import { IEntryPoint } from "../common/interface/entryPoint";
import { ResourceValidator } from "../validators/resource";

let router : express.Router = express.Router({mergeParams: true});

router.route('/:resId').get((req, res) => {

    // Get resource id
    const resourceId = Number.parseInt(req.params.resId);
    if  (resourceId) {
        ResourceService.getResource(resourceId).then((resource) => {
            if ( resource ) {
                sendContent(res, resource);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "resource '" + req.params.resId + "'")
                sendResourceNotFoundResponse(res, error);
            }
        }).catch(() => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "resource '" + req.params.resId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

router.route('/').get((req, res) => {

    // Get session id
    const sessionId = Number.parseInt(req.params.sesId);
    if ( sessionId ) {
        ResourceService.getAllResources(sessionId).then((resources) => {
            if ( resources ) {
                sendContent(res, resources);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "session '" + req.params.sesId + "'")
                sendResourceNotFoundResponse(res, error);
            }
        }).catch(() => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "session '" + req.params.sesId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

router.route('/:resId/inject').post(ResourceValidator.validateEntryPoints, (req, res) => {

    // Get resource id
    const resourceId = Number.parseInt(req.params.resId);
    if  (resourceId) {
        const entryPoints = req.body.entryPoints as IEntryPoint[];
        const injectionTypeId = req.body.injectionTypeId as number;
        const payloadsId = req.body.payloadsId as number[];
        const defaultValue = req.body.defaultValue as string;
        const difference = req.body.difference as string;
        ResourceService.inject(resourceId, null, entryPoints, injectionTypeId, payloadsId, defaultValue, difference).then((injectionStatus) => {
            sendContent(res, injectionStatus);
        }).catch((err) => {
            sendInternalErrorResponse(err);
        });
        
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "resource '" + req.params.resId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

router.use('/:resId/control', ControlRouter);
router.use('/:resId/componentType', ComponentRouter);
router.use('/:resId/word', WordRouter);
router.use('/:resId/comment', CommentRouter );
router.use('/:resId/entryPoint', EntryPointRouter );

export = router;