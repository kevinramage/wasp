import * as express from "express";
import { InjectionTypeService } from "../services/injectionTypeService";
import { sendContent, sendInternalErrorResponse } from "./base";

let router : express.Router = express.Router({mergeParams: true});

router.route('/').get((req, res) => {

    InjectionTypeService.getAllInjectionTypes().then((injectionTypes) => {
        sendContent(res, injectionTypes);
    }).catch(() => {
        sendInternalErrorResponse(res);
    });

});

export = router;