import * as express from "express";
import * as ApplicationRouter from "./application";
import * as InjectionTypeRouter from "./injectionType";
import * as ConfigurationRouter from "./configuration";

let router : express.Router = express.Router({mergeParams: true});

router.use((req, res, next) => {
    res.setHeader("Content-Type", "application/json");
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    next();
});

router.use('/application', ApplicationRouter);
router.use('/injectionType', InjectionTypeRouter);
router.use('/configuration', ConfigurationRouter);

export = router;