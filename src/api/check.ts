import * as express from "express";
import { COMMON_ERROR } from "../common/constantes";
import { sendResourceNotFoundResponse, sendContent, sendInternalErrorResponse } from "./base";
import { CheckService } from "../services/checkService";

let router : express.Router = express.Router({mergeParams: true});

router.route('/').get((req, res) => {

    // Get application id
    const applicationId = Number.parseInt(req.params.appId);
    if ( applicationId ) {
        CheckService.getAllChecks(applicationId).then((checks) => {
            if ( checks ) {
                sendContent(res, checks);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "application '" + req.params.appId + "'")
                sendResourceNotFoundResponse(res, error);
            }
        }).catch(() => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "application '" + req.params.appId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

export = router;