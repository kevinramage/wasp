import * as express from "express";
import { CommentService } from "../services/commentService";
import { sendContent, sendResourceNotFoundResponse, sendInternalErrorResponse } from "./base";
import { COMMON_ERROR } from "../common/constantes";

let router : express.Router = express.Router({mergeParams: true});

router.route('/').get((req, res) => {

    // Get resource id
    const resourceId = Number.parseInt(req.params.resId);
    if ( resourceId ) {
        CommentService.getAllComments(resourceId).then((comments) => {
            if ( comments ) {
                sendContent(res, comments);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "resource '" + req.params.resId + "'")
                sendResourceNotFoundResponse(res, error);
            }
        }).catch(() => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "resource '" + req.params.resId + "'")
        sendResourceNotFoundResponse(res, error);
    }

});

export = router;