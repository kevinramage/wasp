import * as express from "express";
import winston = require("winston");
import { sendInternalErrorResponse, sendContent, sendResourceNotFoundResponse } from "./base";
import { COMMON_ERROR } from "../common/constantes";
import { SessionService } from "../services/sessionService";
import * as RequestRouter from "./request";
import * as ResourceRouter from "./resource";

let router : express.Router = express.Router({mergeParams: true});

// Get session
router.route('/:sesId').get((req, res) => {

    // Get session id
    const sessionId = Number.parseInt(req.params.sesId);
    if ( sessionId ) {
        SessionService.getSession(sessionId).then((session) => {
            if ( session ) {
                sendContent(res, session);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "session '" + req.params.sesId + "'")
                sendResourceNotFoundResponse(res, error);
            }
        }).catch(() => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "session '" + req.params.sesId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

// Get all sessions
router.route('/').get((req, res) => {

    // Get application id
    const applicationId = Number.parseInt(req.params.appId);
    if ( applicationId ) {

        // Filter
        var activated : boolean | null = null;
        if ( !!req.query["activated"] ) {
            activated = (req.query["activated"] as string).toLowerCase() === "true"
        }

        SessionService.getAllSessions(applicationId, activated).then((sessions) => {
            if ( sessions ) {
                sendContent(res, sessions);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "application '" + req.params.appId + "'")
                sendResourceNotFoundResponse(res, error);
            }
        }).catch(() => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "application '" + req.params.appId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

// Request
router.use('/:sesId/request', RequestRouter);

// Resource
router.use('/:sesId/resource', ResourceRouter);

export = router;