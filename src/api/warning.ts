import * as express from "express";
import { WarningService } from "../services/warningService";
import { sendContent, sendResourceNotFoundResponse, sendInternalErrorResponse } from "./base";
import { COMMON_ERROR } from "../common/constantes";

let router : express.Router = express.Router({mergeParams: true});

router.route('/').get((req, res) => {

    // Get application id
    const applicationId = Number.parseInt(req.params.appId);
    const limit = Number.parseInt(req.query.limit) || 200;
    if ( applicationId ) {
        WarningService.getAllWarnings(applicationId, limit).then((warnings) => {
            if (warnings) {
                sendContent(res, warnings);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "application '" + req.params.appId + "'")
                sendResourceNotFoundResponse(res, error);
            }
        }).catch(() => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "application '" + req.params.appId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

export = router;