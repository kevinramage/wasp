import { Response } from "express";
import { COMMON_ERROR } from "../common/constantes";
import winston = require("winston");

export function sendInternalErrorResponse(res : Response) {
    res.status(500);
    res.write(JSON.stringify(COMMON_ERROR.INTERNAL_ERROR));
    res.end();
}

export function sendResourceNotFoundResponse(res : Response, error : object) {
    res.status(404);
    res.write(JSON.stringify(error));
    res.end();
}

export function sendBadRequestResponse(res : Response, error : object) {
    res.status(400);
    res.write(JSON.stringify(error));
    res.end();
}

export function sendContent(res: Response, content: object) {
    res.status(200);
    res.write(JSON.stringify(content));
    res.end();
}

export function sendNewContent(res: Response, content: object) {
    res.status(201);
    res.write(JSON.stringify(content));
    res.end();
}

export function sendEmptyContent(res: Response) {
    res.status(204);
    res.write("");
    res.end();
}