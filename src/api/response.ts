import * as express from "express";
import { sendInternalErrorResponse, sendNewContent, sendContent, sendResourceNotFoundResponse, sendEmptyContent } from "./base";
import { COMMON_ERROR } from "../common/constantes";
import { ResponseService } from "../services/responseService";

let router : express.Router = express.Router({mergeParams: true});

// Get response
router.route('/:resId').get((req, res) => {

    // Get response id
    const responseId = Number.parseInt(req.params.resId);
    if ( responseId ) {
        ResponseService.getResponse(responseId).then((response) => {
            if ( response ) {
                sendContent(res, response);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "response '" + req.params.resId + "'")
                sendResourceNotFoundResponse(res, error);
            }

        }).catch((err) => {
            sendInternalErrorResponse(res);
        })
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "response '" + req.params.resId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

export = router;