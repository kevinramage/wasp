import * as express from "express";
import { EntryPointService } from "../services/entryPointService";
import { sendContent, sendResourceNotFoundResponse, sendInternalErrorResponse } from "./base";
import { COMMON_ERROR } from "../common/constantes";

let router : express.Router = express.Router({mergeParams: true});

router.route('/').get((req, res) => {

    // Get resource id
    const resourceId = Number.parseInt(req.params.resId);
    if ( resourceId ) {
        EntryPointService.getResourceEntryPoints(resourceId).then((entryPoints) => {
            if ( entryPoints ) {
                sendContent(res, entryPoints);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "resource '" + req.params.resId + "'")
                sendResourceNotFoundResponse(res, error);
            }
        }).catch(() => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "resource '" + req.params.resId + "'")
        sendResourceNotFoundResponse(res, error);
    }

});

export = router;