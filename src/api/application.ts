import * as express from "express";
import { ApplicationService } from "../services/applicationService";
import { COMMON_ERROR } from "../common/constantes";
import { ApplicationValidator } from "../validators/application";
import { sendInternalErrorResponse, sendNewContent, sendContent, sendResourceNotFoundResponse, sendEmptyContent } from "./base";
import * as SessionRouter from "./session";
import * as TechnologyRouter from "./technology";
import * as WarningRouter from "./warning";
import * as CheckRouter from "./check";
import * as CheckRunRouter from "./checkRun";

let router : express.Router = express.Router({mergeParams: true});

// Get an existing application
router.route('/:appId').get((req, res) => {

    // Get application id
    const applicationId = Number.parseInt(req.params.appId);
    if ( applicationId ) {

        // Get an existing application
        ApplicationService.getApplication(applicationId)
        .then((application) => {
            if ( application ) {
                sendContent(res, application);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "application '" + req.params.appId + "'");
                sendResourceNotFoundResponse(res, error);
            }
        }).catch(() => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "application '" + req.params.appId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

// Get all applications
router.route('/').get((req, res) => {

    // Get all applications
    ApplicationService.getAllApplications()
    .then((applications) => {
        sendContent(res, applications);
    }).catch(() => {
        sendInternalErrorResponse(res);
    });
});

// Create an application
router.route('/').post(ApplicationValidator.validateApplication, (req, res) => {

    // Create application
    ApplicationService.createApplication(req.body)
    .then((application) => {
        sendNewContent(res, application);
    }).catch(() => {
        sendInternalErrorResponse(res);
    });
});

// Update an existing application
router.route('/:appId').put(ApplicationValidator.validateApplication, (req, res) => {

    // Get application id
    const applicationId = Number.parseInt(req.params.appId);
    if ( applicationId ) {

        // Update an existing application
        ApplicationService.updateApplication(applicationId, req.body)
        .then((application) => {
            if ( application ) {
                sendContent(res, application);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "application '" + req.params.appId + "'");            
                sendResourceNotFoundResponse(res, error);
            }
        }).catch(() => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "application '" + req.params.appId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

// Delete an existing application
router.route('/:appId').delete((req, res) => {

    // Get application id
    const applicationId = Number.parseInt(req.params.appId);
    if ( applicationId ) {

        // Delete an existing application
        ApplicationService.deleteApplication(applicationId)
        .then((deleted) => {
            if ( deleted ) {
                sendEmptyContent(res);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "application '" + req.params.appId + "'");            
                sendResourceNotFoundResponse(res, error);
            }
        }).catch(() => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "application '" + req.params.appId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

// Start profiling
router.route('/:appId/startProfiling').post((req, res) => {
    
    // Get application id
    const applicationId = Number.parseInt(req.params.appId);
    if ( applicationId ) {

        // Start profiling
        ApplicationService.startProfiling(applicationId).then((session) => {
            if ( session ) {
                const message = {
                    code: "SUCCESS",
                    message: "Start profiling success"
                };
                sendContent(res, message);
                
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "application '" + req.params.appId + "'")
                sendResourceNotFoundResponse(res, error);  
            } 
        }).catch(() => {
            sendInternalErrorResponse(res);
        });

    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "application '" + req.params.appId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

// Stop profiling
router.route('/:appId/stopProfiling').post((req, res) => {

    // Start profiling
    ApplicationService.stopProfiling();
    
    // Send response
    const message = {
        code: "SUCCESS",
        message: "Stop profiling success"
    };
    sendContent(res, message);
});

router.route('/:appId/structureView').get((req, res) => {

    // Get application id
    const applicationId = Number.parseInt(req.params.appId);
    if ( applicationId ) {
        ApplicationService.getStructureView(applicationId).then((treeView) => {
            if ( treeView ) {
                sendContent(res, treeView);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "application '" + req.params.appId + "'");            
                sendResourceNotFoundResponse(res, error);
            }
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "application '" + req.params.appId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

// Session
router.use('/:appId/session', SessionRouter);

// Technology
router.use('/:appId/technology', TechnologyRouter);

// Technology
router.use('/:appId/warning', WarningRouter);

// Check
router.use('/:sesId/check', CheckRouter);

// Check Run
router.use('/:sesId/checkRun', CheckRunRouter);

export = router;