import * as express from "express";
import { ConfigurationService } from "../services/configurationService";
import { sendContent, sendInternalErrorResponse, sendNewContent, sendResourceNotFoundResponse, sendEmptyContent } from "./base";
import { COMMON_ERROR } from "../common/constantes";

let router : express.Router = express.Router({mergeParams: true});

// Get all configuration
router.route('/').get((req, res) => {
    ConfigurationService.getAllConfigurations()
    .then((configurations) => {
        sendContent(res, configurations);
    }).catch(() => {
        sendInternalErrorResponse(res);
    });
});

// Create a configuration
router.route('/').post((req, res) => {
    ConfigurationService.createConfiguration(req.body.key, req.body.value)
    .then((configuration) => {
        sendNewContent(res, configuration);
    }).catch(() => {
        sendInternalErrorResponse(res);
    });
});

// Update a configuration
router.route('/:cfgId').put((req, res) => {
    const configurationId = Number.parseInt(req.params.cfgId);
    if ( configurationId ) {
        ConfigurationService.updateConfiguration(configurationId, req.body.key, req.body.value).
        then((configuration) => {
            sendContent(res, configuration);
        }).catch(() => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "configuration '" + req.params.cfgId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

// Delete a configuration
router.route('/:cfgId').delete((req, res) => {
    const configurationId = Number.parseInt(req.params.cfgId);
    if ( configurationId ) {
        ConfigurationService.deleteConfiguration(configurationId).
        then(() => {
            sendEmptyContent(res);
        }).catch(() => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "configuration '" + req.params.cfgId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});


export = router;