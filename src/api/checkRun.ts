import * as express from "express";
import { COMMON_ERROR } from "../common/constantes";
import { sendResourceNotFoundResponse, sendInternalErrorResponse, sendContent, sendEmptyContent } from "./base";
import { CheckRunService } from "../services/checkRunService";

let router : express.Router = express.Router({mergeParams: true});

router.route('/').get((req, res) => {

    // Get application id
    const applicationId = Number.parseInt(req.params.appId);
    if ( applicationId ) {
        CheckRunService.getAllCheckRuns(applicationId).then((checkRuns) => {
            if (checkRuns) {
                sendContent(res, checkRuns);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "application '" + req.params.appId + "'")
                sendResourceNotFoundResponse(res, error);
            }
        }).catch(() => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "application '" + req.params.appId + "'")
        sendResourceNotFoundResponse(res, error);
    }
    
});

router.route('/run').post((req, res) => {

    // Get application id
    const applicationId = Number.parseInt(req.params.appId);
    if ( applicationId ) {
        const ids = req.body.ids as string[];
        CheckRunService.runChecks(applicationId, ids).then(missingChecks => {
            if ( missingChecks.length == 0 ) {
                sendEmptyContent(res);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", missingChecks.join(", ") + " check ids");
                sendResourceNotFoundResponse(res, error);
            }
        }).catch((err) => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "application '" + req.params.appId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

export = router;