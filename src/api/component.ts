import * as express from "express";
import { COMMON_ERROR } from "../common/constantes";
import { sendResourceNotFoundResponse, sendContent, sendInternalErrorResponse } from "./base";
import { ComponentService } from "../services/componentService";

let router : express.Router = express.Router({mergeParams: true});

router.route('/').get((req, res) => {

    // Get resource id
    const resourceId = Number.parseInt(req.params.resId);
    if ( resourceId ) {
        ComponentService.getAllComponents(resourceId).then((components) => {
            if ( components ) {
                sendContent(res, components);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "resource '" + req.params.resId + "'")
                sendResourceNotFoundResponse(res, error);
            }
        }).catch(() => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "resource '" + req.params.resId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

export = router;