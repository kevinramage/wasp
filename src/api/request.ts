import * as express from "express";
import { sendInternalErrorResponse, sendContent, sendResourceNotFoundResponse } from "./base";
import { COMMON_ERROR } from "../common/constantes";
import { RequestService } from "../services/requestService";
import * as ResponseRouter from "./response";

let router : express.Router = express.Router({mergeParams: true});

// Get request
router.route('/:reqId').get((req, res) => {

    // Get request id
    const requestId = Number.parseInt(req.params.reqId);
    if ( requestId ) {
        RequestService.getRequest(requestId).then((request) => {
            if ( request ) {
                sendContent(res, request);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "request '" + req.params.reqId + "'")
                sendResourceNotFoundResponse(res, error);
            }

        }).catch(() => {
            sendInternalErrorResponse(res);
        })
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "request '" + req.params.reqId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

// Get all requests
router.route('/').get((req, res) => {

    // Get request id
    const sessionId = Number.parseInt(req.params.sesId);
    if ( sessionId ) {
        RequestService.getSessionRequests(sessionId).then((requests) => {
            if ( requests ) {
                sendContent(res, requests);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "session '" + req.params.sesId + "'")
                sendResourceNotFoundResponse(res, error);
            }

        }).catch(() => {
            sendInternalErrorResponse(res);
        })
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "session '" + req.params.sesId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

router.use('/:reqId/response', ResponseRouter);

export = router;