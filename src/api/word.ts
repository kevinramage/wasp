import * as express from "express";
import { COMMON_ERROR } from "../common/constantes";
import { sendResourceNotFoundResponse, sendContent, sendInternalErrorResponse } from "./base";
import { WordService } from "../services/wordService";

let router : express.Router = express.Router({mergeParams: true});

router.route('/').get((req, res) => {

    // Get resource id
    const resourceId = Number.parseInt(req.params.resId);
    if ( resourceId ) {
        WordService.getAllWords(resourceId).then((words) => {
            if ( words ) {
                sendContent(res, words);
            } else {
                const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
                error.message = error.message.replace("%s", "resource '" + req.params.resId + "'")
                sendResourceNotFoundResponse(res, error);
            }
        }).catch(() => {
            sendInternalErrorResponse(res);
        });
    } else {
        const error = COMMON_ERROR.RESOURCE_NOT_FOUND;
        error.message = error.message.replace("%s", "resource '" + req.params.resId + "'")
        sendResourceNotFoundResponse(res, error);
    }
});

export = router;