import * as winston from "winston";
import { Request } from "../models/request";
import { Check } from "../models/check";
import { DateUtils } from "../common/utils/DateUtils";
import { Header } from "../models/header";
import { ICheck } from "../common/interface/check";
import { Session } from "../models/session";
import { Resource } from "../models/resource";
import { Application } from "../models/application";
import { IHeader } from "../common/interface/header";
import { IRequest } from "../common/interface/request";
import { CheckRequest } from "../models/checkRequest";

/**
 * Service to operate on check object
 */
export class CheckService {

    /**
     * Create a check
     * @param check check to create
     */
    public static createCheck ( check : ICheck ) {
        return new Promise<Check>((resolve, reject) => {
            winston.debug("CheckService.createCheck");

            // Transform headers function
            const mapHeader = (header : IHeader) => {
                var headerValue : string = null;
                if ( header.value as string[]) {
                    headerValue = "[" + (header.value as string[]).join(",") + "]";
                } else {
                    headerValue = header.value as string;
                }
                return {
                    key: header.key,
                    value: headerValue
                }
            };

            // Transform requests function
            const mapRequest = (requests: {[name: string] : IRequest }) => {
                const newRequests = [];
                Object.keys(check.requests).forEach((key) => {
                    const request = check.requests[key];
                    const newRequest = {
                        name: key,
                        request: {
                            protocol: request.protocol,
                            method: request.method,
                            url: request.url,
                            body: request.body,
                            headers: request.headers.map(mapHeader)
                        }
                    }
                    newRequests.push(newRequest);
                });
                return newRequests;
            }

            // Find or create check
            Check.findOrCreate({
                where: {
                    type: check.type,
                    name: check.name,
                    applicationId: check.applicationId || null,
                    resourceId: check.resourceId || null
                }
            }).then((result) => {
                const checkCreated = result[0];
                const isCreated = result[1];
                if ( isCreated ) {
                    const requests = mapRequest(check.requests);
                    requests.forEach(r => { r.checkId = checkCreated.id });
                    CheckRequest.bulkCreate(requests, {
                        include: [
                            { model: Request, as: 'request', include: [
                                { model: Header, as: 'headers' }
                            ]}
                        ]
                    }).then(() => {
                        resolve(checkCreated);
                    }).catch((err) => {
                        winston.error("CheckService.createCheck - requests - Internal error: ", err);
                        reject(err);
                    });
                } else {
                    resolve(checkCreated);
                }
            }).catch((err) => {
                winston.error("CheckService.createCheck - createIfNotExists - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Create a checks
     * @param checks checks to create
     * @param resourceId resource id linked
     */
    /*
    public static createChecks ( checks : ICheck[], resourceId : number ) {
        return new Promise<Check[]>((resolve, reject) => {
            winston.debug("CheckService.createCheck");
            Check.bulkCreate(checks.map(c => {
                return {
                    type: c.type,
                    name: c.name,
                    request: {
                        protocol: c.request.protocol,
                        method: c.request.method,
                        url: c.request.url,
                        body: c.request.body,
                        sentDate: DateUtils.formatToMysqlFormat(c.request.sentDate),
                        headers: c.request.headers.map(h => {
                            return {
                                key: h.key,
                                value: h.value
                            }
                        })
                    },
                    resourceId: resourceId
                }
            }), {
                include: [
                    { model: Request, as: 'request', include: [
                        { model: Header, as: 'headers' }
                    ] }
                ]
            }).then((checks) => {
                resolve(checks);
            }).catch((err) => {
                winston.debug("CheckService.createCheck - Internal error: ", err);
                reject(err);
            });
        });
    }
    */

    /**
     * Get all checks
     * @param applicationId application id
     */
    public static getAllChecks ( applicationId : number ) {
        winston.debug("CheckService.getAllChecks");
        return new Promise<Check[]>((resolve, reject) => {
            Application.findByPk(applicationId, {
                include: [
                    { model: Session, as: 'sessions', include: [
                        { model: Resource, as: 'resources', include: [
                            { model: Check, as: 'checks' }
                        ] }
                    ] }
                ]
            }).then((application) => {
                if ( application ) {
                    const resources = application.sessions.map(s => { return s.resources }).reduce((a, b) => a.concat(b) );
                    const checks = resources.map(r => { return r.checks; }).reduce((a, b) => a.concat(b) );
                    resolve(checks);
                } else {
                    resolve(null);
                }
            }).catch((err) => {
                winston.error("CheckService.getAllChecks - Internal error: ", err);
                reject(err);
            });
        });
    }
}