import * as winston from "winston";
import { IControl } from "../common/interface/control";
import { Control } from "../models/control";
import { ControlAttribute } from "../models/controlAttribute";
import { Resource } from "../models/resource";

/**
 * Service to operate on control object
 */
export class ControlService {

    /**
     * Get all resource controls
     * @param resourceId resource id to search
     */
    public static getAllControls(resourceId: number) {
        winston.debug("ControlService.getControls: " + resourceId);
        return new Promise<Control[]>((resolve, reject) => {
            Resource.findByPk(resourceId, {
                include: [
                    { model: Control, as: "controls", where: {
                        parentId: null
                    }, include: [
                        { model: Control, as: "children", include: [
                            { model: ControlAttribute, as: "attributes" }
                        ] },
                        { model: ControlAttribute, as: "attributes" }
                    ]}
                ]
            }).then((resource) => {
                if ( resource ) {
                    resolve(resource.controls);
                } else {
                    resolve(null);
                }
            }).catch((err) => {
                winston.error("ControlService.getControls - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Create controls
     * @param controls controls to create
     * @param resourceId resource id
     */
    public static createControls(controls : IControl[], resourceId : number) {
        winston.debug("ControlService.createControl");
        return new Promise<Control[]>((resolve, reject) => {
            Control.bulkCreate(
                controls.map(control => {
                    return {
                        type: control.type,
                        text: control.text,
                        html: control.html,
                        identifier: control.identifier,
                        resourceId: resourceId,
                        children: control.children.map(sc => { return {
                            type: sc.type,
                            text: sc.text,
                            html: sc.html,
                            identifier: sc.identifier,
                            resourceId: resourceId,
                            attributes: sc.attributes.map(a => {
                                return {
                                    key: a.key,
                                    value: a.value,
                                    html: a.html
                                }
                            })
                        }}),
                        attributes: control.attributes.map(a => {
                            return {
                                key: a.key,
                                value: a.value,
                                html: a.html
                            }
                        })
                    };
                }), {
                    include: [
                        { model: Control, as: 'children', include: [
                            { model: ControlAttribute, as: 'attributes' }
                        ] },
                        { model: ControlAttribute, as: 'attributes' }
                    ]
                }
            ).then((controls : Control[]) => {
                resolve(controls);
            }).catch((err) => {
                winston.error("ControlService.createControl - Internal error: ", err);
                reject(err);
            });
        });
    }
}