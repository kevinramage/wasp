import * as winston from "winston";
import { RequestService } from "./requestService";
import { Request } from "../models/request";
import { TreeView } from "../common/interface/treeView";

/**
 * Service to generate application structure view
 */
export class StructureView {
    
    /**
     * Get view application 
     * @param applicationId application id
     */
    public static getViewApplication(applicationId: number) {
        winston.debug("StructureView.getViewApplication");
        return new Promise<TreeView>(async (resolve, reject) => {

            // Root tree
            const treeItem : TreeView = {
                root: {
                    id: null,
                    value: '/',
                    children: []
                }
            }
            
            // Get all requests
            const requests : Request[] = await RequestService.getAllRequests(applicationId);
            requests.forEach((req) => {
                if ( req.url != "/") {
                    const elements = req.url.split('/');
                    var currentElement = treeItem.root;
                    elements.forEach((url, index) => {
                        if ( index > 0 ) {
                            const oldElement = currentElement.children.find(x => { return x.value === url; });
                            if ( !oldElement ) {
                                const newElement = {
                                    id: (index + 1 == elements.length) ? req.id : null,
                                    value: url,
                                    children: []
                                }
                                currentElement.children.push(newElement);
                                currentElement = newElement;
                            } else {
                                currentElement = oldElement;
                            }
                        }
                    });
                } else {
                    treeItem.root.id = req.id;
                }
            });

            resolve(treeItem);
        });
    }
}