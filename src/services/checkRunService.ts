import * as winston from "winston";
import { CheckRun } from "../models/checkRun";
import { ProxyManager } from "../loaders/ProxyManager";
import { Application } from "../models/application";
import { Check } from "../models/check";
import { CheckService } from "./checkService";
import { application } from "express";
import { EventManager } from "../loaders/EventManager";

/**
 * Service to operate on check run object
 */
export class CheckRunService {

    /**
     * Get all run
     * @param applicationId 
     */
    public static getAllCheckRuns(applicationId: number) {
        winston.debug("CheckRunService.getAllCheckRuns");
        return new Promise<CheckRun[]>((resolve, reject) => {
            Application.findByPk(applicationId, {
                include: [
                    { model: CheckRun, as: 'runs' }
                ]
            }).then((application) => {
                if ( application ) {
                    resolve(application.runs);
                } else {
                    resolve(null);
                }
            }).catch((err) => {
                winston.error("CheckRunService.getAllCheckRuns - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Create a new run
     * @param applicationId application id
     */
    public static createCheckRun(applicationId) {
        winston.debug("CheckRunService.createCheckRun");
        return new Promise<CheckRun>((resolve, reject) => {
            CheckRun.create({
                name: "Run " + new Date().toISOString(),
                applicationId: applicationId
            }).then((checkRun) => {
                resolve(checkRun);
            }).catch((err) => {
                winston.error("CheckRunService.createCheckRun - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Run checks
     * @param applicationId application id 
     * @param ids check ids to run
     */
    public static runChecks(applicationId : number, idsText: string[]) {
        winston.debug("CheckRunService.runChecks");
        return new Promise<number[]>((resolve, reject) => {

            // Get check to run
            CheckService.getAllChecks(applicationId).then(checks => {
                const ids : number[] = idsText.map(i => { return Number.parseInt(i); })
                const result = CheckRunService.collectChecks(checks, ids);
                if ( result as Check[] ) {

                    // Create a run
                    CheckRunService.createCheckRun(applicationId).then(run => {

                        // Call a job to execute checks
                        EventManager.instance.emit("RunChecks", run, result as Check[]);

                        resolve([]);
                    }).catch((err) => {
                        reject(err);
                    });
                    
                } else {
                    resolve(result as number[]);
                }
            }).catch((err) => {
                reject(err);
            });
        });
    }

    /**
     * Search a list of checks from its id
     * @param checks all application checks
     * @param ids checks id to execute
     */
    private static collectChecks(checks: Check[], ids: number[]) : Check[] | number[] {
        const checksToRun : Check[] = [];
        var idsMissing : number[] = ids.concat([]);
        ids.sort((a, b) => { return a > b ? 1 : -1 });
        checks.sort((a, b) => { return a.id > b.id ? 1 : -1 });
        winston.debug("CheckRunService.runChecks, ids: ", ids);
        winston.debug("CheckRunService.runChecks, checks: ", checks);
        for ( var i = 0, j = 0; i < checks.length; ) {
            if ( checks[i].id == ids[j] ) {
                checksToRun.push(checks[i]);
                const index = idsMissing.indexOf(ids[j]);
                idsMissing = idsMissing.splice(index, 1);
                i++; j++;
            } else if ( checks[i].id < ids[j] ) {
                j++;
            } else {
                i++;
            }
        }
        if ( idsMissing.length > 0 ) {
            return idsMissing;
        } else {
            return checksToRun;
        }
    }
}