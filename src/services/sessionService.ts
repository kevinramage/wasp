import * as winston from "winston";
import { Session } from "../models/session";
import { DatabaseManager } from "../loaders/DatabaseManager";
import { Application } from "../models/application";
import { Request } from "../models/request";
import { Response } from "../models/response";
import { Header } from "../models/header";
import { HTTPUtils } from "../common/utils/HTTPUtils";

/**
 * Service to operate on session object
 */
export class SessionService {

    /**
     * Search all sessions linked to an application
     * @param applicationId application id
     */
    public static getAllSessions(applicationId : number, activated : boolean | null) {
        winston.debug("SessionService.getAllSessions: " + applicationId + ", " + activated);
        return new Promise<Object[]>((resolve, reject) => {
            Application.findByPk(applicationId, {
                include: [
                    { model: Session, as: "sessions", include: [
                        { model: Request, as: "requests" }
                    ] }
                ]
            }).then((application) => {
                if ( application ) {
                    const sessions = application.sessions.filter(s => {
                        return activated === null || activated === s.activated;
                    }).map(session => {
                        return {
                            id: session.id,
                            name: session.name,
                            activated: session.activated,
                            requestNumber: session.requests.length,
                            createdAt: session.createdAt,
                            updatedAt: session.updatedAt
                        }
                    });
                    resolve(sessions);
                } else {
                    resolve(null)
                }
            }).catch((err) => {
                winston.error("SessionService.getAllSessions - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Get a specific session from it's id
     * @param sessionId session id
     */
    public static getSession(sessionId : number) {
        winston.debug("SessionService.getSession");
        return new Promise<Session>((resolve, reject) => {
            Session.findByPk(sessionId, {
                include: [
                    { model: Request, as: 'requests', include: [
                        { model: Header, as: 'headers' },
                        { model: Response, as: 'response', include: [
                            { model: Header, as: 'headers' }
                        ]}
                    ] }
                ]
            }).then((session) => {

                // Transform blob to string
                if ( session ) {
                    session["requests"].forEach(r => {
                        r["body"] = r["body"].toString();
                        if ( r["response"] ) {
                            r["response"]["body"] = r["response"]["body"].toString();
                        }
                    });
                }
                resolve(session);
            }).catch((err) => {
                winston.error("SessionService.getSession - Internal error: ", err);
                reject(err);
            });

        });
    }

    /**
     * Create a session
     * @param applicationId application id linked
     */
    public static createSession(applicationId : number) : Promise<Session> {
        winston.debug("SessionService.createSession");
        return new Promise<Session>((resolve, reject) => {

            // Create a transaction
            DatabaseManager.instance.database.transaction().then((t) => {

                Application.findByPk(applicationId)
                .then((application) => {

                    if ( application ) {
                    
                        // Inactive all previous session
                        Session.update({
                            activated: false
                        }, { transaction: t, where: {} })
                        .then(() => {
                            
                            // Create session
                            Session.create({
                                name: new Date().toISOString(),
                                activated: true,
                                applicationId: applicationId
                            }, { transaction: t })
                            .then((session) => {
                                t.commit();
                                resolve(session);
                            }).catch((err) => {
                                t.rollback();
                                winston.error("SessionService.createSession - Internal error during the creation: ", err);
                                reject(err);
                            });

                        }).catch((err) => {
                            t.rollback();
                            winston.error("SessionService.createSession - Internal error during updating: ", err);
                            reject(err);
                        });

                    } else {
                        resolve(null);
                    }
                }).catch((err) => {
                    winston.error("SessionService.createSession - Internal error during the application loading: ", err);
                    reject(err);
                });

            }).catch((err) => {
                winston.error("SessionService.createSession - Internal error during transaction creation: ", err);
                reject(err);
            });
        });
    }

    /**
     * Get all XHR requests identify in the session
     * @param sessionId session id
     */
    public static getXHRRequests(sessionId : number ) {
        winston.debug("SessionService.getXHRRequests");
        return new Promise<Request[]>(resolve => {
            SessionService.getSession(sessionId).then((session) => {
                winston.debug("SessionService.getXHRRequests - Requests: " + session.requests.length);
                const xhrRequests = session.requests.filter(HTTPUtils.isXHRRequest);
                resolve(xhrRequests)
            }).catch(() => {
                resolve([]);
            });
        })
    }
}