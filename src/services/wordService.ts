import * as winston from "winston";
import { Word } from "../models/word";
import { Resource } from "../models/resource";
import { resolve } from "bluebird";

export class WordService {

    /**
     * Get all words linked to a resource
     * @param resourceId resource id
     */
    public static getAllWords(resourceId: number) {
        winston.debug("WordService.getAllWords: " + resourceId);
        return new Promise<Word[]>((resolve, reject) => {
            Resource.findByPk(resourceId, {
                include: [ { model: Word, as: 'words', order: [ 
                    ['counter', 'DESC'] 
                ] }]
            }).then((resource) => {
                if ( resource ) {
                    resolve(resource.words.sort((a, b) => { return b.counter > a.counter ? 1: -1}));
                } else {
                    resolve(null);
                }
            }).catch((err) => {
                winston.error("WordService.getAllWords - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Create words
     * @param counter words to create
     * @param resourceId resource id linked
     */
    public static createWords(counter: {[id: string]:number}, resourceId: number) {
        winston.debug("WordService.createWords");
        return new Promise<Word[]>((resolve, reject) => {

            // Create words object
            const wordAttributes = Object.keys(counter).map((k) => {
                return {
                    word: k,
                    counter: counter[k],
                    resourceId: resourceId
                };
            });

            // Create instances
            Word.bulkCreate(wordAttributes).then((words) => {
                resolve(words);
            }).catch((err) => {
                winston.error("WordService.createWords - Internal error: ", err);
                reject(err);
            });
        });
    }
}