import * as winston from "winston";
import * as xssFilters from "xss-filters";
import { Application } from "../models/application";
import { ProxyManager } from "../loaders/ProxyManager";
import { SessionService } from "./sessionService";
import { Session } from "../models/session";
import { TreeView } from "../common/interface/treeView";
import { URL_TYPE } from "../common/constantes";
import { StructureView } from "./structureView";

/**
 * Service to operate on application object
 */
export class ApplicationService {

    /**
     * Get all applications
     * @returns all applications
     */
    public static getAllApplications() {
        winston.debug("ApplicationService.getAllApplications");
        return new Promise<Application[]>((resolve, reject) => {
            Application.findAll({
            }).then((applications : Application[]) => {
                resolve(applications);
            }).catch((err) => {
                winston.error("ApplicationService.getAllApplications - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Get an existing application
     * @param id application id
     * @returns an existing application or null
     */
    public static getApplication(id : number) {
        winston.debug("ApplicationService.getApplication");
        return new Promise<Application>((resolve, reject) => {
            Application.findByPk(id).then((application : Application) => {
                resolve(application);
            }).catch((err) => {
                winston.error("ApplicationService.getApplication - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Create an application
     * @param applicationDTO application DTO
     * @returns application created
     */
    public static createApplication(applicationDTO) {
        winston.debug("ApplicationService.createApplication");
        return new Promise<Application>((resolve, reject) => {

            // Filter the application name
            applicationDTO.name = xssFilters.inHTMLData(applicationDTO.name);

            // Create application
            Application.create({
                name: applicationDTO.name
            }).then((application : Application) => {
                resolve(application);

            }).catch((err) => {
                winston.error("ApplicationService.createApplication - Internal error during application creation: ", err);
                reject(err);
            });
        });
    }

    /**
     * Update an application
     * @param id application id
     * @param applicationDTO application DTO
     * @returns application updated
     */
    public static updateApplication(id : number, applicationDTO) {
        winston.debug("ApplicationService.updateApplication");
        return new Promise<Application>((resolve, reject) => {
            applicationDTO.name = xssFilters.inHTMLData(applicationDTO.name);
            Application.findByPk(id).then((application) => {
                if ( application ) {
                    application.name = applicationDTO.name;
                    application.save().then((app) => {
                        resolve(app);
                    }).catch((err) => {
                        winston.error("ApplicationService.updateApplication - Internal error during save: ", err);
                        reject(err);
                    })
                } else {
                    resolve(application);
                }
            }).catch((err) => {
                winston.error("ApplicationService.updateApplication - Internal error during find: ", err);
                reject(err);
            });
        });
    }

    /**
     * Delete an application
     * @param id application id
     * @param applicationDTO application DTO
     * @returns application updated
     */
    public static deleteApplication(id : number) {
        winston.debug("ApplicationService.deleteApplication");
        return new Promise<boolean>((resolve, reject) => {
            Application.destroy({
                where: { id: id }
            }).then((value: number) => {
                resolve(value === 1);
            }).catch((err) => {
                winston.error("ApplicationService.deleteApplication - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Start profiling
     */
    public static startProfiling(applicationId : number) : Promise<Session> {
        winston.debug("ApplicationService.startProfiling");
        return new Promise<Session>((resolve, reject) => {

            // Create session
            SessionService.createSession(applicationId).then((session) => {

                // Start proxy
                ProxyManager.instance.start(session).then(() => {
                    resolve(session);
                }).catch((err) => {
                    reject(err);
                });

            }).catch((err) => {
                reject(err);
            });
        });
    }

    /**
     * Stop profiling
     */
    public static stopProfiling() {
        winston.debug("ApplicationService.stopProfiling");
        ProxyManager.instance.stop();
    }

    /**
     * Change application's url type
     * @param applicationId application id
     * @param ulrType new url type to apply
     */
    public static changeUrlType(applicationId : number, ulrType : string) {
        winston.debug("ApplicationService.changeUrlType");
        return new Promise<Application>((resolve, reject) => {
            Application.findByPk(applicationId).then(application => {
                application.urlType = ulrType;
                application.save().then(() => {
                    resolve(application);
                }).catch((err) => {
                    winston.error("ApplicationService.changeUrlType - Internal error during the save: ", err);
                    reject(err);
                })
            }).catch((err) => {
                winston.error("ApplicationService.changeUrlType - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Get the application structure view
     * @param applicationId application id
     */
    public static getStructureView(applicationId : number ) {
        winston.debug("ApplicationService.getStructureView");
        return new Promise<TreeView>((resolve, reject) => {
            ApplicationService.getApplication(applicationId).then(application => {
                if ( application && application.urlType === URL_TYPE.VIEW ) {
                    StructureView.getViewApplication(applicationId).then(treeView => {
                        resolve(treeView);
                    }).catch((err) => {
                        reject(err);
                    });
                } else {
                    resolve(null);
                }
            }).catch(err => {
                reject(err);
            });
        });
    }
}