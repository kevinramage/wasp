import * as winston from "winston";
import { IComponent } from "../common/interface/component";
import { Component } from "../models/component";
import { Resource } from "../models/resource";

export class ComponentService {

    /**
     * Get all components linked to a resource
     * @param resourceId resource id
     */
    public static getAllComponents(resourceId : number) {
        winston.debug("ComponentService.getAllComponents");
        return new Promise<Component[]>((resolve, reject) => {
            Resource.findByPk(resourceId, {
                include: [
                    { model: Component, as: 'components' }
                ]
            }).then((resource) => {
                if ( resource ) {
                    resolve(resource.components);
                } else {
                    resolve(null);
                }
            }).catch((err) => {
                winston.error("ComponentService.getAllComponents - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Create a new component
     * @param component component to save
     */
    public static createComponent(component : IComponent) {
        winston.debug("ComponentService.createComponent");
        return new Promise<Component>((resolve, reject) => {
            Component.create({
                type: component.type,
                score: component.score,
                html: component.html,
                detail: component.detail,
                resourceId: component.resourceId
            }).then((componentCreated) => {
                resolve(componentCreated);
            }).catch((err) => {
                winston.error("ComponentService.createComponent - Internal error: ", err);
                reject(err);
            });
        });
    }
}