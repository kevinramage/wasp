import * as winston from "winston";
import { IWarning } from "../common/interface/warning";
import { Warning } from "../models/warning";
import { Application } from "../models/application";

/**
 * Service to operate on warning object
 */
export class WarningService {

    /**
     * Get all application warnings
     * @param applicationId application id
     * @param limit limit number
     */
    public static getAllWarnings(applicationId: number, limit: number) {
        winston.debug("WarningService.getAllWarnings");
        return new Promise<Warning[]>((resolve, reject) => {
            Application.findByPk(applicationId, {
                include: [
                    { model: Warning, as: 'warnings', order: ["type"], limit: limit }
                ],
            }).then((application) => {
                if ( application ) {
                    const warningSortable = (a : IWarning ,b: IWarning) => {
                        return a.type.localeCompare(b.type);
                    };
                    application.warnings.sort(warningSortable);
                    resolve(application.warnings);
                } else {
                    resolve(null);
                }
            }).catch((err) => {
                winston.error("WarningService.getAllWarnings - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Create a warning
     * @param warning warning to create
     */
    public static createWarning(warning : IWarning) {
        winston.debug("WarningService.createWarning: ", warning);
        return new Promise<Warning>((resolve, reject) => {
            Warning.findOne({
                where: {
                    type: warning.type,
                    message: warning.message,
                    applicationId: warning.applicationId
                } 
            }).then((warn) => {
                if ( !warn ) {
                    Warning.create({
                        title: warning.title,
                        risk: warning.risk,
                        type: warning.type,
                        value: warning.value,
                        message: warning.message,
                        applicationId: warning.applicationId,
                        resourceId: warning.resourceId
                    }).then((warning) => {
                        resolve(warning);
                    }).catch((err) => {
                        winston.error("WarningService.createWarning - Internal error during the creation: ", err);
                        reject(err);
                    });
                } else {
                    resolve(warn);
                }
            }).catch((err) => {
                winston.error("WarningService.createWarning - Internal error during the search: ", err);
                reject(err);
            });
        });
    }
}