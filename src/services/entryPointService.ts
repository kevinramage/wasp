import * as winston from "winston";
import { EntryPoint } from "../models/entryPoint";
import { Resource } from "../models/resource";
import { IEntryPoint } from "../common/interface/entryPoint";

export class EntryPointService {

    /**
     * Get all entry points
     */
    public static getAllEntryPoints() {
        winston.debug("EntryPointService.getAllEntryPoints");
        return new Promise<EntryPoint[]>(((resolve, reject) => {
            EntryPoint.findAll({}).then(entryPoints => {
                resolve(entryPoints);
            }).catch((err) => {
                winston.error("EntryPointService.getAllEntryPoints - Internal error: ", err);
            });
        }));
    }

    /**
     * Get all entry points of a specific resource
     * @param resourceId resource id
     */
    public static getResourceEntryPoints(resourceId: number) {
        winston.debug("EntryPointService.getResourceEntryPoints: " + resourceId);
        return new Promise<EntryPoint[]>(((resolve, reject) => {
            Resource.findByPk(resourceId, {
                include: [
                    { model: EntryPoint, as: 'entryPoints'}
                ]
            }).then((resource) => {
                if ( resource ) {
                    resolve(resource.entryPoints);
                } else {
                    resolve(null);
                }
            }).catch((err) => {
                winston.error("EntryPointService.getAllEntryPoints - Internal error: ", err);
                reject(err);
            });
        }));
    }

    /**
     * Get  entry point from its id
     * @param entryPoint entry point id
     */
    public static getEntryPoint(entryPoint: number) {
        winston.debug("EntryPointService.getEntryPoint: " + entryPoint);
        return new Promise<EntryPoint>(((resolve, reject) => {
            EntryPoint.findByPk(entryPoint).then((entryPoint) => {
                resolve(entryPoint);
            }).catch((err) => {
                winston.error("EntryPointService.getEntryPoint - Internal error: ", err);
                reject(err);
            });
        }));
    }

    /**
     * Create entry points
     * @param entryPoints entry points to create
     */
    public static createEntryPoints(entryPoints : IEntryPoint[]) {
        winston.debug("EntryPointService.createEntryPoints");
        return new Promise<EntryPoint[]>((resolve, reject) => {
            EntryPoint.bulkCreate(
                entryPoints
            ).then((entryPointCreated) => {
                resolve(entryPointCreated);
            }).catch((err) => {
                winston.error("EntryPointService.createEntryPoints - Internal error: ", err);
                reject(err);
            });
        });
    }
}