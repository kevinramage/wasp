import * as winston from "winston";
import { ITechnology } from "../common/interface/technology";
import { Technology } from "../models/technology";
import { Application } from "../models/application";

/**
 * Service to operate on technology object
 */
export class TechnologyService {

    /**
     * Get all technology of an application
     * @param applicationId application id
     */
    public static getAllTechnology(applicationId: number) {
        winston.debug("TechnologyService.getAllTechnology");
        return new Promise<Technology[]>((resolve, reject) => {
            Application.findByPk(applicationId, {
                include: [
                    { model: Technology, as: 'technologies'}
                ]
            }).then((application) => {
                if ( application ) {
                    const technoSortable = (a : ITechnology ,b: ITechnology) => {
                        return a.type.localeCompare(b.type);
                    };
                    application.technologies.sort(technoSortable);
                    resolve(application.technologies);
                } else {
                    resolve(null);
                }
            }).catch((err) => {
                winston.error("TechnologyService.getAllTechnology - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Create a technology instance if not exists
     * @param applicationId application id
     * @param technology technology to create
     */
    public static createTechnology(applicationId: number, technology: ITechnology) {
        winston.debug("TechnologyService.createTechnology", technology);
        return new Promise<Technology>((resolve, reject) => {

            Technology.findOrCreate({
                where: {
                    applicationId: applicationId,
                    type: technology.type,
                    value: technology.value,
                    name: technology.name ? technology.name : null,
                    version: technology.version ? technology.version : null
                }
            }).then((value:[Technology, boolean]) => {
                resolve(value[0]);
            }).catch((err) => {
                winston.error("TechnologyService.createTechnology - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Get technology from an application id and a technology type
     * @param applicationId application id
     * @param type technology type
     */
    public static getTechnologyFromType(applicationId: number, type: string) {
        winston.debug("TechnologyService.getTechnologyFromType");
        return new Promise<Technology>((resolve, reject) => {
            Technology.findOne({
                where: {
                    applicationId: applicationId,
                    type: type
                }
            }).then((technology) => {
                resolve(technology);
            }).catch((err) => {
                winston.error("TechnologyService.getTechnologyFromType - Internal error: ", err);
                reject(err);
            });
        });
    }
}