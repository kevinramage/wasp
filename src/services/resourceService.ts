import * as winston from "winston";
import { IResource } from "../common/interface/resource";
import { Resource } from "../models/resource";
import { Control } from "../models/control";
import { ControlAttribute } from "../models/controlAttribute";
import { Session } from "../models/session";
import { CONTROL_TYPE } from "../common/constantes";
import { InjectionTypeService } from "./injectionTypeService";
import { RequestService } from "./requestService";
import { EntryPointService } from "./entryPointService";
import { InjectionRunner } from "../jobs/injectionRunner";
import { IInjectionStatus } from "../common/interface/injectionStatus";
import { IEntryPoint } from "../common/interface/entryPoint";
import { IRequestSimplified } from "../common/interface/request";

/**
 * Service to operate to resource object
 */
export class ResourceService {

    /**
     * Get all resources of a session
     * @param sessionId session id
     */
    public static getAllResources(sessionId : number) {
        winston.debug("ResourceService.getAllResources");
        return new Promise<Object>((resolve, reject) => {
            Session.findByPk(sessionId, {
                include: [
                    { model: Resource, as: 'resources', include: [
                        { model: Control, as: 'controls' }
                    ] }
                ]
            }).then((session) => {
                if ( session ) {
                    const resources = session.resources.map(r => {
                        const linkCount = r.controls.filter(c => { return c.type === CONTROL_TYPE.LINK }).length;
                        const controlCount = r.controls.length - linkCount;
                        return {
                            id: r.id,
                            identifier: r.identifier,
                            url: r.url,
                            completePathname: r.completePathname,
                            pathname: r.pathname,
                            path: r.path,
                            title: r.title,
                            type: "",
                            createdAt: r.createdAt,
                            updatedAt: r.updatedAt,
                            linkNumber: linkCount,
                            controlNumber: controlCount
                        }
                    });
                    resolve(resources);
                } else {
                    resolve(null);
                }
            }).catch((err) => {
                winston.error("ResourceService.getAllResources - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Get resource from its id
     * @param resourceId resource id
     */
    public static getResource(resourceId : number ) {
        winston.debug("ResourceService.getResource");
        return new Promise<Resource>((resolve, reject) => {
            Resource.findByPk(resourceId).then((resource) => {
                resolve(resource);
            }).catch((err) => {
                winston.error("ResourceService.getResource - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Create a resource
     * @param resource resource to create
     * @param sessionId session id linked
     */
    public static createResource(resource : IResource, sessionId: number) {
        winston.debug("ResourceService.createResource");
        return new Promise<Resource>((resolve, reject) => {
            Resource.create({
                identifier: resource.identifier,
                url: resource.url,
                completePathname: resource.completePathname,
                pathname: resource.pathname,
                path: resource.path,
                title: resource.title,
                requestId: resource.requestId,
                sessionId: sessionId
            }).then((resource) => {
                resolve(resource);
            }).catch((err) => {
                winston.error("ResourceService.createResource - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Inject
     * @param resourceId resource id
     * @param entryPoints entry points
     * @param injectionTypeId injection type id
     * @param payloadsId payloads id
     * @param defaultValue default value
     * @param difference difference
     */
    public static inject(resourceId: number, request: IRequestSimplified, entryPoints: IEntryPoint[], injectionTypeId: number, 
        payloadsId: number[], defaultValue: string, difference: string) {

        return new Promise<IInjectionStatus>(async(resolve, reject) => {
            
            // Collec request
            var resource : IResource, req : IRequestSimplified;
            if ( resourceId ) {
                resource = await ResourceService.getResource(resourceId);
                req = await RequestService.getRequest(resource.requestId);
            } else {
                req = request;
            }

            // Collect data
            const allEntryPoints = await EntryPointService.getAllEntryPoints();
            const injectionTypes = await InjectionTypeService.getAllInjectionTypes();
            const injectionType = injectionTypes.find(i => { return i.id === injectionTypeId });
            const payloads = injectionType.payloads.filter(p => { return payloadsId.includes(p.id) });

            // Update the entry points data
            entryPoints.forEach( e => {
                if ( e.id ) {
                    const entryPoint = allEntryPoints.find(re => { return re.id === e.id });
                    if ( entryPoint ) {
                        e.name = entryPoint.name;
                        e.type = entryPoint.type;
                    }
                }
            });

            // Inject
            const injectionRunner : InjectionRunner = new InjectionRunner(req, entryPoints, injectionType, payloads, defaultValue, difference);
            const injectionStatus = await injectionRunner.inject();
            resolve(injectionStatus);
        });
    }
}