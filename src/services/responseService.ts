import * as winston from "winston";
import { Response } from "../models/response";
import { Header } from "../models/header";

/**
 * Service to operate on response object
 */
export class ResponseService {

    /**
     * Get a response from it's id
     * @param responseId response id
     */
    public static getResponse(responseId: number ) {
        winston.debug("ResponseService.getResponse");
        return new Promise<Response>((resolve, reject) => {
            Response.findByPk(responseId, {
                include: [
                    { model: Header, as: 'headers' }
                ]
            }).then((response) => {
                if ( response ) {
                    response["body"] = response["body"].toString();
                }
                resolve(response);
            }).catch((err) => {
                winston.error("ResponseService.getResponse - Internal error: ", err);
                reject(err);
            });
        });
    }
}