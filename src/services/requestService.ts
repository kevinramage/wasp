import * as winston from "winston";
import { IRequest } from "../common/interface/request";
import { Request } from "../models/request";
import { DateUtils } from "../common/utils/DateUtils";
import { Header } from "../models/header";
import { Response } from "../models/response";
import { Session } from "../models/session";
import { IResponse } from "../common/interface/response";
import { ProxyManager } from "../loaders/ProxyManager";
import { response } from "express";

/**
 * Service to operate on request object
 */
export class RequestService {

    /**
     * Get requests captured during all application's sessions
     * @param applicationId application id
     */
    public static getAllRequests(applicationId: number ) {
        winston.debug("RequestService.getAllRequests");
        return new Promise<Request[]>((resolve, reject) => {
            Session.findAll({
                where: {
                    applicationId: applicationId
                },
                include: [
                    { model: Request, as: 'requests' }
                ]
            }).then((sessions) => {
                const requests : Request[] = sessions.map(s => { return s.requests }).reduce((previousValue, currentValue) => {
                    return previousValue.concat(currentValue);
                });
                resolve(requests);
            }).catch((err) => {
                winston.error("RequestService.getAllRequests - Internal error: ", err);
                reject(err);
            })
        });
    }

    /**
     * Get session requests
     * @param sessionId session id
     */
    public static getSessionRequests(sessionId: number) {
        winston.debug("RequestService.getSessionRequest");
        return new Promise<Object>((resolve, reject) => {
            Session.findByPk(sessionId, {
                include: [
                    { model: Request, as: 'requests', include: [
                        { model: Response, as: 'response' }
                    ] }
                ]
            }).then((session) => {
                if ( session ) {
                    const requests = session.requests.map(req => {
                        const sentDate = new Date(req.sentDate);
                        var receptionDate : Date = null;
                        var diff : number = null;
                        if ( req.response ) {
                            receptionDate = new Date(req.response.receptionDate);
                            diff = receptionDate.getTime() - sentDate.getTime();
                        }
                        return {
                            id: req.id,
                            protocol: req.protocol,
                            host: req.host,
                            method: req.method,
                            url: req.url,
                            body: req.body.toString(),
                            code: req.response ? req.response.status : 0,
                            time: DateUtils.prettyPrint(diff),
                            sentDate: req.sentDate,
                            createdAt: req.createdAt,
                            updatedAt: req.updatedAt,
                        }
                    });
                    resolve(requests)
                } else {
                    resolve(null);
                }
            }).catch((err) => {
                winston.error("RequestService.getSessionRequest - Internal error: ", err);
                reject(err);
            })
        });
    }

    /**
     * Get a request from it's id
     * @param requestId request id
     */
    public static getRequest(requestId: number ) {
        winston.debug("RequestService.getRequest");
        return new Promise<Request>((resolve, reject) => {
            Request.findByPk(requestId, {
                include: [
                    { model: Header, as: 'headers' },
                    { model: Response, as: 'response', include: [
                        { model: Header, as: 'headers' }
                    ]}
                ]
            }).then((request) => {
                var requestObj = null;
                if ( request ) {

                    // Request
                    requestObj = {
                        id: request.id,
                        protocol: request.protocol,
                        host: request.host,
                        method: request.method,
                        url: request.url,
                        body: request.body.toString(),
                        code: request.response.status,
                        sentDate: request.sentDate,
                        createdAt: request.createdAt,
                        updatedAt: request.updatedAt,
                        headers: request.headers.map(h => {
                            return {
                                id: h.id,
                                key: h.key,
                                value: h.value,
                                createdAt: h.createdAt,
                                updatedAt: h.updatedAt
                            }
                        }),
                        response: {}
                    }

                    if ( request["response"] ) {
                        const reqDateTime = new Date(request.sentDate).getTime();
                        const resDateTime = new Date(request.response.receptionDate).getTime();
                        const time = DateUtils.prettyPrint(resDateTime - reqDateTime);
                        requestObj.response = {
                            id: request.response.id,
                            status: request.response.status,
                            time: time,
                            receptionDate: request.response.receptionDate,
                            body: request.response.body.toString(),
                            createdAt: request.response.createdAt,
                            updatedAt: request.response.updatedAt,
                            headers: request.response.headers.map(h => {
                                return {
                                    id: h.id,
                                    key: h.key,
                                    value: h.value,
                                    createdAt: h.createdAt,
                                    updatedAt: h.updatedAt
                                }
                            })
                        }
                    }
                }
                resolve(requestObj);
            }).catch((err) => {
                winston.error("RequestService.getRequest - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Create request
     * @param request request to create
     */
    public static createRequest(request : IRequest, response : IResponse) : Promise<Request> {
        winston.debug("RequestService.createRequest");
        return new Promise<Request>((resolve, reject) => {
            Request.create({
                protocol: request.protocol,
                host: request.host,
                method: request.method,
                url: request.url,
                body: request.body,
                sentDate: DateUtils.formatToMysqlFormat(request.sentDate),
                headers: request.headers.map(h => {
                    return {
                        key: h.key,
                        value: h.value
                    }
                }),
                response: {
                    status: response.statusCode,
                    receptionDate: DateUtils.formatToMysqlFormat(response.receivedDate),
                    body: response.body,
                    headers: response.headers.map(h => {
                        return {
                            key: h.key,
                            value: h.value
                        }
                    })
                },
                sessionId: ProxyManager.instance.session.id
            }, {
                include: [
                    { model: Header, as: "headers" },
                    { model: Response, as: "response", include: [
                        { model: Header, as: "headers" }
                    ]}
                ]
            }).then((request) => {
                resolve(request);
            }).catch((err) => {
                winston.error("RequestService.createRequest - Internal error: ", err);
                reject(err);
            });
        });
    }
}