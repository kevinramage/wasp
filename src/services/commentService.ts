import * as winston from "winston";
import { Comment } from "../models/comment";
import { Resource } from "../models/resource";

/**
 * Service to operate on comment object
 */
export class CommentService {

    /**
     * Get all comments for a specific resource
     * @param resourceId resource id
     */
    public static getAllComments(resourceId: number) {
        winston.debug("CommentService.getAllComments: " + resourceId);
        return new Promise<Comment[]>((resolve, reject) => {
            Resource.findByPk(resourceId, {
                include: [
                    { model: Comment, as: 'comments' }
                ]
            }).then(resource => {
                if ( resource ) {
                    resolve(resource.comments);
                } else {
                    resolve(null);
                }
            }).catch((err) => {
                winston.error("CommentService.getAllComments - Internal error: ", err);
                reject(err);
            });
        });
    }
    
    /**
     * Create comments
     * @param comments comments to generate
     * @param resourceId resource linked
     */
    public static createComments(comments : string[], resourceId : number) {
        winston.debug("CommentService.createComments");
        return new Promise<Comment[]>((resolve, reject) => {
            Comment.bulkCreate(
                comments.map(c => {
                    return {
                        comment: c,
                        resourceId: resourceId
                    }
                })
            ).then((newComments) => {
                resolve(newComments);
            }).catch((err) => {
                winston.error("CommentService.createComments - Internal error: ", err);
                reject(err);
            });
        });
    }
}