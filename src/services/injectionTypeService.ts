import * as winston from "winston";
import { InjectionType } from "../models/injectionType";
import { Payload } from "../models/payload";

export class InjectionTypeService {

    /**
     * Get all injection types
     */
    public static getAllInjectionTypes() {
        winston.debug("InjectionTypeService.getAllInjectionTypes");
        return new Promise<InjectionType[]>((resolve, reject) => {
            InjectionType.findAll({
                include: [
                    { model: Payload, as: "payloads" }
                ]
            }).then((injectionTypes) => {
                resolve(injectionTypes);
            }).catch((err) => {
                winston.error("InjectionTypeService.getAllInjectionTypes - Internal error: ", err);
                reject(err);
            });
        });
    }
}