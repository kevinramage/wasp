import * as winston from "winston";
import { CheckRun } from "../models/checkRun";
import { Check } from "../models/check";
import { CheckExecution } from "../models/checkExecution";
import { CHECKEXECUTION_STATUS } from "../common/constantes";

export class CheckExecutionService {

    /**
     * Create executions
     * @param checkRun run container
     * @param checks checks to execute
     */
    public static createExecutions(checkRun: CheckRun, checks: Check[]) {
        return new Promise<CheckExecution[]>((resolve, reject) => {
            winston.debug("CheckExecutionService.createExecutions");
            const attributes = checks.map(c => {
                return {
                    status: CHECKEXECUTION_STATUS.NORUN,
                    expected: "",
                    actual: "",
                    checkId: c.id,
                    checkRunId: checkRun.id
                }
            });
            CheckExecution.bulkCreate(attributes).then((executions) => {
                executions.forEach(e => {
                    const check = checks.find(c => { return c.id === e.id });
                    e.check = check;
                    e.checkRun = checkRun;
                });
                resolve(executions);
            }).catch((err) => {
                winston.debug("CheckExecutionService.createExecutions - Internal error: ", err);
                reject(err);
            });
        });
    }
}