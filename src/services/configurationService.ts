import * as winston from "winston";
import { Configuration } from "../models/configuration";
import { ConfigurationManager } from "../loaders/ConfigurationManager";

/**
 * Service to operate configuration object
 */
export class ConfigurationService {

    /**
     * Get all configurations
     */
    public static getAllConfigurations() {
        winston.debug("ConfigurationService.getAllConfigurations");
        return new Promise<Configuration[]>((resolve, reject) => {
            Configuration.findAll({}).then((configurations) => {
                resolve(configurations);
            }).catch((err) => {
                winston.error("ConfigurationService.getAllConfigurations - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Create a configuration
     * @param key configuration key
     * @param value configuration key
     */
    public static createConfiguration(key: string, value: string) {
        winston.debug("ConfigurationService.createConfiguration");
        return new Promise<Configuration>((resolve, reject) => {
            Configuration.create({
                key: key.toLowerCase(),
                value: value
            }).then((configuration) => {
                ConfigurationManager.instance.setValue(configuration.key, configuration.value);
                resolve(configuration);
            }).catch((err) => {
                winston.error("ConfigurationService.createConfiguration - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Update a configuration
     * @param id configuration id
     * @param key configuration key
     * @param value configuration value
     */
    public static updateConfiguration(id: number, key: string, value: string) {
        winston.debug("ConfigurationService.updateConfiguration");
        return new Promise<Configuration>((resolve, reject) => {
            Configuration.findOne({ where: { id : id } }).then((configuration) => {
                configuration.key = key.toLowerCase();
                configuration.value = value;
                configuration.save().then((newConfiguration) => {
                    ConfigurationManager.instance.setValue(configuration.key, configuration.value);
                    resolve(newConfiguration);
                }).catch((err) => {
                    winston.error("ConfigurationService.updateConfiguration - Internal error: ", err);
                    reject(err);
                });
            }).catch((err) => {
                winston.error("ConfigurationService.updateConfiguration - Internal error: ", err);
                reject(err);
            });
        });
    }

    /**
     * Delete a configuration from its id
     * @param id configuration id
     */
    public static deleteConfiguration(id: number) {
        winston.debug("ConfigurationService.updateConfiguration");
        return new Promise<void>((resolve, reject) => {
            Configuration.destroy({ where: { id }}).then(() => {
                resolve();
            }).catch((err) => {
                winston.error("ConfigurationService.updateConfiguration - Internal error: ", err);
                reject(err);
            });
        });
    }
}