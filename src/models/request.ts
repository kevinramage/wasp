import { Model, DataTypes, ModelAttributes } from 'sequelize';
import { Response } from './response';
import { Header } from './header';

/**
 * Request model
 */
export class Request extends Model {
    public id !: number;
    public protocol !: string;
    public host !: string;
    public method !: string;
    public url !: string;
    public body !: string;
    public sentDate !: string;
    public headers ?: Header[];
    public readonly response ?: Response;
    public readonly sessionId ?: number;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

/**
 * Request attributes
 */
export const RequestAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    protocol: {
        type: DataTypes.STRING,
        allowNull: false
    },
    host: {
        type: DataTypes.STRING
    },
    method: {
        type: DataTypes.STRING,
        allowNull: false
    },
    url: {
        type: DataTypes.STRING,
        allowNull: false
    },
    body: {
        type: 'MEDIUMBLOB'
    },
    sentDate: {
        type: 'DATETIME(3)'
    }
}

/**
 * RequestFactory
 */
export const RequestFactory = () => {
    Request.hasMany(Header, { as: 'headers', foreignKey: 'requestId', foreignKeyConstraint: true });
    Header.belongsTo(Request, { foreignKey: 'requestId', foreignKeyConstraint: true });
    Request.hasOne(Response, { as: 'response', foreignKey: 'requestId', foreignKeyConstraint: true});
}