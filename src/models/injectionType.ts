import { Model, DataTypes, ModelAttributes } from 'sequelize';
import { Payload } from './payload';

export class InjectionType extends Model {
    public id !: number;
    public type !: string;
    public name !: string;
    public payloads !: Payload[];

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

export const InjectionTypeAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    type: {
        type: DataTypes.ENUM("SQL_BOOLEAN_OR", "SQL_BOOLEAN_AND", "SQL_TIME", "REFLECTED_XSS")
    },
    name: {
        type: DataTypes.STRING
    }
}

export const InjectionTypeFactory = () => {

    // Payload
    InjectionType.hasMany(Payload, { as: "payloads", foreignKey: "injectionTypeId", foreignKeyConstraint: true } );
    Payload.belongsTo(InjectionType, { as: "injectionType", foreignKey: "injectionTypeId", foreignKeyConstraint: true });
}