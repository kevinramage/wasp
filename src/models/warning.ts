import { Model, DataTypes, ModelAttributes } from 'sequelize';
import { Application } from './application';
import { Resource } from './resource';
import { WARNING_SERVER_VERSION, WARNING_APP_VERSION, WARNING_CSP_NOTPRESENT, WARNING_XCSP_NOTPRESENT, WARNING_WEAKPWD_TRANSMISSION, WARNING_PWDSTOREDINCOOKIE, WARNING_UPGRADEINSECUREREQUESTS_NOTPRESENT, WARNING_BLOCKALLMIXEDCONTENT_NOTPRESENT, WARNING_FEATUREPOLICY_NOTPRESENT, WARNING_COOKIESECURE_NOTPRESENT, WARNING_COOKIEHTTPONLY_NOTPRESENT, WARNING_REFERRERPOLICY_NOTPRESENT, WARNING_XFRAMEOPTIONS_NOTPRESENT, WARNING_XXSSPROTECTION_NOTPRESENT, WARNING_STACKTRACE_DIVULGATION, WARNING_SERVERINFO_DIVULGATION, WARNING_SERVERSTATUS_DIVULGATION, WARNING_PHPINFO_DIVULGATION, WARNING_CVSDIRECTORY_DIVULGATION, WARNING_ROBOTSFILE_DIVULGATION, WARNING_UNCONTROLLED_REDIRECTION, WARNING_CONTENTTYPE_MISSING } from '../common/warnings';

/**
 * Warning model
 */
export class Warning extends Model {
    public id !: number;
    public type !: string;
    public title !: string;
    public message !: string;
    public risk !: string;
    public value ?: string;
    public readonly applicationId !: number;
    public readonly resourceId !: number;
    public readonly application !: Application;
    public readonly resource !: Resource;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

/**
 * Warning attributes
 */
export const WarningAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: {
        type: DataTypes.STRING
    },
    type: {
        type: DataTypes.ENUM(WARNING_SERVER_VERSION.type,
            WARNING_APP_VERSION.type,
            WARNING_CSP_NOTPRESENT.type,
            WARNING_XCSP_NOTPRESENT.type,
            WARNING_WEAKPWD_TRANSMISSION.type,
            WARNING_PWDSTOREDINCOOKIE.type,
            WARNING_UPGRADEINSECUREREQUESTS_NOTPRESENT.type,
            WARNING_BLOCKALLMIXEDCONTENT_NOTPRESENT.type,
            WARNING_FEATUREPOLICY_NOTPRESENT.type,
            WARNING_COOKIESECURE_NOTPRESENT.type,
            WARNING_COOKIEHTTPONLY_NOTPRESENT.type,
            WARNING_REFERRERPOLICY_NOTPRESENT.type,
            WARNING_XFRAMEOPTIONS_NOTPRESENT.type,
            WARNING_XXSSPROTECTION_NOTPRESENT.type,
            WARNING_STACKTRACE_DIVULGATION.type,
            WARNING_SERVERINFO_DIVULGATION.type,
            WARNING_SERVERSTATUS_DIVULGATION.type,
            WARNING_PHPINFO_DIVULGATION.type,
            WARNING_CVSDIRECTORY_DIVULGATION.type,
            WARNING_ROBOTSFILE_DIVULGATION.type,
            WARNING_UNCONTROLLED_REDIRECTION.type,
            WARNING_CONTENTTYPE_MISSING.type
        )
    },
    risk: {
        type: DataTypes.TEXT
    },
    value: {
        type: DataTypes.TEXT
    },
    message: {
        type: DataTypes.TEXT
    }
}