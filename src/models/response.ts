import { Model, DataTypes, ModelAttributes } from 'sequelize';
import { Header } from './header';
import { Request } from './request';

/**
 * Response model
 */
export class Response extends Model {
    public id !: number;
    public status !: number;
    public receptionDate !: string;
    public body !: string;
    public requestId !: number;
    public readonly request ?: Request;
    public readonly headers ?: Header[];

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

/**
 * Response attributes
 */
export const ResponseAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    status: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    receptionDate: {
        type: 'DATETIME(3)',
        allowNull: false
    },
    body: {
        type: 'MEDIUMBLOB'
    }
}

/**
 * Response factory
 */
export const ResponseFactory = () => {
    Response.hasMany(Header, { as: 'headers' , foreignKey: 'responseId', foreignKeyConstraint: true });
    Header.belongsTo(Response, { foreignKey: 'responseId', foreignKeyConstraint: true });
}