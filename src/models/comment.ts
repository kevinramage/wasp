import { Model, DataTypes, ModelAttributes } from 'sequelize';

/**
 * Comment model
 */
export class Comment extends Model {
    public id !: number;
    public comment !: string;
    public readonly resourceId !: number;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

/**
 * Comment attributes
 */
export const CommentAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    comment: {
        type: DataTypes.TEXT
    }
}