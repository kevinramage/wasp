import { Model, DataTypes, ModelAttributes } from 'sequelize';
import { CheckExecution } from './checkExecution';
import { Check } from './check';

/**
 * CheckRun model
 */
export class CheckRun extends Model {
    public id !: number;
    public name !: string;
    public readonly executions !: CheckExecution[];
    public readonly applicationId !: number;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

/**
 * CheckRun attributes
 */
export const CheckRunAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING
    }
}

export const CheckRunFactory = () => {
    
    // Executions
    CheckRun.hasMany(CheckExecution, { as: 'executions', foreignKey: 'checkRunId', foreignKeyConstraint: true });
    CheckExecution.belongsTo(CheckRun, { as: 'checkRun', foreignKey: 'checkRunId', foreignKeyConstraint: true, onDelete: 'CASCADE' });
}