import { Model, DataTypes, ModelAttributes } from 'sequelize';
import { Request } from './request';
import { Control } from './control';
import { Comment } from './comment';
import { Warning } from './warning';
import { Session } from './session';
import { Component } from './component';
import { Word } from './word';
import { Check } from './check';
import { EntryPoint } from './entryPoint';

/**
 * Resource model
 */
export class Resource extends Model {
    public id !: number;
    public identifier !: string;
    public url !: string;
    public completePathname !: string;
    public pathname !: string;
    public path !: string;
    public title !: string;
    public readonly requestId ?: number;
    public readonly request ?: Request;
    public readonly controls ?: Control[];
    public readonly components ?: Component[];
    public readonly words ?: Word[];
    public readonly sessionId ?: number;
    public readonly session ?: Session;
    public readonly checks ?: Check[];
    public readonly comments ?: Comment[];
    public readonly entryPoints ?: EntryPoint[];

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

/**
 * Resource attributes
 */
export const ResourceAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    identifier: {
        type: DataTypes.STRING
    },
    url: {
        type: DataTypes.STRING
    },
    completePathname: {
        type: DataTypes.STRING
    },
    pathname: {
        type: DataTypes.STRING
    },
    path: {
        type: DataTypes.STRING
    },
    title: {
        type: DataTypes.STRING
    }
}

/**
 * Resource factory
 */
export const ResourceFactory = () => {

    // Request
    Resource.belongsTo(Request, { as: "request", foreignKey: "requestId", foreignKeyConstraint: true});

    // Control
    Resource.hasMany(Control, { as: "controls", foreignKey: "resourceId", foreignKeyConstraint: true });
    Control.belongsTo(Resource, { as: "resource", foreignKey: "resourceId", foreignKeyConstraint: true, onDelete: "CASCADE" });

    // Comment
    Resource.hasMany(Comment, { as: 'comments', foreignKey: 'resourceId', foreignKeyConstraint: true });
    Comment.belongsTo(Resource, { as: 'resource', foreignKey: 'resourceId', foreignKeyConstraint: true });

    // Warning
    Warning.belongsTo(Resource, { as: 'resource', foreignKey: 'resourceId', foreignKeyConstraint: true, onDelete: 'CASCADE' });

    // Component
    Resource.hasMany(Component, { as: 'components', foreignKey: 'resourceId', foreignKeyConstraint: true });
    Component.belongsTo(Resource, { as: 'resource', foreignKey: 'resourceId', foreignKeyConstraint: true, onDelete: "CASCADE" });

    // Words
    Resource.hasMany(Word, { as: 'words', foreignKey: 'resourceId', foreignKeyConstraint: true });
    Word.belongsTo(Resource, { as: 'resource', foreignKey: 'resourceId', foreignKeyConstraint: true, onDelete: "CASCADE" });

    // Check
    Resource.hasMany(Check, { as: 'checks', foreignKey: 'resourceId', foreignKeyConstraint: true });
    Check.belongsTo(Resource, { as: 'resource', foreignKey: 'resourceId', foreignKeyConstraint: true, onDelete: 'CASCADE' });

    // Entry points
    Resource.hasMany(EntryPoint, { as: 'entryPoints', foreignKey: 'resourceId', foreignKeyConstraint: true });
    EntryPoint.belongsTo(Resource, { as: 'resource', foreignKey: 'resourceId', foreignKeyConstraint: true, onDelete: "CASCADE" });
}