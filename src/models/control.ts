import { Model, DataTypes, ModelAttributes } from 'sequelize';
import { CONTROL_TYPE } from '../common/constantes';
import { ControlAttribute } from './controlAttribute';
import { Resource } from './resource';

/**
 * Control model
 */
export class Control extends Model {
    public id !: number;
    public type !: string;
    public text !: string;
    public html !: string;
    public identifier !: string;
    public readonly resourceId ?: number;
    public readonly resource ?: Resource;
    public readonly attributes ?: ControlAttribute[];
    public readonly parentId ?: number;
    public readonly children ?: Control[];
    public readonly parent ?: Control;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

/**
 * Control attributes
 */
export const ControlAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    type: {
        type: DataTypes.ENUM(CONTROL_TYPE.FORM, CONTROL_TYPE.INPUT, CONTROL_TYPE.LINK, CONTROL_TYPE.TEXTAREA, 
            CONTROL_TYPE.H1, CONTROL_TYPE.H2, CONTROL_TYPE.H3, CONTROL_TYPE.H4, CONTROL_TYPE.H5, CONTROL_TYPE.H6)
    },
    text: {
        type: DataTypes.TEXT
    },
    html: {
        type: DataTypes.TEXT
    },
    identifier: {
        type: DataTypes.TEXT
    }
}

/**
 * Control factory
 */
export const ControlFactory = () => {

    // Attributes
    Control.hasMany(ControlAttribute, { as: 'attributes', foreignKey: 'controlId', foreignKeyConstraint: true });
    ControlAttribute.belongsTo(Control, { foreignKey: 'controlId', foreignKeyConstraint: true, onDelete: 'CASCADE' });

    // Controls
    Control.hasMany(Control, { as: "children", foreignKey: 'parentId', foreignKeyConstraint: true });
    Control.belongsTo(Control, { as: "parent", foreignKey: 'parentId', foreignKeyConstraint: true, onDelete: 'CASCADE'});
}