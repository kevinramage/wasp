import { Model, DataTypes, ModelAttributes, HasManyAddAssociationMixin, HasManyCountAssociationsMixin } from 'sequelize';
import { Request } from './request';
import { Resource } from './resource';

/**
 * Session model
 */
export class Session extends Model {
    public id !: number;
    public name !: string;
    public activated !: boolean;
    public readonly requests ?: Request[];
    public readonly applicationId ?: number;
    public readonly resources ?: Resource[];
    
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;

    public addRequest!: HasManyAddAssociationMixin<Request, number>;
    public countRequest!: HasManyCountAssociationsMixin;
}


/**
 * Session attributes
 */
export const SessionAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING
    },
    activated: {
        type: DataTypes.BOOLEAN
    }
}

/**
 * Session factory
 */
export const SessionFactory = () => {

    // Requests
    Session.hasMany(Request, { as: "requests", foreignKey: "sessionId", foreignKeyConstraint: true });
    Request.belongsTo(Session, { foreignKey: "sessionId", foreignKeyConstraint: true, onDelete: "CASCADE" });

    // Resouces
    Session.hasMany(Resource, { as: "resources", foreignKey: "sessionId", foreignKeyConstraint: true });
    Resource.belongsTo(Session, { as: "session", foreignKey: "sessionId", foreignKeyConstraint: true, onDelete: "CASCADE" });
}