import { Model, DataTypes, ModelAttributes } from 'sequelize';

/**
 * Word model
 */
export class Word extends Model {
    public id !: number;
    public word !: string;
    public counter !: number;
    public readonly resourceId !: number;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

/**
 * Word attributes
 */
export const WordAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    word: {
        type: DataTypes.STRING
    },
    counter: {
        type: DataTypes.INTEGER
    }
}