import { Model, DataTypes, ModelAttributes } from 'sequelize';

/**
 * Configuration model
 */
export class Configuration extends Model {
    public id !: number;
    public key !: string;
    public value !: string;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;    
}

/**
 * Configuration attributes
 */
export const ConfigurationAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    key: {
        type: DataTypes.STRING
    },
    value: {
        type: DataTypes.STRING
    }
}