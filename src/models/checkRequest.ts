import { Model, DataTypes, ModelAttributes } from 'sequelize';
import { Request } from './request';
import { Check } from './check';

/**
 * CheckRequest model
 */
export class CheckRequest extends Model {
    public id !: number;
    public name !: string;
    public readonly checkId !: number;
    public readonly check !: Check;
    public readonly requestId !: number;
    public readonly request !: Request;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

/**
 * CheckRequest attributes
 */
export const CheckRequestAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING
    }
}

export const CheckRequestFactory = () => {
    
    // Request
    CheckRequest.belongsTo(Request, { as: 'request', foreignKey: 'requestId', foreignKeyConstraint: true});
}