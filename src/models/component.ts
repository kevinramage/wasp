import { Model, DataTypes, ModelAttributes } from 'sequelize';
import { COMPONENT_TYPE } from '../common/constantes';

/**
 * Component model
 */
export class Component extends Model{
    public id !: number;
    public type !: string;
    public score !: number;
    public html !: string;
    public detail !: string;
    public readonly resourceId !: number;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

/**
 * Component attributes
 */
export const ComponentAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    type: {
        type: DataTypes.ENUM(COMPONENT_TYPE.LOGIN, COMPONENT_TYPE.LOGOUT, COMPONENT_TYPE.SEARCH, 
            COMPONENT_TYPE.ORDER, COMPONENT_TYPE.UPDATE_PROFILE, COMPONENT_TYPE.FORGET_PASSWORD,
            COMPONENT_TYPE.COMMENT, COMPONENT_TYPE.ARTICLE, COMPONENT_TYPE.SIGNUP)
    },
    score: {
        type: DataTypes.FLOAT
    },
    html: {
        type: DataTypes.TEXT
    },
    detail: {
        type: DataTypes.TEXT
    }
}