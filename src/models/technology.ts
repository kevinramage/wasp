import { Model, DataTypes, ModelAttributes } from 'sequelize';
import { TECHNOLOGY_TYPE } from '../common/constantes';

/**
 * Technology model
 */
export class Technology extends Model {
    public id !: number;
    public type !: string;
    public value !: string;
    public name !: string;
    public version !: string;
    public readonly applicationId ?: number;

    
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

/**
 * Technology attributes
 */
export const TechnologyAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    type: {
        type: DataTypes.ENUM(TECHNOLOGY_TYPE.CLIENT, TECHNOLOGY_TYPE.SERVER, TECHNOLOGY_TYPE.LANGUAGE, 
            TECHNOLOGY_TYPE.CLIENTLIB, TECHNOLOGY_TYPE.COOKIE, TECHNOLOGY_TYPE.EXTENSION, TECHNOLOGY_TYPE.DATABASE)
    },
    value: {
        type: DataTypes.STRING
    },
    name: {
        type: DataTypes.STRING
    },
    version: {
        type: DataTypes.STRING
    }
}