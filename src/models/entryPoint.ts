import { Model, ModelAttributes, DataTypes } from "sequelize";
import { Resource } from "./resource";
import { ENTRYPOINT_TYPE } from "../common/constantes";

/**
 * Entry point model
 */
export class EntryPoint extends Model {
    public id !: number;
    public name !: string;
    public type !: string;
    public readonly resourceId !: number;
    public readonly resource !: Resource;
    
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

/**
 * Entry point attributes
 */
export const EntryPointAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING
    },
    type: {
        type: DataTypes.ENUM(ENTRYPOINT_TYPE.QUERY, ENTRYPOINT_TYPE.BODY, ENTRYPOINT_TYPE.HEADER, ENTRYPOINT_TYPE.COOKIE)
    }
}