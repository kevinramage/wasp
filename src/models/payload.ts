import { Model, DataTypes, ModelAttributes } from 'sequelize';

export class Payload extends Model {
    public id !: number;
    public payload !: string;
    public injectionTypeId !: number;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

export const PayloadAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    payload: {
        type: DataTypes.STRING
    }
}