import { Model, DataTypes, ModelAttributes } from 'sequelize';
import { Request } from './request';
import { CHECKEXECUTION_STATUS } from '../common/constantes';
import { Check } from './check';
import { CheckRun } from './checkRun';

/**
 * CheckExecution model
 */
export class CheckExecution extends Model {
    public id !: number;
    public status !: string;
    public expected !: string;
    public actual !: string;
    public readonly checkId !: number;
    public check !: Check;
    public readonly checkRunId !: number;
    public checkRun !: CheckRun;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

/**
 * Check execution attributes
 */
export const CheckExecutionAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    status: {
        type: DataTypes.ENUM(CHECKEXECUTION_STATUS.NORUN, CHECKEXECUTION_STATUS.INPROGRESS, CHECKEXECUTION_STATUS.PASSED, CHECKEXECUTION_STATUS.FAILED)
    },
    expected: {
        type: DataTypes.STRING
    },
    actual: {
        type: DataTypes.STRING
    }
}

export const CheckExecutionFactory = () => {

}