import { Model, DataTypes, ModelAttributes } from 'sequelize';
import { Control } from './control';

/**
 * Control attribute model
 */
export class ControlAttribute extends Model {
    public id !: number;
    public key !: string;
    public value !: string;
    public html !: string;
    public readonly controlId ?: number;
    public readonly control ?: Control;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

/**
 * Control attribute attributes
 */
export const ControlAttributeAttributes : ModelAttributes = {
    id : { 
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    key: {
        type: DataTypes.STRING,
    },
    value: {
        type: DataTypes.STRING
    },
    html: {
        type: DataTypes.STRING
    }
}