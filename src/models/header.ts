import { Model, DataTypes, ModelAttributes } from 'sequelize';

/**
 * Header model
 */
export class Header extends Model {
    public id!: number;
    public key!: string;
    public value!: string;
    public requestId ?: string;
    public responseId ?: string;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}

/**
 * Header attributes
 */
export const HeaderAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    key: {
        type: DataTypes.STRING,
        allowNull: false
    },
    value: {
        type: DataTypes.TEXT,
        allowNull: false
    }
}