import { Model, DataTypes, ModelAttributes } from 'sequelize';
import { Session } from './session';
import { URL_TYPE } from '../common/constantes';
import { Technology } from './technology';
import { Warning } from './warning';
import { CheckRun } from './checkRun';

/**
 * Application model
 */
export class Application extends Model {
    public id !: number;
    public name !: string;
    public urlType : string;
    public readonly sessions !: Session[];
    public readonly technologies !: Technology[];
    public readonly warnings !: Warning[];
    public readonly runs !: CheckRun[];

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

/**
 * Application attributes
 */
export const ApplicationAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING
    },
    urlType: {
        type: DataTypes.ENUM(URL_TYPE.PAGE, URL_TYPE.VIEW)
    }
}

/**
 * Factory
 */
export const ApplicationFactory = () => {

    // Sessions
    Application.hasMany(Session, { as: "sessions", foreignKey: "applicationId", foreignKeyConstraint: true, onDelete: "CASCADE"});
    Session.belongsTo(Application, { foreignKey: "applicationId", foreignKeyConstraint: true, onDelete: "CASCADE" });

    // Technologies
    Application.hasMany(Technology, { as: "technologies", foreignKey: "applicationId", foreignKeyConstraint: true });
    Technology.belongsTo(Application, { foreignKey: "applicationId", foreignKeyConstraint: true, onDelete: "CASCADE"})

    // Warnings
    Application.hasMany(Warning, { as: 'warnings', foreignKey: 'applicationId', foreignKeyConstraint: true });
    Warning.belongsTo(Application, { as: 'application', foreignKey: 'applicationId', foreignKeyConstraint: true, onDelete: 'CASCADE' });

    // CheckRun
    Application.hasMany(CheckRun, { as: 'runs', foreignKey: 'applicationId', foreignKeyConstraint: true });
    CheckRun.belongsTo(Application, { foreignKey: 'applicationId', foreignKeyConstraint: true, onDelete: 'CASCADE' });
}