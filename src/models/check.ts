import { Model, DataTypes, ModelAttributes } from 'sequelize';
import { Request } from "./request";
import { Resource } from './resource';
import { CHECK_TYPE } from '../common/constantes';
import { CheckExecution } from './checkExecution';
import { Application } from './application';
import { CheckRequest } from './checkRequest';

/**
 * Check model
 */
export class Check extends Model {
    public id !: number;
    public type !: string;
    public name !: string;
    public requests ?: CheckRequest[];
    public resourceId ?: number;
    public resource ?: Resource;
    public applicationId ?: string;
    public application ?: Application;
    public readonly executions !: CheckExecution[];

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

/**
 * Check attributes
 */
export const CheckAttributes : ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    type: {
        type: DataTypes.ENUM(CHECK_TYPE.ERROR_HANDLING, CHECK_TYPE.INJECTION_HANDLING, CHECK_TYPE.PAGE_ENABLED)
    },
    name: {
        type: DataTypes.STRING
    }
}

/**
 * Factory
 */
export const CheckFactory = () => {

    // Check execution
    Check.hasMany(CheckExecution, { as: 'executions', foreignKey: 'checkId', foreignKeyConstraint: true });
    CheckExecution.belongsTo(Check, { as: 'check', foreignKey: 'checkId', foreignKeyConstraint: true, onDelete: 'CASCADE' });

    // Application
    Check.belongsTo(Application, { as: 'application', foreignKey: 'applicationId', foreignKeyConstraint: true });

    // Request
    Check.hasMany(CheckRequest, { as: 'requests', foreignKey: 'checkId', foreignKeyConstraint: true });
    CheckRequest.belongsTo(Check, { as: 'check', foreignKey: 'checkId', foreignKeyConstraint: true, onDelete: 'CASCADE' });
}