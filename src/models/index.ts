import { Sequelize } from 'sequelize';
import { Application, ApplicationAttributes, ApplicationFactory } from "./application";
import { Session, SessionAttributes, SessionFactory } from './session';
import { RequestFactory, Request, RequestAttributes } from './request';
import { ResponseFactory, Response, ResponseAttributes } from './response';
import { Header, HeaderAttributes } from './header';
import { ResourceFactory, Resource, ResourceAttributes } from './resource';
import { Technology, TechnologyAttributes } from './technology';
import { Control, ControlAttributes, ControlFactory } from './control';
import { ControlAttribute, ControlAttributeAttributes } from './controlAttribute';
import { Comment, CommentAttributes } from './comment';
import { Warning, WarningAttributes } from './warning';
import { Check, CheckFactory, CheckAttributes } from './check';
import { Component, ComponentAttributes } from './component';
import { Word, WordAttributes } from './word';
import { CheckExecution, CheckExecutionAttributes, CheckExecutionFactory } from './checkExecution';
import { CheckRun, CheckRunAttributes } from './checkRun';
import { CheckRequest, CheckRequestAttributes, CheckRequestFactory } from './checkRequest';
import { EntryPoint, EntryPointAttributes } from './entryPoint';
import { InjectionType, InjectionTypeAttributes, InjectionTypeFactory } from './injectionType';
import { Payload, PayloadAttributes } from './payload';
import { Configuration, ConfigurationAttributes } from './configuration';

/**
 * Instanciate database connexion
 * Instanciate the database models
 * @param sequelizeConfig 
 */
export const createModels = (sequelizeConfig : any) : Sequelize => {

    // Instanciate database connexion
    sequelizeConfig.host = process.env.MYSQL_HOST || sequelizeConfig.host;
    sequelizeConfig.password = process.env.MYSQL_PASSWORD || sequelizeConfig.password;
    const sequelize = new Sequelize(sequelizeConfig);

    // Instanciate the database models
    Application.init(ApplicationAttributes, { tableName: "Application", sequelize: sequelize });
    Session.init(SessionAttributes, { tableName: "Session", sequelize: sequelize });
    Header.init(HeaderAttributes, { tableName: "Header", sequelize: sequelize });
    Request.init(RequestAttributes, { tableName: "Request", sequelize: sequelize });
    Response.init(ResponseAttributes, { tableName: "Response", sequelize: sequelize });
    Resource.init(ResourceAttributes, { tableName: "Resource", sequelize: sequelize });
    Technology.init(TechnologyAttributes, { tableName: "Technology", sequelize: sequelize });
    Control.init(ControlAttributes, { tableName: "Control", sequelize: sequelize });
    ControlAttribute.init(ControlAttributeAttributes, { tableName: "ControlAttribute", sequelize: sequelize });
    Comment.init(CommentAttributes, { tableName: 'Comment', sequelize: sequelize });
    Warning.init(WarningAttributes, { tableName: 'Warning', sequelize: sequelize });
    Check.init(CheckAttributes, { tableName: 'Check', sequelize: sequelize });
    CheckRequest.init(CheckRequestAttributes, { tableName: 'CheckRequest', sequelize: sequelize });
    CheckRun.init(CheckRunAttributes, { tableName: 'CheckRun', sequelize: sequelize });
    CheckExecution.init(CheckExecutionAttributes, { tableName: 'CheckExecution', sequelize: sequelize });
    Component.init(ComponentAttributes, { tableName: 'Component', sequelize: sequelize });
    Word.init(WordAttributes, { tableName: 'Word', sequelize: sequelize });
    EntryPoint.init(EntryPointAttributes, { tableName: 'EntryPoint', sequelize: sequelize });
    InjectionType.init(InjectionTypeAttributes, { tableName: 'InjectionType', sequelize: sequelize });
    Payload.init(PayloadAttributes, { tableName: "Payload", sequelize: sequelize });
    Configuration.init(ConfigurationAttributes, { tableName: "Configuration", sequelize: sequelize });

    // Factory
    ApplicationFactory();
    SessionFactory();
    RequestFactory();
    ResponseFactory();
    ResourceFactory();
    ControlFactory();
    CheckFactory();
    CheckRequestFactory();
    CheckExecutionFactory();
    InjectionTypeFactory();

    return sequelize;
}