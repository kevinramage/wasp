import * as winston from "winston";
import DailyRotateFile = require("winston-daily-rotate-file");
import { DatabaseManager } from "./loaders/DatabaseManager";
import { APIManager } from "./loaders/APIManager";
import { ProxyManager } from "./loaders/ProxyManager";
import { jobFactory } from "./jobs";
import { ConfigurationManager } from "./loaders/ConfigurationManager";
import { WebSocketManager } from "./loaders/WebSocketManager";

async function main() {

    // Configure log system
    winston.add(
        new DailyRotateFile({
            filename: "logs/WASP_%DATE%.log",  
            datePattern: 'YYYY-MM-DD',
            level: 'debug',
            zippedArchive: true,
            maxSize: '20m',
            maxFiles: '14d'
        })
    );
    winston.add(new winston.transports.Console({ level: "info" }));
    winston.info("Index.main - Starting ...");

    // Initialize the database
    await DatabaseManager.instance.initialize();

    // Initialize proxy
    ProxyManager.instance.initialize();

    // Initialize services
    new APIManager().initialize();

    // Initialize jobs
    jobFactory();

    // Initialize configuration manager
    ConfigurationManager.instance.init();

    // Initialize web socket system
    WebSocketManager.instance.init();
}

main();