export module COMMON_PORT {
    export const COMMON_HOST = "localhost";
    export const API_PORT = 7000;
    export const PROXY_PORT = 7001;
    export const SOCKET_PORT = 7002;
}

export module COMMON_ERROR {
    export const INTERNAL_ERROR = {
        code: "INTERNAL ERROR",
        message: "An internal error occured, please contact your administrator"
    }
    export const RESOURCE_NOT_FOUND = {
        code: "RESOURCE NOT FOUND",
        message: "%s not found" 
    }
    export const BAD_REQUEST_REQUIRED = {
        code: "BAD REQUEST",
        message: "The field %s is required"
    }
    export const BAD_REQUEST_FORMAT = {
        code: "BAD REQUEST",
        message: "The field %s doesn't have the good format"
    }
}

export module HEADER_KEY {
    export const CONTENT_TYPE = "content-type";
    export const CONTENT_LENGTH = "content-length";
    export const HOST = "host";
    export const PROXY = "proxy";
    export const SERVER = "server";
    export const XPOWEREDBY = "x-powered-by";
    export const COOKIE = "cookie";
    export const SETCOOKIE = "set-cookie";
    export const CONTENTSECURITYPOLICY = "content-security-policy";
    export const XCONTENTSECURITYPOLICY = "x-content-security-policy";
    export const FEATUREPOLICY = "feature-policy";
    export const REFERRER_POLICY = "referrer-policy";
    export const XFRAMEOPTIONS = "x-frame-options";
    export const XXSSPROTECTION = "x-xss-protection";
    export const USERAGENT = "user-agent";
    export const TDSVERSION = "tds-version";
}

export module CONTENTTYPE_VALUE {
    export const HTML = "text/html";
}

export module URL_TYPE {
    export const PAGE = "PAGE";
    export const VIEW = "VIEW";
}

export const URLTYPE_PAGESCORE = 0.9;
export const ANALYZEURL_MINREQUEST = 3;

export module TECHNOLOGY_TYPE {
    export const CLIENT = "CLIENT";
    export const SERVER = "SERVER";
    export const COOKIE = "COOKIE";
    export const LANGUAGE = "LANGUAGE";
    export const CLIENTLIB = "CLIENTLIB";
    export const EXTENSION = "EXTENSION";
    export const DATABASE = "DATABASE";
}

export module CONTROL_TYPE {
    export const LINK = "LINK";
    export const FORM = "FORM";
    export const INPUT = "INPUT";
    export const TEXTAREA = "TEXTAREA";
    export const H1 = "H1";
    export const H2 = "H2";
    export const H3 = "H3";
    export const H4 = "H4";
    export const H5 = "H5";
    export const H6 = "H6";
}

export module WARNING_SEVERITY {
    export const MINOR = "MINOR";
    export const MAJOR = "MAJOR";
    export const SEVERE = "SEVERE";
    export const CRITICAL = "CRITICAL";
}

export module HTTP_METHOD {
    export const GET = "GET";
    export const POST = "POST";
    export const PUT = "PUT";
    export const DELETE = "DELETE";
}

export module ATTRIBUTE_NAME {
    export const ID = "id";
    export const NAME = "name";
    export const TYPE = "type";
    export const METHOD = "method";
    export const ACTION = "action";
    export const VALUE = "value";
    export const HREF = "href";
}

export module INPUT_TYPE {
    export const TEXT = "text";
    export const PASSWORD = "password";
    export const SUBMIT = "submit";
    export const CHECKBOX = "checkbox";
    export const MAIL = "mail";
    export const TEL = "tel";
}

export module COMPONENT_TYPE {
    export const LOGIN = "LOGIN";
    export const LOGOUT = "LOGOUT";
    export const SEARCH = "SEARCH";
    export const ORDER = "ORDER";
    export const UPDATE_PROFILE = "UPDATE_PROFILE";
    export const FORGET_PASSWORD = "FORGET_PASSWORD";
    export const COMMENT = "COMMENT";
    export const ARTICLE = "ARTICLE";
    export const SIGNUP = "SIGNUP";
}

export module COMPONENT_COMMON {
    export const LOGIN_URL = [ "log", "signin" ];
    export const LOGIN_KEYWORDS = [ "user", "mail", "pseudo", "account", "id", "name"];
    export const USERNAME_KEYWORDS = [ "user", "pseudo", "account", "id", "name" ];
    export const PASSWORD_KEYWORDS = [ "pass" ];
    export const LOGIN_SUBMIT_KEYWORD = [ "log", "connect", "sign" ];
    export const LOGIN_REMEMBER_KEYWORD = [ "remember", "auto" ];
    export const LOGOUT_KEYWORDS = [ "logout", "signout", "disconnect", "exit", "quit", "goodbye"];
    export const MAIL_KEYWORDS = ["mail"];
    export const PHONE_KEYWORDS = [ "phone", "number", "tel" ];
    export const ADDRESS_KEYWORDS = [ "address" ];
    export const CITY_KEYWORDS = [ "city" ];
    export const UPDATE_KEYWORDS = [ "update", "modify", "submit" ];
    export const PROFIL_KEYWORDS = [ "user", "profil", "account" ];
    export const COMMENT_KEYWORDS = [ "comment", "note", "remark", "guestbook" ];
    export const ADDCOMMENT_KEYWORDS = [ "add", "write", "update", "post", "submit" ];
    export const SEARCH_KEYWORDS = [ "search", "query", "request" ];
    export const SEARCH_SUBMIT_KEYWORDS = [ "search", "query", "request", "go" ];
}

export module CHECK_TYPE {
    export const PAGE_ENABLED = "PAGE_ENABLED";
    export const ERROR_HANDLING = "ERROR_HANDLING";
    export const INJECTION_HANDLING = "INJECTION_HANDLING";
}

export module CHECK_NAME {
    export const PHPINFO_ENABLED = "PHPINFO_ENABLED";
    export const RESOURCE_ACCESS = "RESOURCE_ACCESS";
}

export module CHECKEXECUTION_STATUS {
    export const NORUN = "NORUN";
    export const INPROGRESS = "INPROGRESS";
    export const FAILED = "FAILED";
    export const PASSED = "PASSED";
}

export module ENTRYPOINT_TYPE {
    export const QUERY = "QUERY";
    export const BODY = "BODY";
    export const HEADER = "HEADER";
    export const COOKIE = "COOKIE";
}

export module INJECTIONTYPE_TYPE {
    export const SQL_BOOLEAN_OR = "SQL_BOOLEAN_OR";
    export const SQL_BOOLEAN_AND = "SQL_BOOLEAN_AND";
    export const SQL_TIME = "SQL_TIME";
    export const REFLECTED_XSS = "REFLECTED_XSS";
}

export type INJECTION_STATUS = "FAILED" | "WARNING" | "SUCCESS";
export module INJECTION_STATUS {
    export const FAILED = "FAILED";
    export const SUCCESS = "SUCCESS";
    export const WARNING = "WARNING";
}
export type INJECTIONPAYLOAD_STATUS = "INJECTED" | "NOT_INJECTED" | "FAILED";
export module INJECTIONPAYLOAD_STATUS {
    export const INJECTED = "INJECTED";
    export const NOT_INJECTED = "NOT_INJECTED";
    export const FAILED = "FAILED";
}

export module INJECTION_ERRORMESSAGE {
    export const UNKNOWN_INJECTIONTYPE = "Unknown injection type";
    export const REFERENCE_INJECTION_FAILED = "Reference query execution failed";
}

export module CONFIGURATION {
    export const INJECTION = {
        AGENT_NUMBER: "injection.agentnumber",
        //FOLLOW_REDIRECT: "injection.followredirect",
        TIMEOUT: "injection.timeout",
        USERAGENT: "injection.useragent",
        PROXY: {
            ENABLED: "injection.proxy.enabled",
            HOST: "injection.proxy.host",
            PORT: "injection.proxy.port",
            /*USERNAME: "injection.proxy.username",
            PASSWORD: "injection.proxy.password"*/
        },
        CUSTOM_HEADER: {
            ENABLED: "injection.customheader.enabled",
            KEY: "injection.customheader.key",
            VALUE: "injection.customheader.value"
        },
        TDS_HEADER: {
            ENABLED: "injection.tdsheader.enabled",
            VALUE: "injection.tdsheader.value"
        }
    }
    export const PROXY = {
        DISPLAY: {
            FIREFOX_DETECTPORTAL: "proxy.display.firefoxdetectportal"
        }
    }
}

export module CONTENT_TYPE {
    export const CSS = "CSS";
    export const JS = "JS";
    export const HTML = "HTML";
    export const ICO = "ICO";
    export const IMG = "IMG";
    export const DATA = "DATA";
    export const OTHER = "OTHER";
}