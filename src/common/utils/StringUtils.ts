import * as winston from "winston";

/**
 * Util class to operate on string
 */
export class StringUtils {

    /**
     * Remove some characters
     * @param expression expression to update
     * @param keywords keywords to remove
     */
    static removeChars(expression: string, keywords: Array<string>) {
        keywords.forEach(k => {
            const index = expression.indexOf(k);
            if ( index != 1 ) {
                expression = expression.substr(0, index) + expression.substr(index+1);
            }
        });
        return expression;
    }

    /**
     * Check if an expression contains one of the keyword present in the array
     * @param expression expression to check
     * @param keywords keywords to search
     * @returns return true if one of keywords find, false else
     */
    static containsOneOf(expression: string, keywords : Array<string>) : boolean {
        var result : boolean = false;

        // Security
        if ( !expression ) {
            return false;
        }

        keywords.forEach(k => {
            if ( expression.includes(k)) {
                result = true;
                return;
            }
        });
        return result;
    }

    /**
     * Check if an expression equals to one element of a list
     * @param expression expression to test
     * @param keywords reference keywords
     */
    static equalsOneOf(expression: string, keywords : Array<string>) : boolean {
        var result : boolean = false;

        // Security
        if ( !expression ) {
            return false;
        }

        keywords.forEach(k => {
            if ( expression == k ) {
                result = true;
                return;
            }
        });
        return result;
    }    

    /**
     * Fill a string with specific character x times
     * @param character character to repeat x times
     * @param length the repetition
     * @returns a new string filled
     */
    static fill (character: string, length: number) : string {
        var result = "";
        for (var i = 0; i < length; i++) {
            result += character;
        }
        return result;
    }
}