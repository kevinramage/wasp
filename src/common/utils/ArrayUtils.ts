export class ArrayUtils {

    /**
     * Method to identify if an element is unique
     * @param value item's value 
     * @param index item's index 
     * @param self the array
     * @return an boolean to indicate if the element is unique or not
     */
    public static isUnique (value, index, self) {
        return self.indexOf(value) === index;
    }

    /**
     * Count all occurennces present on a string array
     * @param array array to analyze
     */
    public static countOccurences ( array : Array<string> ) : { [key : string] : number } {
        const counter = {};
        array.forEach(elt => {
            if ( counter[elt] ) {
                counter[elt]++;
            } else {
                counter[elt] = 1;
            }
        });
        return counter;
    }

    /**
     * Get the maximum occurence of a counter
     * @param counter counter to analyze
     */
    public static getMaxElement(counter : { [ key : string ] : number }){
        return Object.keys(counter).reduce((a, b) => { 
            return counter[a] > counter[b] ? a : b
        });
    }
}