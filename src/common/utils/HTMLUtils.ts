import * as winston from "winston";
import { IControl } from "../interface/control";
import { IControlAttribute } from "../interface/controlAttribute";
import { CONTROL_TYPE, ATTRIBUTE_NAME } from "../constantes";
import { StringUtils } from "./StringUtils";

/**
 * Util class to operate on HTML
 */
export class HTMLUtils {

	/**
	 * Analyze the html code and extract control from tag name provided
	 * @param tagName tag name to search
	 * @param controlType control type to assign on control built
	 * @param html html code to analyze
	 */
	private static extractTag(tagName : string, controlType : string, html : string ) : Array<IControl> {
		winston.debug("HTMLUtils.extractTag: ", tagName);
		
		// Remove endline characters
		html = html.replace(/\r/g, "").replace(/\n/g, " ");

		// Prepare regex
		var doubleTagRegexString = "<##TAG##((\\s*[\\w|:|_|-]*=(\\\"|')[\\w|\\.|:|\\/|\\s|\\?|=|_|&|;|#|+|!|(|)|,|-]*(\\\"|'))*)>(((?!<\\/##TAG##>).)*)<\\/##TAG##>";
		var singleTagRegexString = "<##TAG##((\\s*[\\w|:|_|-]*=(\\\"|')[\\w|\\.|:|\\/|\\s|\\?|=|_|&|;|#|+|!|(|)|,|-]*(\\\"|'))*)\\/?>";
		doubleTagRegexString = doubleTagRegexString.replace(/##TAG##/g, tagName);
		singleTagRegexString = singleTagRegexString.replace(/##TAG##/g, tagName);
		const doubleTagRegex = new RegExp(doubleTagRegexString, 'gi');
		const singleTagRegex = new RegExp(singleTagRegexString, 'gi');
		

		// Collect control
		var controls = this.extractTagWithRegex(tagName, controlType, html, doubleTagRegex);
		if ( controls.length === 0 ) {
			controls = this.extractTagWithRegex(tagName, controlType, html, singleTagRegex);
		}

		return controls;
    }
    
	/**
	 * Analyze the html code and extract control from tag name and regex provided
	 * @param tagName tag name to search
	 * @param controlType control type to assign on control built
	 * @param html html code to analyze
	 * @param regex regex to use
	 */
	private static extractTagWithRegex(tagName: string, controlType: string, html: string, regex : RegExp) : Array<IControl> {
		winston.debug("HTMLUtils.extractTagWithRegex: ", tagName, regex);

		const attributeRegex = /\s*([\w|:|_|-]*)=(\"|')([\w|\.|:|\/|\s|\?|=|_|&|;|#|+|!|(|)|,|-]*)(\"|')/g;
		const controls : Array<IControl> = [];
		var match : RegExpExecArray;
		while ( match = regex.exec(html) ) {

			// Build control
			const control : IControl = {};
			control.html = match.length > 0 ? match[0] : null;
			control.text = match.length > 5 ? match[5] : null;
			control.attributes = [];
			control.children = [];
			control.type = controlType;

			// Collect attribute
			var matchAttribute : RegExpExecArray;
			if ( match.length > 1 ) {
				while ( matchAttribute = attributeRegex.exec(match[1]) ) {
					const attribute : IControlAttribute = <IControlAttribute> {
						html: matchAttribute.length > 0 ? matchAttribute[0] : null,
						key: matchAttribute.length > 1 ? matchAttribute[1] : null,
						value: matchAttribute.length > 3 ? matchAttribute[3] : null
					}
					control.attributes.push(attribute);
				}
			}

			// Compute identifier
			const idAttribute = control.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.ID});
			if ( !idAttribute ) {
				const nameAttribute = control.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.NAME});
				if ( !nameAttribute ) {
					const actionAttribute = control.attributes.find(a => { return a.key.toLowerCase() === ATTRIBUTE_NAME.ACTION});
					if ( !actionAttribute ) {
						if ( !control.text || control.text.indexOf("<") != -1 && control.text.indexOf(">") != -1 ) {
							 control.identifier = "";
						} else {
							control.identifier = control.text.substr(0, 25);
						}
					} else {
						const queryIndex = actionAttribute.value.indexOf("?");
						if ( queryIndex > -1 ) {
							control.identifier = actionAttribute.value.substr(0, queryIndex);
						} else {
							control.identifier = actionAttribute.value;
						}
					}
				} else {
					control.identifier = nameAttribute.value;
				}
			} else {
				control.identifier = idAttribute.value;
			}

			// Collect children
			if ( controlType === CONTROL_TYPE.FORM && match.length > 5) {

				// Inputs
				var subControls = HTMLUtils.extractInputs(match[5]);
				subControls.forEach(c => { c.parent = control; });
				control.children = control.children.concat(subControls);

				// Textarea
				var subControls = HTMLUtils.extractTextArea(match[5]);
				subControls.forEach(c => { c.parent = control; });
				control.children = control.children.concat(subControls);
			}

			controls.push(control);
		}

		return controls;
	}

    /**
     * Extract form present in html code
     * @param html the HTML string to analyze
     * @returns a list of forms found
     */
    public static extractForm(html : string) : Array<IControl> {
		return HTMLUtils.extractTag("form", CONTROL_TYPE.FORM, html);
	}
	
    /**
     * Extract inputs present in html code
     * @param html the HTML string to analyze
     * @returns a list of inputs found
     */
    private static extractInputs(html : string) : Array<IControl> {
        return HTMLUtils.extractTag("input", CONTROL_TYPE.INPUT, html);
    }
	
    /**
     * Extract textarea present in html code
     * @param html the HTML string to analyze
     * @returns a list of textarea found
     */
    private static extractTextArea(html : string) : Array<IControl> {
        return HTMLUtils.extractTag("textarea", CONTROL_TYPE.TEXTAREA, html);
    }
	
    /**
     * Extract links present in html code
     * @param html the HTML string to analyze
     * @returns a list of links found
     *
	 */
    public static extractLinks(html : string) : Array<IControl> {
        return HTMLUtils.extractTag("a", CONTROL_TYPE.LINK, html);
	}
	
	/**
     * Extract H1 present in html code
     * @param html the HTML string to analyze
     * @returns a list of H1 found
     *
	 */
    public static extractH1(html : string) : Array<IControl> {
        return HTMLUtils.extractTag("h1", CONTROL_TYPE.H1, html);
	}

	/**
     * Extract H2 present in html code
     * @param html the HTML string to analyze
     * @returns a list of H2 found
     *
	 */
    public static extractH2(html : string) : Array<IControl> {
        return HTMLUtils.extractTag("h2", CONTROL_TYPE.H2, html);
	}
	
	/**
     * Extract H3 present in html code
     * @param html the HTML string to analyze
     * @returns a list of H3 found
     *
	 */
    public static extractH3(html : string) : Array<IControl> {
        return HTMLUtils.extractTag("h3", CONTROL_TYPE.H3, html);
	}
	
	/**
     * Extract H4 present in html code
     * @param html the HTML string to analyze
     * @returns a list of H4 found
     *
	 */
    public static extractH4(html : string) : Array<IControl> {
        return HTMLUtils.extractTag("h4", CONTROL_TYPE.H4, html);
	}
	
	/**
     * Extract H5 present in html code
     * @param html the HTML string to analyze
     * @returns a list of H5 found
     *
	 */
    public static extractH5(html : string) : Array<IControl> {
        return HTMLUtils.extractTag("h5", CONTROL_TYPE.H5, html);
	}
	
	/**
     * Extract H6 present in html code
     * @param html the HTML string to analyze
     * @returns a list of H6 found
     *
	 */
    public static extractH6(html : string) : Array<IControl> {
        return HTMLUtils.extractTag("h6", CONTROL_TYPE.H6, html);
	}

	/**
	 * Extract all words of an HTML page
	 * @param html html code to analyze
	 */
	public static extractWords(html : string ) : string [] {
		winston.debug("HtmlUtils.extractWords");
		const words : Array<string> = [];
		const commonWords = ["and", "or", "nor", "in", "out", "on", "off", "of",
			"1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "zero", "one", "two", "three",
			"i", "you", "he", "she", "it", "we", "they", "us",
			"is", "have", "will", "do", "to", "can", "could", "should", "would",
			"mine", "your", "her", "his", "its", "our", "their", 
			"if", "else", "for", "into","how", "which", "what", "who", "where", "not",
			"the", "a", "an", "all", "every", "as", "well", "so", "other", "others", "someone",
			"this", "these", "from", "also", "already", "too", "more", "than", "less"]
		const symbols = [ ",", "?", ";", ".", ":", "/", "!", "$", "*", "_", "-", "|", "(", "[", ")", "]", "="];

		const regexWord = /(?<=>)[^<>]+(?=<)/g;
		var results = regexWord.exec(html);
		while ( results != null ) {
			results.forEach(r => {
				const wordsText = r.replace(/'/g, "").replace(/"/g, "").replace(/`/g, "");
				wordsText.split(" ").forEach(wt => {
					wt = wt.toLowerCase();
					wt = StringUtils.removeChars(wt, symbols);
					if ( wt.trim() != "") {

						if ( !StringUtils.equalsOneOf(wt, symbols)) {
							if (!StringUtils.equalsOneOf(wt, commonWords)) {
								words.push(wt);
							}
						}
					}
				});
			});
			results = regexWord.exec(html);
		}

		return words;
	}

	/**
	 * Extract title tag
	 * Return the title text or undefined if tag not found
	 * @param html html to analyze
	 */
	public static extractTitle(html : string) : string {
		winston.debug("HtmlUtils.extractTitle");
		html = html.replace(/\r/g, "").replace(/\n/g, " ");
		const regexTitle = /<title>(.*)<\/?title>/gi;
		const matches = regexTitle.exec(html);
		if ( matches != null && matches.length > 1 ) {
			return matches[1];
		} else {
			return undefined;
		}
	}

	/**
	 * Check if the content is HTML content
	 * @param html html
	 */
	public static isHTMLContent(html : string ) : boolean {
		winston.debug("HtmlUtils.isHTMLContent");
		html = html.replace(/\r/g, "").replace(/\n/g, " ");
		return /<[a-z][\s\S]*>/i.test(html);
	}

	/**
	 * Extract comments present in html page
	 * @param html html to study
	 */
	public static extractComments(html : string ) : string[] {
		winston.debug("HtmlUtils.extractComments");
		const comments : string[] = [];

		const regex = /<!--([^>]*)-->/g;
		html = html.replace(/\r/g, "").replace(/\n/g, " ");
		var match : RegExpExecArray;
		while ( match = regex.exec(html) ) {
			if ( match.length > 1 ) {
				comments.push(match[1]);
			}
		}
		return comments;
	}
}