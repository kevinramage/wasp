export class DateUtils {

    /**
     * Fill a value with 0 to have 3 digits
     * @param value value to fill
     */
    private static threeDigits(value: string) : string {
        if ( value.length < 3 ) {
            return "00" + value;        
        } else if ( value.length < 2 ) {
            return "0" + value;
        } else {
            return value;
        }
    }

    /**
     * Fill a value with 0 to have 2 digits
     * @param value value to fill
     */
    private static twoDigits(value: string) : string {
        if ( value.length < 2 ) {
            return "0" + value;
        } else {
            return value;
        }
    }

    /**
     * Format a JS date to MySql format
     * @param date date to format
     */
    public static formatToMysqlFormat(date: Date) : string {

        // Security
        if ( !date ) {
            return null;
        }

        const year : string = date.getFullYear() + "";
        const month = DateUtils.twoDigits((date.getMonth() + 1) + "");
        const day = DateUtils.twoDigits(date.getDate() + "");
        const hour = DateUtils.twoDigits(date.getHours() + "");
        const minute = DateUtils.twoDigits(date.getMinutes() + "");
        const second = DateUtils.twoDigits(date.getSeconds() + "");
        const millisecond = DateUtils.threeDigits(date.getMilliseconds() + "");

        const format = "YYYY-MM-DD HH:mm:ss.sss";
        return format.replace("YYYY", year)
            .replace("MM", month)
            .replace("DD", day)
            .replace("HH", hour)
            .replace("mm", minute)
            .replace("ss", second)
            .replace("sss", millisecond)
    }

    /**
     * Pretty print a date diff
     * @param diff diff to apply
     */
    public static prettyPrint(diff: number) {
        var value : number = diff;
        var unity : string = "ms"

        // Security
        if ( !value ) {
            return "";
        }

        // Milliseconds to seconds
        if ( value > 1000 ) {
            value = value / 1000.0;
            unity = "s";
        }

        // Seconds to minutes
        if ( value > 1000 ) {
            value = value / 60.0;
            unity = "min";
        }

        return Math.round(value * 100) / 100 + " " + unity;
    }
}