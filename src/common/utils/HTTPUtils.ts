import { IHeader } from "../interface/header";
import { Request } from "../../models/request";
import { IRequest } from "../interface/request";
import { HEADER_KEY, CONTENTTYPE_VALUE, CONTENT_TYPE } from "../constantes";
import winston = require("winston");
import { IResponse } from "../interface/response";

/**
 * Util class to operate on HTTP system
 */
export class HTTPUtils {

    /**
     * Transform key / value dictionnary on class instances
     * @param headers headers to transform
     */
    public static transformHeaders(headersDTO: object): IHeader[] {
        const headers : IHeader[] = [];
        Object.keys(headersDTO).forEach(k => {
            headers.push({
                key: k,
                value: headersDTO[k] + ""
            });
        });
        return headers;
    }

    /**
     * Get complete url of a request
     * @param request request to analyze
     */
    public static getCompleteUrl(request : Request | IRequest) {
        const host = request.headers.find(h => { return h.key.toLowerCase() == HEADER_KEY.HOST; });
        if ( host ) {
            return request.protocol + "://" + host.value + request.url;
        } else {
            return request.url;
        }
    }

    /**
     * Identify if the request is an XHR request or not
     * @param request request to analyze
     */
    public static isXHRRequest(request : Request ) {
        winston.debug("HTTPUtils.isXHRRequest")

        // Filter on extension first (Avoid issue on response missing for static content)
        if ( request.url && (request.url.endsWith(".ico") || request.url.endsWith(".txt") || request.url.endsWith(".css") ||
        request.url.endsWith(".gif") || request.url.endsWith(".png") || request.url.endsWith(".jpg") )) {
            return false;
        }

        // Check the content type
        if ( request.response && request.response.headers ) {

            // Check the content type header
            const contentType = request.response.headers.find(h => { return h.key && h.key.toLowerCase() == HEADER_KEY.CONTENT_TYPE; });
            return contentType && contentType.value && contentType.value.toLowerCase().startsWith(CONTENTTYPE_VALUE.HTML); 
        } else {
            if ( !request.response ) {
                winston.error("HTTPUtils.isXHRRequest - Some values missing request.response: " + request.url);
                throw "HTTPUtils.isXHRRequest - Some values missing request.response";
            } else if ( !request.response.headers ) {
                winston.error("HTTPUtils.isXHRRequest - Some values missing request.response.headers: " +request.url);
            } else {
                winston.error("HTTPUtils.isXHRRequest - Some values missing request.url");
            }
            return false;
        }
    }

    /**
     * Identify the response type
     * @param request request to analyze
     * @param response response to analyze
     */
    public static identifyType(request: IRequest, response: IResponse) {
        const contentType = response.headers.find(h => { return h.key === HEADER_KEY.CONTENT_TYPE; });
        if ( contentType ) {
            return this.identifyTypeWithContentType(contentType.value as string);
        } else {
            return this.identifyTypeWithUrl(request.url);
        }
    }

    /**
     * Identify the response type from the content type header
     * @param contentType content type header to analyze
     */
    private static identifyTypeWithContentType(contentType: string) {
        switch ( contentType ) {
            case "text/css":
                return CONTENT_TYPE.CSS;
            case "application/javascript":
            case "application/typescript":
                return CONTENT_TYPE.JS;
            case "text/html":
            case "application/xhtml+xml":
                return CONTENT_TYPE.HTML;
            case "image/x-icon":
                return CONTENT_TYPE.ICO;
            case "image/gif":
            case "image/jpeg":
            case "image/png":
            case "image/svg+xml":
            case "image/tiff":
            case "image/webp":
                return CONTENT_TYPE.IMG;
            case "text/csv":
            case "text/plain":
            case "application/json":
            case "application/rtf":
            case "application/xml":
                return CONTENT_TYPE.DATA;
            default:
                return CONTENT_TYPE.OTHER;
        }
    }

    /**
     * Identify the response type from the url
     * @param url url to analyze
     */
    private static identifyTypeWithUrl(url: string) {
        if ( url.endsWith(".css")) { return CONTENT_TYPE.CSS; }
        else if ( url.endsWith(".js")) { return CONTENT_TYPE.JS }
        else if ( url.endsWith(".ts")) { return CONTENT_TYPE.JS }
        else if ( url.endsWith(".htm")) { return CONTENT_TYPE.HTML }
        else if ( url.endsWith(".html")) { return CONTENT_TYPE.HTML }
        else if ( url.endsWith(".xhtml")) { return CONTENT_TYPE.HTML }
        else if ( url.endsWith(".ico")) { return CONTENT_TYPE.ICO }
        else if ( url.endsWith(".gif")) { return CONTENT_TYPE.IMG }
        else if ( url.endsWith(".jpeg")) { return CONTENT_TYPE.IMG }
        else if ( url.endsWith(".jpg")) { return CONTENT_TYPE.IMG }
        else if ( url.endsWith(".png")) { return CONTENT_TYPE.IMG }
        else if ( url.endsWith(".svg")) { return CONTENT_TYPE.IMG }
        else if ( url.endsWith(".tif")) { return CONTENT_TYPE.IMG }
        else if ( url.endsWith(".tiff")) { return CONTENT_TYPE.IMG }
        else if ( url.endsWith(".webp")) { return CONTENT_TYPE.IMG }
        else if ( url.endsWith(".csv")) { return CONTENT_TYPE.DATA }
        else if ( url.endsWith(".txt")) { return CONTENT_TYPE.DATA }
        else if ( url.endsWith(".json")) { return CONTENT_TYPE.DATA }
        else if ( url.endsWith(".rtf")) { return CONTENT_TYPE.DATA }
        else if ( url.endsWith(".xml")) { return CONTENT_TYPE.DATA }
        else { return CONTENT_TYPE.OTHER; }
    }
}