/**
 * Util class to operate on URL
 */
export class URLUtils {

    /**
     * Extract the pathname of an url
     * @param url url to analyze
     */
    public static getPathName(url : string) {
        const myUrl = new URL(url);
        return myUrl.pathname;
    }

    /**
     * Extract the last pathname of an url (last occurrence after the last /)
     * @param url url to analyze
     */
    public static getLastPathName(url : string) {
        var pathname = URLUtils.getPathName(url);
        const lastIndexOf = pathname.lastIndexOf("/");
        pathname = pathname.substr(lastIndexOf + 1);
        return pathname;
    }

    /**
     * Get query params of the url
     * @param url url
     */
    public static getQueryParams(url : string ) {
        const params : { [key: string] : string } = {};
        const searchParams = new URL(url).searchParams;
        const it = searchParams.entries();
        let result = it.next();
        while ( !result.done ) {
            params[result.value[0].toLowerCase()] = result.value[1];
            result = it.next();
        }
        return params;
    }
}