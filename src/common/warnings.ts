// Version found in server header 
export module WARNING_SERVER_VERSION {
    export const type = "WARNING_SERVER_VERSION";
    export const title = "Exposition of software version";
    export const message = "We found a software version exposed in header \"${HEADER_NAME}\" with the value \"${HEADER_VALUE}\"";
    export const risk = "Expose a version provide informations to hackers, version informations can help hacker to find some vunerabilities with CVE.";
}

// Version found in x-powered-by header 
export module WARNING_APP_VERSION {
    export const type = "WARNING_APP_VERSION";
    export const title = "Exposition of software version";
    export const message = "We found a software version exposed in header \"${HEADER_NAME}\" with the value \"${HEADER_VALUE}\"";
    export const risk = "Expose a version provide informations to hackers, version informations can help hacker to find some vunerabilities with CVE.";
}

// No header content-security-policy found and no meta tag found with value content-security-policy
export module WARNING_CSP_NOTPRESENT {
    export const type = "WARNING_CSP_NOTPRESENT";
    export const title = "Content Security Policy not present";
    export const message = "We not found your Content Security Policy header, CSP allow you to add security restrictions for you application.";
    export const risk = "Using CSP can prevent and protect against some security issues, not use it";
}

// No header x-content-security-policy found and no meta tag found with value x-content-security-policy
export module WARNING_XCSP_NOTPRESENT {
    export const type = "WARNING_XCSP_NOTPRESENT";
    export const title = "X Content Security Policy not present";
    export const message = "We not found your Content Security Policy header, CSP allow you to add security restrictions for you application.";
    export const risk = "Using CSP can prevent and protect against some security issues, not use it"; 
}

// Presence of set-cookie header and http protocole used
export module WARNING_WEAKPWD_TRANSMISSION {
    export const type = "WARNING_WEAKPWD_TRANSMISSION";
    export const title = "Password send through HTTP protocol";
    export const message = "We found a weak login system, the login credential send to server without any encryption system.";
    export const risk = "An attacker can intercept user's credentials.";
}

// Presence of same value between post body and set cookie value
export module WARNING_PWDSTOREDINCOOKIE {
    export const type = "WARNING_PWDSTOREDINCOOKIE";
    export const title = "Password store in clear in session cookie";
    export const message = "We found some password store in cookie";
    export const risk = "An attacker can intercept user's password."; 
}

// No header upgrade-insecure-requests
export module WARNING_UPGRADEINSECUREREQUESTS_NOTPRESENT {
    export const type = "WARNING_UPGRADEINSECUREREQUESTS_NOTPRESENT";
    export const title = "Upgrade-Insecure-Requests header not present";
    export const message = "Upgrade-Insecure-Requests header help you to use HTTPS protocol instead of HTTP if possible";
    export const risk = "Users can be vulberable with http procotol whereas https protocol is available"; 
}

// No header block-all-mixed-content
export module WARNING_BLOCKALLMIXEDCONTENT_NOTPRESENT {
    export const type = "WARNING_BLOCKALLMIXEDCONTENT_NOTPRESENT";
    export const title = "Block-All-Mixed-Content header not present";
    export const message = "Block-All-Mixed-Content header help you to block HTTP protocol";
    export const risk = "Users can be vulberable with http procotol"; 
}

// No header feature-policy
export module WARNING_FEATUREPOLICY_NOTPRESENT {
    export const type = "WARNING_FEATUREPOLICY_NOTPRESENT";
    export const title = "Feature-Policy header not present";
    export const message = "We not found the Feature-Policy header, if you not defined a policy, by default all features are enabled";
    export const risk = "By default, you let an access to all resources, hacker can potentially access to this resource"; 
}

// No presence of secure flag for one cookie
export module WARNING_COOKIESECURE_NOTPRESENT {
    export const type = "WARNING_COOKIESECURE_NOTPRESENT";
    export const title = "Secure flag not present for session cookie";
    export const message = "We not found secure flag for a session cookie";
    export const risk = "Cookie can be transmitted with HTTP procotol and can be intercept by an attacker"; 
}

// No presence of secure httpOnly for one cookie
export module WARNING_COOKIEHTTPONLY_NOTPRESENT {
    export const type = "WARNING_COOKIEHTTPONLY_NOTPRESENT";
    export const title = "HttpOnly flag not present for session cookie";
    export const message = "We not found httpOnly flag for a session cookie";
    export const risk = "Cookie can be manipulated and hijack by an attacker"; 
}

// No referrer-policy header
export module WARNING_REFERRERPOLICY_NOTPRESENT {
    export const type = "WARNING_REFERRERPOLICY_NOTPRESENT";
    export const title = "Referrer-policy header not present";
    export const message = "We not found Referrer-Policy header";
    export const risk = "Untrust domain can transmitted informations like session cookie"; 
}

// No x-frame-options header
export module WARNING_XFRAMEOPTIONS_NOTPRESENT {
    export const type = "WARNING_XFRAMEOPTIONS_NOTPRESENT";
    export const title = "X-Frame-Options header not present";
    export const message = "We not found X-Frame-Options";
    export const risk = "Unstrust web site can acess to Your GUI with iframe"; 
}

// No x-xss-protection header
export module WARNING_XXSSPROTECTION_NOTPRESENT {
    export const type = "WARNING_XXSSPROTECTION_NOTPRESENT";
    export const title = "X-Xss-Protection header not present";
    export const message = "We not found X-Xss-Protection";
    export const risk = "You don't benefict of browser XSS protection, some attackers can inject some malicious requests"; 
}

// Stack trace divulgation
// .+Exception[^\n]+\n(\t+\Qat \E.+\s+)+
export module WARNING_STACKTRACE_DIVULGATION {
    export const type = "WARNING_STACKTRACE_DIVULGATION";
    export const title = "Stack trace disclosed";
    export const message = "A stack trace is present in response body";
    export const risk = "Stack trace can reveal lot's of informations about the technologies / libraries used to the attacker"; 
}

// Request path contains server-info and response status equals to 200
export module WARNING_SERVERINFO_DIVULGATION {
    export const type = "WARNING_SERVERINFO_DIVULGATION";
    export const title = "Server info page divulgated";
    export const message = "Some requests to access to get server info page send a HTTP status 200";
    export const risk = "An attacker can collect some informations about server"; 
}

// Request path contains server-status and response status equals to 200
export module WARNING_SERVERSTATUS_DIVULGATION {
    export const type = "WARNING_SERVERSTATUS_DIVULGATION";
    export const title = "Server status page divulgated";
    export const message = "Some requests to access to get server status page send a HTTP status 200";
    export const risk = "An attacker can collect some informations about server"; 
}

// Request path contains phphinfo and response status equals to 200
export module WARNING_PHPINFO_DIVULGATION {
    export const type = "WARNING_PHPINFO_DIVULGATION";
    export const title = "Php info page divulgated";
    export const message = "Some requests to access to get php info page send a HTTP status 200";
    export const risk = "An attacker can collect some informations about server"; 
}

// Request path contains .git and response status equals to 200
export module WARNING_CVSDIRECTORY_DIVULGATION {
    export const type = "WARNING_CVSDIRECTORY_DIVULGATION";
    export const title = "CVS directory divulgated";
    export const message = "Some requests to access to CVS directory send a HTTP status 200";
    export const risk = "An attacker can collect some informations about source code"; 
}


// Request path contains robots.txt and response status equals to 200
export module WARNING_ROBOTSFILE_DIVULGATION {
    export const type = "WARNING_ROBOTSFILE_DIVULGATION";
    export const title = "Robots file divulgated";
    export const message = "Some requests to access to robots.txt send a HTTP status 200";
    export const risk = "An attacker can collect some protect directories or urls";
}

// Request contains url in redirect query and response status equals to 303 and location value equals to query param
export module WARNING_UNCONTROLLED_REDIRECTION {
    export const type = "WARNING_UNCONTROLLED_REDIRECTION";
    export const title = "No controlled redirection";
    export const message = "There are no control to check the redirection url";
    export const risk = "An attacker can redirect users to an other website and stole some informations";
}

// Crossdomain.xml accesible
/*
export module OPEN_SWF_CDP {
    export const type = "OPEN_SWF_CDP";
    export const title = "";
    export const message = "";
    export const risk = "";
}

// ClientAccessPolicy. accessible
export module OPEN_SILVERLIGHT_CAP {
    export const type = "OPEN_SILVERLIGHT_CAP";
    export const title = "";
    export const message = "";
    export const risk = "";
}

// sftp-config.json accessible
export module OPEN_SUBLIME_CONFIG {
    export const type = "OPEN_SUBLIME_CONFIG";
    export const title = "";
    export const message = "";
    export const risk = "";
}

// .DS_Store accessible
export module MAC_CACHE_DIVULGATION {
    export const type = "MAC_CACHE_DIVULGATION";
    export const title = "";
    export const message = "";
    export const risk = "";
}

// Input not contains auto_complete option
export module AUTOCOMPLETE_ENABLED {
    export const type = "";
    export const title = "";
    export const message = "";
    export const risk = "";
}

// .bak tmp found
export module BACKUP_FILE_DISCLOSURE {
    export const type = "";
    export const title = "";
    export const message = "";
    export const risk = "";
}

// Response body contains IP
export module IPADDRESS_EXPOSED {
    export const type = "";
    export const title = "";
    export const message = "";
    export const risk = "";
}

// Trace protocol and response status not equals to 405
export module TRACE_METHOD_ENABLED {
    export const type = "";
    export const title = "";
    export const message = "";
    export const risk = "";
}

// access-control-origin header equals to *
export module PUBLIC_CORS_ENABLED {
    export const type = "";
    export const title = "";
    export const message = "";
    export const risk = "";
}
*/

// No content-type header
export module WARNING_CONTENTTYPE_MISSING {
    export const type = "WARNING_CONTENTTYPE_MISSING";
    export const title = "Content-Type header missing";
    export const message = "We not found 'Content-Type' header";
    export const risk = "If an attacker can upload content, an attacker can upload some media content with vulnerabilities";
}