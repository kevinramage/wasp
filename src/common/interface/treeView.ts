export interface TreeView {
    root : TreeItem;
}

export interface TreeItem {
    id ?: number;
    value ?: string;
    children : TreeItem[];
}