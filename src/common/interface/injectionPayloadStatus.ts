import { INJECTIONPAYLOAD_STATUS } from "../constantes";
import { IRequest } from "./request";

export interface IInjectionPayloadStatus {
    status: INJECTIONPAYLOAD_STATUS;
    payload: string;
    result: string;
    errorMessage?: string;
    request: IRequest;
}