import { IControlAttribute } from "./controlAttribute";

export interface IControl {
    type ?: string;
    text ?: string;
    html ?: string;
    identifier ?: string;
    attributes ?: IControlAttribute[];
    children ?: IControl[];
    parent ?: IControl;
}