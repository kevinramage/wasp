import { IRequest } from "./request";

export interface ICheck {
    id ?: number;
    type ?: string;
    name ?: string;
    applicationId ?: number;
    resourceId ?: number;
    requests ?: { [ name: string ] : IRequest };
}