export interface IComponent {
    id?: number,
    type?: string,
    score?: number,
    html ?: string,
    detail ?: string,
    resourceId ?: number
}