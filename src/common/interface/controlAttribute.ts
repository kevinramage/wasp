export interface IControlAttribute {
    key ?: string;
    value ?: string;
    html ?: string;
}