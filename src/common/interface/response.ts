import { IHeader } from "./header";
import { IRequest } from "./request";

export interface IResponse extends IResponseSimplified {
    request ?: IRequest;
    receivedDate ?: Date;
    type ?: string;
}

export interface IResponseSimplified {
    statusCode ?: number;
    headers ?: IHeader[];
    body ?: string;
}