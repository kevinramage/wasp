export interface IResource {
    id ?: number;
    identifier ?: string
    url ?: string
    completePathname ?: string
    pathname ?: string
    path ?: string;
    title ?: string;
    requestId ?: number;
}