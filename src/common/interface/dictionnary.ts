export interface IDictionnary {
    key: string;
    value: string;
}