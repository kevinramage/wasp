export interface ITechnology {
    id ?: number;
    type ?: string;
    value ?: string;
    name ?: string;
    version ?: string;
}