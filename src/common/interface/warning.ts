export interface IWarning {
    type : string;
    title : string;
    risk : string;
    value ?: string;
    message ?: string;
    applicationId ?: number;
    resourceId ?: number;
}