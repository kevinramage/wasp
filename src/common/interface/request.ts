import { IHeader } from "./header";
import { IResponse, IResponseSimplified } from "./response";

export interface IRequest {
    uuid ?: string;
    protocol ?: string;
    host ?: string;
    method ?: string;
    url ?: string;
    body ?: string;
    headers ?: IHeader[];
    response ?: IResponse;
    sentDate ?: Date;
}

export interface IRequestSimplified {
    uuid ?: string;
    protocol ?: string;
    host ?: string;
    method ?: string;
    url ?: string;
    body ?: string;
    headers ?: IHeader[];
    response ?: IResponseSimplified;
}