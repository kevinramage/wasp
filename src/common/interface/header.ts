export interface IHeader {
    key : string;
    value : string  | Array<String>;
}