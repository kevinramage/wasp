import { IInjectionPayloadStatus } from "./injectionPayloadStatus";
import { INJECTION_STATUS } from "../constantes";

export interface IInjectionStatus {
    resourceId?: number;
    entryPointId?: number;
    injectionTypeId?: number;
    reference?: string;
    defaultValue ?: string;
    difference?: string;
    status?: INJECTION_STATUS;
    errorMessage?: string;
    payloads?: IInjectionPayloadStatus[];
}